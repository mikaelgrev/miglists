package com.miginfocom.lists;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 2013-10-21
 *         Time: 18:05
 */
public final class FlatPage<T> implements MigList.Page<T, int[]>
{
	private final Serializer<T> serializer;
	private final int flatObjSize;
	private final int[] array;

	private int flatSize = 0; // Always a multiple of flatObjSize.
	private int sharers = 0;

	// These are modified even for read operations
	private final Pipe<T> pipe;
	private Pipe<T> objectCache = null; // Lazy

	FlatPage(Serializer<T> serializer, int pageSize)
	{
		this.serializer = serializer;
		this.flatObjSize = serializer.intsRequired();
		this.array = new int[pageSize * flatObjSize];
		this.pipe = new Pipe<>(array, flatObjSize);
	}

	/**
	 * @param serializer
	 * @param arr The full array to base the flat array on
	 * @param size The number of actual objects in <code>arr</code> (size)
	 */
	FlatPage(Serializer<T> serializer, T[] arr, int size)
	{
		this.serializer = serializer;
		this.flatObjSize = serializer.intsRequired();
		this.array = toFlatArray(Arrays.asList(arr), size);
		this.pipe = new Pipe<>(array, flatObjSize);
		this.flatSize = size * flatObjSize;

	}

	private FlatPage(Serializer<T> serializer, int[] array, int flatSize)
	{
		this.serializer = serializer;
		this.flatObjSize = serializer.intsRequired();
		this.array = array;
		this.pipe = new Pipe<>(array, flatObjSize);
		this.flatSize = flatSize;
	}

	// read ********************************************************

	public int size()
	{
		return flatSize / flatObjSize;
	}

	public boolean isFull()
	{
		return flatSize == array.length;
	}

	public FlatPage<T> copyPage()
	{
		removeSharer();
		return new FlatPage<>(serializer, array.clone(), flatSize);
	}

	public T get(int offset)
	{
		return serializer.read(preparePipe(offset));
	}

	public int offsetOf(Object o, int startOffset, int endOffset)
	{
		boolean succ = writeToCacheArray(o);
		return succ ? offsetOf(objectCache.array, toFlat(startOffset), toFlat(endOffset)) : -1;
	}

	int offsetOf(int[] flatView, int flatStartOffset, int flatEndOffset)
	{
		int first = flatView[0]; // Big speedup if first int is quite unique
		for (; flatStartOffset < flatEndOffset; flatStartOffset += flatObjSize) {
			if (first == array[flatStartOffset] && flatEqualsFromOne(flatView, flatStartOffset))
				return flatStartOffset / flatObjSize;
		}
		return -1;
	}

	public int lastOffsetOf(Object o, int startOffset, int endOffset)
	{
		boolean succ = writeToCacheArray(o);
		return succ ? lastOffsetOf(objectCache.array, toFlat(startOffset), toFlat(endOffset)) : -1;
	}

	public int lastOffsetOf(int[] flatView, int flatStartOffset, int flatEndOffset)
	{
		int first = flatView[0]; // Big speedup if first int is quite unique
		for (int i = flatEndOffset - flatObjSize; i >= flatStartOffset; i -= flatObjSize) {
			if (first == array[i] && flatEqualsFromOne(flatView, i))
				return i / flatObjSize;
		}
		return -1;
	}

	public void toArray(T[] destArr, int destOffset)
	{
		for (int flatIx = 0; flatIx < flatSize; destOffset++, flatIx += flatObjSize)
			destArr[destOffset] = serializer.read(preparePipeFlat(flatIx));
	}

	public void toArray(T[] destArr, int destOffset, int startOffset, int endOffset)
	{
		startOffset = toFlat(startOffset);
		endOffset = toFlat(endOffset);
		for (; startOffset < endOffset; destOffset++, startOffset += flatObjSize)
			destArr[destOffset] = serializer.read(preparePipeFlat(startOffset));
	}

	public int elementCountDebug()
	{
		return array.length / flatObjSize;
	}

	// Write ********************************************************


	public void addSharer()
	{
		sharers++;
	}

	public void removeSharer()
	{
		sharers--;
	}

	public int getSharers()
	{
		return sharers;
	}

	public FlatPage<T> set(int offset, T element)
	{
		if (sharers == 0) {
			return setImpl(toFlat(offset), element);
		} else {
			return copyPage().setImpl(toFlat(offset), element);
		}
	}

	private FlatPage<T> setImpl(int flatOffset, T element)
	{
		serializer.write(element, preparePipeFlat(flatOffset));
		return this;
	}

	public FlatPage<T> add(T element)
	{
		if (sharers == 0) {
			return addImpl(element);
		} else {
			return copyPage().addImpl(element);
		}
	}

	private FlatPage<T> addImpl(T element)
	{
		serializer.write(element, preparePipeFlat(flatSize));
		flatSize += flatObjSize;
		return this;
	}

	public FlatPage<T> add(int offset, T element)
	{
		if (sharers == 0) {
			return addImpl(toFlat(offset), element);
		} else {
			return copyPage().addImpl(toFlat(offset), element);
		}
	}

	private FlatPage<T> addImpl(int flatOffset, T element)
	{
		System.arraycopy(array, flatOffset, array, flatOffset + flatObjSize, flatSize - flatOffset);
		serializer.write(element, preparePipeFlat(flatOffset));
		flatSize += flatObjSize;
		return this;
	}

	public FlatPage<T> add(Iterator<? extends T> source)
	{
		if (sharers == 0) {
			return addImpl(source);
		} else {
			return copyPage().addImpl(source);
		}
	}

	private FlatPage<T> addImpl(Iterator<? extends T> source)
	{
		while(flatSize < array.length && source.hasNext()) {
			serializer.write(source.next(), preparePipeFlat(flatSize));
			flatSize += flatObjSize;
		}

		return this;
	}

	public FlatPage<T> removeIx(int offset)
	{
		if (sharers == 0) {
			return removeIxImpl(toFlat(offset));
		} else {
			return copyPage().removeIxImpl(toFlat(offset));
		}
	}

	private FlatPage<T> removeIxImpl(int flatOffset)
	{
		flatSize -= flatObjSize;
		int flatCopyLen = flatSize - flatOffset;
		if (flatCopyLen > 0)
			System.arraycopy(array, flatOffset + flatObjSize, array, flatOffset, flatCopyLen);

		return this;
	}

	public FlatPage<T> removeFirst(Object o, int startOffset, int endOffset)
	{
		if (sharers == 0) {
			return removeFirstImpl(o, toFlat(startOffset), toFlat(endOffset));
		} else {
			return removeFirstOnCopyIfFound(o, toFlat(startOffset), toFlat(endOffset));
		}
	}

	private FlatPage<T> removeFirstImpl(Object o, int flatStartOffset, int flatEndOffset)
	{
		if (writeToCacheArray(o)) {
			int first = objectCache.array[0]; // Big speedup if first int is quite unique
			for (int i = flatStartOffset; i < flatEndOffset; i += flatObjSize) {
				if (first == array[i] && flatEqualsFromOne(objectCache.array, i)) {
					removeIxImpl(i);
					return this;
				}
			}
		}
		return null;
	}

	private FlatPage<T> removeFirstOnCopyIfFound(Object o, int flatStartOffset, int flatEndOffset)
	{
		if (writeToCacheArray(o)) {
			int first = objectCache.array[0]; // Big speedup if first int is quite unique
			for (int i = flatStartOffset; i < flatEndOffset; i += flatObjSize) {
				if (first == array[i] && flatEqualsFromOne(objectCache.array, i))
					return copyPage().removeIxImpl(i);
			}
		}
		return null;
	}

	public FlatPage<T> removeAll(Object o, int startOffset, int endOffset)
	{
		if (sharers == 0) {
			return removeAllImpl(o, toFlat(startOffset), toFlat(endOffset));
		} else {
			return removeAllOnCopyIfFound(o, toFlat(startOffset), toFlat(endOffset));
		}
	}

	private FlatPage<T> removeAllImpl(Object o, int flatStartOffset, int flatEndOffset)
	{
		if (writeToCacheArray(o)) {
			int src = flatStartOffset, dst = flatStartOffset;
			int first = objectCache.array[0]; // Big speedup if first int is quite unique
			for (; src < flatEndOffset; src += flatObjSize) {
				if (first != array[src] || !flatEqualsFromOne(objectCache.array, src)) {
					if (src != dst)
						System.arraycopy(array, src, array, dst, flatObjSize);
					dst += flatObjSize;
				}
			}
			if (dst != src) {
				flatSize = dst;
				return this;
			}
		}
		return null; // No change
	}

	private FlatPage<T> removeAllOnCopyIfFound(Object o, int flatStartOffset, int flatEndOffset)
	{
		if (writeToCacheArray(o)) {
			int first = objectCache.array[0]; // Big speedup if first int is quite unique
			for (int i = flatStartOffset; i < flatEndOffset; i += flatObjSize) {
				if (first == array[i] && flatEqualsFromOne(objectCache.array, i))
					return copyPage().removeAllImpl(o, i, flatEndOffset);
			}
		}
		return null;
	}

	public FlatPage<T> removeRange(int startOffset, int endOffset)
	{
		if (startOffset == endOffset)
			return null;

		if (sharers == 0) {
			return removeRangeImpl(toFlat(startOffset), toFlat(endOffset));
		} else {
			return copyPage().removeRangeImpl(toFlat(startOffset), toFlat(endOffset));
		}
	}

	private FlatPage<T> removeRangeImpl(int flatStartOffset, int flatEndOffset)
	{
		int moveLen = flatSize - flatEndOffset;
		if (moveLen != 0)
			System.arraycopy(array, flatEndOffset, array, flatStartOffset, moveLen);

		flatSize -= (flatEndOffset - flatStartOffset);
		return this;
	}

	public FlatPage<T> retainAll(Collection<?> col, int startOffset, int endOffset)
	{
		int[] flatCol = toFlatArray(col, col.size());

		if (sharers == 0) {
			return retainAllImpl(flatCol, toFlat(startOffset), toFlat(endOffset));
		} else {
			return retainAllOnCopyIfNotFound(flatCol, toFlat(startOffset), toFlat(endOffset));
		}
	}

	private FlatPage<T> retainAllImpl(int[] flatCollection, int flatStartOffset, int flatEndOffset)
	{
		int src = flatStartOffset, dst = flatStartOffset;
		for (; src < flatEndOffset; src += flatObjSize) {
			if (flatContains(flatCollection, src)) {
				if (src != dst)
					System.arraycopy(array, src, array, dst, flatObjSize);
				dst += flatObjSize;
			}
		}

		int removedFlatLen = src - dst;
		if (removedFlatLen == 0) // Nothing changed
			return null;

		int moveFlatLen = flatSize - flatEndOffset;
		if (moveFlatLen > 0)
			System.arraycopy(array, src, array, dst, moveFlatLen);

		flatSize -= removedFlatLen;
		return this;
	}

	private FlatPage<T> retainAllOnCopyIfNotFound(int[] flatCollection, int flatStartOffset, int flatEndOffset)
	{
		for (int flatIx = flatStartOffset; flatIx < flatEndOffset; flatIx += flatObjSize) {
			if (!flatContains(flatCollection, flatIx))
				return copyPage().retainAllImpl(flatCollection, flatIx, flatEndOffset);
		}
		return null;
	}

	public FlatPage<T> clear()
	{
		if (flatSize == 0)
			return null;

		if (sharers == 0) {
			return clearImpl();
		} else {
			return copyPage().clearImpl();
		}
	}

	private FlatPage<T> clearImpl()
	{
		flatSize = 0;
		return this;
	}

	public FlatPage<T>[] split(int fromOffset)
	{
		int fromFlatOffset = toFlat(fromOffset);
		FlatPage <T> newPage = new FlatPage<>(serializer, size());
		int newPageFlatSize = flatSize - fromFlatOffset;
		System.arraycopy(array, fromFlatOffset, newPage.array, 0, newPageFlatSize);
		newPage.flatSize = newPageFlatSize;

		if (sharers == 0) {
			flatSize = fromFlatOffset;
			return new FlatPage[] {this, newPage};
		} else {
			sharers--;
			FlatPage<T> pageClone = new FlatPage<>(serializer, array.length / flatObjSize);
			System.arraycopy(array, 0, pageClone.array, 0, fromFlatOffset);
			pageClone.flatSize = fromFlatOffset;
			return new FlatPage[] {pageClone, newPage};
		}
	}

	private Pipe<T> preparePipe(int offset)
	{
		pipe.setFlatOffset(toFlat(offset));
		return pipe;
	}

	private Pipe<T> preparePipeFlat(int flatOffset)
	{
		pipe.setFlatOffset(flatOffset);
		return pipe;
	}

	private boolean writeToCacheArray(Object o)
	{
		if (objectCache == null)
			objectCache = new Pipe<>(new int[flatObjSize], flatObjSize);

		objectCache.setToWhole();
		return serializer.writeObject(o, objectCache);
	}

	private int toFlat(int offset)
	{
		return offset * flatObjSize;
	}

	private boolean flatEqualsFromOne(int[] arr, int flatOffset)
	{
		for (int i = 1; i < arr.length; i++) {
			if (arr[i] != array[flatOffset + i])
				return false;
		}
		return true;
	}

	/** Returns if arr contains any flat objects at the flat index <code>ix</code> of this page array.
	 * @param arr An array with 0 to more objects in a flat array.
	 * @param flatStartOffset The index into this page's flat array
	 * @return The it contains at least one object.
	 */
	private boolean flatContains(int[] arr, final int flatStartOffset)
	{
		final int flatEndOffset = flatStartOffset + flatObjSize;
		for (int i = 0; i < arr.length; i += flatObjSize) {
			boolean misMatch = false;
			for (int arrIx = i, flatIx = flatStartOffset; flatIx < flatEndOffset; flatIx++, arrIx++) {
				if (arr[arrIx] != array[flatIx]) {
					misMatch = true;
					break;
				}
			}
			if (!misMatch)
				return true;
		}
		return false;
	}

	private int[] toFlatArray(Collection<?> col, int size)
	{
		int[] arr = new int[size * flatObjSize];
		Pipe<T> arrPipe = new Pipe<>(arr, flatObjSize);
		int flatIx = 0;
		for (Object o : col) {
			arrPipe.setFlatOffset(flatIx);
			if (serializer.writeObject(o, arrPipe))
				flatIx += flatObjSize;
		}
		return flatIx == arr.length ? arr : Arrays.copyOf(arr, flatIx);
	}
}
