package com.miginfocom.lists;


/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 2013-10-21
 *         Time: 18:10
 */
public interface ReadPipe<T>
{
	byte[] readBytes();

	void readBytes(byte[] target);

	short[] readShorts();

	void readShorts(short[] target);

	int readInt();

	int readInt(int ix);

	long readLong();

	long readLong(int ix);

	float readFloat();

	float readFloat(int ix);

	double readDouble();

	double readDouble(int ix);
}
