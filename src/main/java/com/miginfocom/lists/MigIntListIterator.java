package com.miginfocom.lists;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 10/5/13
 *         Time: 0:18 AM
 */
public class MigIntListIterator extends MigListIterator<Integer>
{
	public MigIntListIterator(MigListInt list, long fromIndex)
	{
		super(list, fromIndex);
	}

	public MigIntListIterator(MigListInt list, long fromIndex, long endIndex)
	{
		super(list, fromIndex, endIndex);
	}

	// todo Complete..

//	public void add(Integer i)
//	{
//		addInt(i);
//	}
//
//	public void addInt(int i)
//	{
//		((MigListInt) list).addInt(cursor++, i);
//		lastRet = -1;
//		endIndex++;
//	}
//
//	public Integer next()
//	{
//	}
//
//	public Integer previous()
//	{
//	}
//
//	public void set(Integer integer)
//	{
//	}
}
