package com.miginfocom.lists;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 10/13/13
 *         Time: 22:39 PM
 */
final class ObjectPage<T> implements MigList.Page<T, T[]>
{
	private int size;
	private int sharers = 1;
	private final T[] array;

	ObjectPage(int pageSize)
	{
		this.array = (T[]) new Object[pageSize];
	}

	ObjectPage(int size, T[] array)
	{
		this.size = size;
		this.array = array;
	}

	// read ********************************************************

	public int size()
	{
		return size;
	}

	public boolean isFull()
	{
		return size == array.length;
	}

	public ObjectPage<T> copyPage()
	{
		removeSharer();
		return new ObjectPage<>(size, array.clone());
	}

	public T get(int offset)
	{
		return array[offset];
	}

	public int offsetOf(Object o, int startOffset, int endOffset)
	{
		for (int i = startOffset; i < endOffset; i++) {
			if (Objects.equals(array[i], o))
				return i;
		}
		return -1;
	}

	public int lastOffsetOf(Object o, int startOffset, int endOffset)
	{
		for (int i = endOffset - 1; i >= startOffset; i--) {
			if (Objects.equals(array[i], o))
				return i;
		}
		return -1;
	}

	public void toArray(T[] destArr, int destOffset)
	{
		System.arraycopy(array, 0, destArr, destOffset, size);
	}

	public void toArray(T[] destArr, int destOffset, int startOffset, int endOffset)
	{
		System.arraycopy(array, startOffset, destArr, destOffset, endOffset - startOffset);
	}

	public int elementCountDebug()
	{
		return array.length;
	}

	// Write ********************************************************


	public void addSharer()
	{
		sharers++;
	}

	public void removeSharer()
	{
		sharers--;
	}

	public int getSharers()
	{
		return sharers;
	}

	public MigList.Page<T, T[]> set(int offset, T element)
	{
		if (sharers == 1) {
			return setImpl(offset, element);
		} else {
			return copyPage().setImpl(offset, element);
		}
	}

	private MigList.Page<T, T[]> setImpl(int offset, T element)
	{
		array[offset] = element;
		return this;
	}

	public MigList.Page<T, T[]> add(T element)
	{
		if (sharers == 1) {
			return addImpl(element);
		} else {
			return copyPage().addImpl(element);
		}
	}

	private MigList.Page<T, T[]> addImpl(T element)
	{
		array[size++] = element;
		return this;
	}

	public MigList.Page<T, T[]> add(int offset, T element)
	{
		if (sharers == 1) {
			return addImpl(offset, element);
		} else {
			return copyPage().addImpl(offset, element);
		}
	}

	private MigList.Page<T, T[]> addImpl(int offset, T element)
	{
		System.arraycopy(array, offset, array, offset + 1, size - offset);
		array[offset] = element;
		size++;
		return this;
	}

	public MigList.Page<T, T[]> add(Iterator<? extends T> source)
	{
		if (sharers == 1) {
			return addImpl(source);
		} else {
			return copyPage().addImpl(source);
		}
	}

	private MigList.Page<T, T[]> addImpl(Iterator<? extends T> source)
	{
		while(size < array.length && source.hasNext())
			array[size++] = source.next();
		return this;
	}

	public MigList.Page<T, T[]> removeIx(int offset)
	{
		if (sharers == 1) {
			return removeIxImpl(offset);
		} else {
			return copyPage().removeIxImpl(offset);
		}
	}

	private MigList.Page<T, T[]> removeIxImpl(int offset)
	{
		int len = --size - offset;
		if (len > 0)
			System.arraycopy(array, offset + 1, array, offset, len);

		array[size] = null;
		return this;
	}

	public MigList.Page<T, T[]> removeFirst(Object o, int startOffset, int endOffset)
	{
		if (sharers == 1) {
			return removeFirstImpl(o, startOffset, endOffset);
		} else {
			return removeFirstOnCopyIfFound(o, startOffset, endOffset);
		}
	}

	private MigList.Page<T, T[]> removeFirstImpl(Object o, int startOffset, int endOffset)
	{
		for (int i = startOffset; i < endOffset; i++) {
			if (Objects.equals(array[i], o)) {
				removeIxImpl(i);
				return this;
			}
		}
		return null;
	}

	private MigList.Page<T, T[]> removeFirstOnCopyIfFound(Object o, int startOffset, int endOffset)
	{
		for (int i = startOffset; i < endOffset; i++) {
			if (Objects.equals(array[i], o))
				return copyPage().removeIxImpl(i);
		}
		return null;
	}

	public MigList.Page<T, T[]> removeAll(Object o, int startOffset, int endOffset)
	{
		if (sharers == 1) {
			return removeAllImpl(o, startOffset, endOffset);
		} else {
			return removeAllOnCopyIfFound(o, startOffset, endOffset);
		}
	}

	private MigList.Page<T, T[]> removeAllImpl(Object o, int startOffset, int endOffset)
	{
		int src = startOffset, dst = startOffset;
		for (; src < endOffset; src++) {
			T t = array[src];
			if (!Objects.equals(o, t))
				array[dst++] = t;
		}

		if (dst == src)
			return null; // No change

		// Clear end of array to not have a memory leak
		Arrays.fill(array, dst, size, null);
		size = dst;
		return this;
	}

	private MigList.Page<T, T[]> removeAllOnCopyIfFound(Object o, int startOffset, int endOffset)
	{
		for (int i = startOffset; i < endOffset; i++) {
			if (Objects.equals(array[i], o))
				return copyPage().removeAllImpl(o, i, endOffset);
		}
		return null;
	}

	public MigList.Page<T, T[]> removeRange(int startOffset, int endOffset)
	{
		if (startOffset == endOffset)
			return null;

		if (sharers == 1) {
			return removeRangeImpl(startOffset, endOffset);
		} else {
			return copyPage().removeRangeImpl(startOffset, endOffset);
		}
	}

	private MigList.Page<T, T[]> removeRangeImpl(int startOffset, int endOffset)
	{
		int moveLen = size - endOffset;
		if (moveLen != 0)
			System.arraycopy(array, endOffset, array, startOffset, moveLen);

		int newSize = size - (endOffset - startOffset);
		Arrays.fill(array, newSize, size, null);
		size = newSize;
		return this;
	}

	public MigList.Page<T, T[]> retainAll(Collection<?> col, int startOffset, int endOffset)
	{
		if (sharers == 1) {
			return retainAllImpl(col, startOffset, endOffset);
		} else {
			return retainAllOnCopyIfFound(col, startOffset, endOffset);
		}
	}

	private MigList.Page<T, T[]> retainAllImpl(Collection<?> col, int startOffset, int endOffset)
	{
		int src = startOffset, dst = startOffset;
		for (; src < endOffset; src++) {
			T t = array[src];
			if (col.contains(t))
				array[dst++] = t;
		}

		int removedLen = src - dst;
		if (removedLen == 0) // Nothing changed
			return null;

		int moveLen = size - endOffset;
		if (moveLen > 0)
			System.arraycopy(array, src, array, dst, moveLen);

		// Clear end of array to not have a memory leak
		Arrays.fill(array, size - removedLen, size, null);
		size -= removedLen;
		return this;
	}

	private MigList.Page<T, T[]> retainAllOnCopyIfFound(Collection<?> col, int startOffset, int endOffset)
	{
		for (int ix = startOffset; ix < endOffset; ix++) {
			if (!col.contains(array[ix]))
				return copyPage().retainAllImpl(col, ix, endOffset);
		}
		return null;
	}

	public MigList.Page<T, T[]> clear()
	{
		if (size == 0)
			return null;

		if (sharers == 1) {
			return clearImpl();
		} else {
			return copyPage().clearImpl();
		}
	}

	private MigList.Page<T, T[]> clearImpl()
	{
		Arrays.fill(array, 0, size, null);
		size = 0;
		return this;
	}

	public MigList.Page<T, T[]>[] split(int fromOffset)
	{
		ObjectPage<T> newPage = new ObjectPage<>(array.length);
		int newPageSize = size - fromOffset;
		System.arraycopy(array, fromOffset, newPage.array, 0, newPageSize);
		newPage.size = newPageSize;

		if (sharers == 1) {
			Arrays.fill(array, fromOffset, size, null);
			size = fromOffset;
			return new MigList.Page[] {this, newPage};
		} else {
			sharers--; // todo The phantom reference queue aren't checked if we have som sharers to remove.
			ObjectPage<T> pageClone = new ObjectPage<>(array.length);
			System.arraycopy(array, 0, pageClone.array, 0, fromOffset);
			pageClone.size = fromOffset;
			return new MigList.Page[] {pageClone, newPage};
		}
	}
}
