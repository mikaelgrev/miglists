package com.miginfocom.lists;


/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 2013-10-14
 *         Time: 19:06
 */
//public final class ObjectPageRW<T> extends ObjectPage<T>
//{
//	private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
//
//	ObjectPageRW(int pageSize)
//	{
//		super(pageSize);
//	}
//
//	ObjectPageRW(int size, T[] array)
//	{
//		super(size, array);
//	}
//
//	public void removeSharer()
//	{
//		lock.writeLock().lock();
//		try {
//			super.removeSharer();
//		} finally {
//			lock.writeLock().unlock();
//		}
//	}
//
//	public void addSharer()
//	{
//		lock.writeLock().lock();
//		try {
//			super.addSharer();
//		} finally {
//			lock.writeLock().unlock();
//		}
//	}
//
//	public T get(int offset)
//	{
//		lock.readLock().lock();
//		try {
//			return super.get(offset);
//		} finally {
//			lock.readLock().unlock();
//		}
//	}
//
//	public int offsetOf(Object o, int startOffset, int endOffset)
//	{
//		lock.readLock().lock();
//		try {
//			return super.offsetOf(o, startOffset, endOffset);
//		} finally {
//			lock.readLock().unlock();
//		}
//	}
//
//	public int lastOffsetOf(Object o, int startOffset, int endOffset)
//	{
//		lock.readLock().lock();
//		try {
//			return super.lastOffsetOf(o, startOffset, endOffset);
//		} finally {
//			lock.readLock().unlock();
//		}
//	}
//
//	public void toArray(T[] destArr, int destOffset)
//	{
//		lock.readLock().lock();
//		try {
//			super.toArray(destArr, destOffset);
//		} finally {
//			lock.readLock().unlock();
//		}
//	}
//
//	public void toArray(T[] destArr, int destOffset, int startOffset, int endOffset)
//	{
//		lock.readLock().lock();
//		try {
//			super.toArray(destArr, destOffset, startOffset, endOffset);
//		} finally {
//			lock.readLock().unlock();
//		}
//	}
//
//	public int arraySizeDebug()
//	{
//		lock.readLock().lock();
//		try {
//			return super.arraySizeDebug();
//		} finally {
//			lock.readLock().unlock();
//		}
//	}
//
//	//************ Write *************************************
//
//
//	public MigList.Page<T, T[]> set(int offset, T element)
//	{
//		lock.writeLock().lock();
//		try {
//			return super.set(offset, element);
//		} finally {
//			lock.writeLock().unlock();
//		}
//	}
//
//	public MigList.Page<T, T[]> add(T element)
//	{
//		lock.writeLock().lock();
//		try {
//			return super.add(element);
//		} finally {
//			lock.writeLock().unlock();
//		}
//	}
//
//	public MigList.Page<T, T[]> add(int offset, T element)
//	{
//		lock.writeLock().lock();
//		try {
//			return super.add(offset, element);
//		} finally {
//			lock.writeLock().unlock();
//		}
//	}
//
//	public MigList.Page<T, T[]> add(Iterator<? extends T> source)
//	{
//		lock.writeLock().lock();
//		try {
//			return super.add(source);
//		} finally {
//			lock.writeLock().unlock();
//		}
//	}
//
//	public MigList.Page<T, T[]> removeIx(int offset)
//	{
//		lock.writeLock().lock();
//		try {
//			return super.removeIx(offset);
//		} finally {
//			lock.writeLock().unlock();
//		}
//	}
//
//	public MigList.Page<T, T[]> removeFirst(Object o, int startOffset, int endOffset)
//	{
//		lock.writeLock().lock();
//		try {
//			return super.removeFirst(o, startOffset, endOffset);
//		} finally {
//			lock.writeLock().unlock();
//		}
//	}
//
//	public MigList.Page<T, T[]> removeAll(Object o, int startOffset, int endOffset)
//	{
//		lock.writeLock().lock();
//		try {
//			return super.removeAll(o, startOffset, endOffset);
//		} finally {
//			lock.writeLock().unlock();
//		}
//	}
//
//	public MigList.Page<T, T[]> removeRange(int startOffset, int endOffset)
//	{
//		lock.writeLock().lock();
//		try {
//			return super.removeRange(startOffset, endOffset);
//		} finally {
//			lock.writeLock().unlock();
//		}
//	}
//
//	public MigList.Page<T, T[]> retainAll(Collection<?> col, int startOffset, int endOffset)
//	{
//		lock.writeLock().lock();
//		try {
//			return super.retainAll(col, startOffset, endOffset);
//		} finally {
//			lock.writeLock().unlock();
//		}
//	}
//
//	public MigList.Page<T, T[]> clear()
//	{
//		lock.writeLock().lock();
//		try {
//			return super.clear();
//		} finally {
//			lock.writeLock().unlock();
//		}
//	}
//
//	public MigList.Page<T, T[]>[] split(int fromOffset)
//	{
//		lock.writeLock().lock();
//		try {
//			return super.split(fromOffset);
//		} finally {
//			lock.writeLock().unlock();
//		}
//	}
//}
