package com.miginfocom.lists;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 10/2/13
 *         Time: 21:17 PM
 */
public interface MigList<E> extends List<E>
{
	// ** Read ********************************

	public abstract E get(long ix);

	public abstract long sizeLong();

	public abstract boolean contains(Object o, long startIndex, long endIndex);

	public abstract long indexOf(Object o, long startIndex, long endIndex);

	public abstract long indexOfLong(Object o);

	public abstract long lastIndexOf(Object o, long startIndex, long endIndex);

	public abstract long lastIndexOfLong(Object o);

	public abstract boolean containsAll(Collection<?> c, long startIndex, long endIndex);

	public abstract Object[] toArray(long startIndex, long endIndex);

	/** Returns the elements as one or many arrays.
	 * @param a The array that will be reused for the first element IF the whole list fits in it.
	 * Otherwise only the type is used to create new arrays.
	 * @param maxPageSize The maximum size of the returned arrays. Will not be used if 'a' is used.
	 * @param startIndex The start index to copy into the array
	 * @param endIndex The end index to copy into the array. Must be equal or greater than startIndex.
	 * @param <T> The type
	 * @return Never null. Never empty. If no elements are returned new T[1][0] is returned.
	 */
	public abstract <T> T[][] toArrays(T[] a, int maxPageSize, long startIndex, long endIndex);

	public abstract <T> T[] toArray(T[] arr, long startIndex, long endIndex);

	// ** Write ********************************

	public abstract MigListIterator<E> listIterator(long startIndex);

	public abstract MigListIterator<E> listIterator(long startIndex, long endIndex);

	public abstract MigList<E> subList(int fromIndex, int toIndex);

	public abstract MigList<E> subList(long fromIndex, long toIndex);

	public abstract E set(long index, E element);

	public abstract void add(long ix, E object);

	public abstract boolean addAll(long index, Collection<? extends E> col);

	public abstract E remove(long ix);

	public abstract boolean remove(Object o, long startIndex, long endIndex);

	public abstract long retainAll(Collection<?> col, long startIndex, long endIndex);

	public abstract long removeAll(Collection<?> col, long startIndex, long endIndex);

	public abstract void removeRange(long startIndex, long endIndex);

	/**
	 * Page return policy for all modifying methods.<p>
	 *     <code>null</code> if nothing was changed. <code>this</code> if no sharers and the content was changed. A new cloned page with the modification if there were shares on <code>this</code>.
	 * @param <E>
	 * @param <ARR>
	 */
	interface Page<E, ARR>
	{
//		Page() {} // Actually fastest exactly like this

		abstract void addSharer();

		abstract void removeSharer();

		abstract int getSharers();

		/** Copy the page if it is shared.
		 * @return The new copy (with 0 sharers) or this with at most 1 sharer (this)
		 */
		abstract Page copyPage();


		// Read ******************************************

		abstract E get(int offset);

		/** Copy the full content of the page to the array with the destination index <code>destOffset</code>.
		 * @param arr The target array. Must be large enough.
		 * @param destOffset The destination offset. Must exist in <code>arr</code>.
		 */
		abstract void toArray(E[] arr, int destOffset);

		/** Copy part of the content of the page to the array with the destination index <code>destOffset</code>.
		 * @param arr The target array. Must be large enough.
		 * @param destOffset The destination offset. Must exist in <code>arr</code>.
		 * @param startOffset The start offset in the page. Must exist.
		 * @param endOffset The end offset (exclusive) in the page. Must exist.
		 */
		abstract void toArray(E[] arr, int destOffset, int startOffset, int endOffset);

		abstract int offsetOf(Object o, int startOffset, int endOffset);
		abstract int lastOffsetOf(Object o, int startOffset, int endOffset);
		abstract int size();
		abstract boolean isFull();

		abstract int elementCountDebug();

		// Read & Write ******************************************

		/**
		 * @param offset
		 * @param element
		 * @return See class JavaDoc.
		 */
		abstract Page<E, ARR> set(int offset, E element);

		/** Adds the object at the end of the page if it fits.
		 * @param object The object to add.
		 * @return See class JavaDoc.
		 */
		abstract Page<E, ARR> add(E object);

		/** Adds the object at the page relative offset if it fits.
		 * @param offset The offset into the page.
		 * @param object The object to add.
		 * @return See class JavaDoc.
		 */
		abstract Page<E, ARR> add(int offset, E object);


		/** Adds as many objects from the iterator that fits.
		 * @param source The source of the objects.
		 * @return See class JavaDoc.
		 */
		abstract Page<E, ARR> add(Iterator<? extends E> source);

		/**
		 * @param offset
		 * @return See class JavaDoc.
		 */
		abstract Page<E, ARR> removeIx(int offset);

		/**
		 * @param o
		 * @param startOffset
		 * @param endOffset
		 * @return See class JavaDoc.
		 */
		abstract Page<E, ARR> removeFirst(Object o, int startOffset, int endOffset);

		/**
		 * @param o
		 * @param startOffset
		 * @param endOffset
		 * @return See class JavaDoc.
		 */
		abstract Page<E, ARR> removeAll(Object o, int startOffset, int endOffset);

		/**
		 * @param startOffset
		 * @param endOffset
		 * @return See class JavaDoc.
		 */
		abstract Page<E, ARR> removeRange(int startOffset, int endOffset);

		/**
		 * @param col
		 * @param startOffset
		 * @param endOffset
		 * @return See class JavaDoc.
		 */
		abstract Page<E, ARR> retainAll(Collection<?> col, int startOffset, int endOffset);

		/** Clears the page and sets all element references to null.
		 * @return See class JavaDoc.
		 */
		abstract Page<E, ARR> clear();

		/** Copy the part of the array from index <code>fromOffset</code> to a new page and returns it.
		 * @param fromOffset Can be (but shouldn't since it's inefficient) be 0. Less than size.
		 * @return Always two elements, none which are null.<p>
		 * 0 - The content before fromOffset. Either as "this" or if there are sharers a copy of that content that should be replaced with this page.
		 * 1 - A new page with 0 sharers that should be inserted after this page.
		 */
		abstract Page<E, ARR>[] split(int fromOffset);
	}
}
