package com.miginfocom.lists;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 26/10/13
 *         Time: 10:42 AM
 */
public class Entry<V, K>
{
	public final K key;
	public final V value;

	public Entry(K key, V value)
	{
		this.key = key;
		this.value = value;
	}
}
