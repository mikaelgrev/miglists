package com.miginfocom.lists;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 09/11/13
 *         Time: 0:01 AM
 */
public final class IndexMigList<E> extends MigListBase<E, E[]>
{
	private final Indexer<E>[] indexers;
	private final boolean indexValues;

	@SafeVarargs
	public IndexMigList(boolean indexValues, Indexer<E>... indexers)
	{
		this.indexers = indexers.clone();
		this.indexValues = indexValues;
	}

	@SafeVarargs
	public IndexMigList(boolean indexValues, Collection<? extends E> startCol, Indexer<E>... indexers)
	{
		super(startCol);
		this.indexers = indexers.clone();
		this.indexValues = indexValues;
	}

	@SafeVarargs
	public IndexMigList(boolean indexValues, Indexer<E>[] indexers, E... startArray)
	{
		super(startArray);
		this.indexers = indexers.clone();
		this.indexValues = indexValues;
	}

	Page<E, E[]> createPage()
	{
		return new IndexedObjectPage<>(pageSize, indexers, indexValues);
	}

	Page<E, E[]> createPage(E[] arr, int size)
	{
		return new IndexedObjectPage<>(size, arr, indexers, indexValues);
	}

	private IndexedObjectPage<E> getPage(int ix)
	{
		return (IndexedObjectPage<E>) pages[ix];
	}

	/** Returns zero or more elements denoted by the index.
	 * @param indexer The indexer used to create the index. Must be same object that was sent into the constructor.
	 * @param index The index to look up. May be null.
	 * @param maxResults The maximum number of entries that <code>outCol</code> will contain. At least one will be added (if found) even if maxResults &lt; 0.
	 * @return Never null. Unmodifiable.
	 * @throws IllegalArgumentException If the indexer wasn't one of the indexers sent into the constructor.
	 */
	public List<E> getIndexed(Indexer<E> indexer, final Object index, final int maxResults, long startIndex, long endIndex)
	{
		int ix = getIndexOfIndexer(indexer);

		GetIndexedVisitor visitor = new GetIndexedVisitor(index, ix, maxResults);
		visitPagesReadOnly(visitor, startIndex, endIndex);

		return visitor.list != null ? visitor.list : Collections.<E>emptyList();
	}

	/** Returns zero or more elements denoted by the index.
	 * @param indexer The indexer used to create the index. Must be same object that was sent into the constructor.
	 * @param index The index to look up. May be null.
	 * @param maxResults The maximum number of entries that <code>outCol</code> will contain. At least one will be added (if found) even if maxResults &lt; 0.
	 * @return Never null. Unmodifiable.
	 * @throws IllegalArgumentException If the indexer wasn't one of the indexers sent into the constructor.
	 */
	public List<E> getIndexed(Indexer<E> indexer, Object index, int maxResults) // At least 7-10 % faster than the one with visit pages above.
	{
		List<E> retList = null;

		int ix = getIndexOfIndexer(indexer);
//		System.out.println("---");
		for (int i = 0; i < pageCount; i++) {
			retList = getPage(i).getIndexed(index, ix, maxResults, retList);
			if (retList != null && retList.size() >= maxResults)
				break;
//			System.out.println("not in page " + i);
		}
		return retList != null ? retList : Collections.<E>emptyList();
	}

	private int getIndexOfIndexer(Indexer<E> indexer)
	{
		for (int i = 0; i < indexers.length; i++) {
			 if (indexers[i] == indexer)
				 return i;
		}
		throw new IllegalArgumentException("Unknown indexer: " + indexer);
	}

	private final class GetIndexedVisitor extends PageVisitor<Page<E, E[]>>
	{
		private final Object index;
		private final int indexIx, maxResults;
		private List<E> list = null;

		GetIndexedVisitor(Object index, int indexIx, int maxResults)
		{
			this.index = index;
			this.indexIx = indexIx;
			this.maxResults = maxResults;
		}

		public Page<E, E[]> visit(Page<E, E[]> page, int startOffset, int endOffset)
		{
			list = ((IndexedObjectPage<E>) page).getIndexed(index, indexIx, maxResults, list);
			if (list != null && list.size() >= maxResults)
				loopAgain = false;
			return null;
		}
	}
}
