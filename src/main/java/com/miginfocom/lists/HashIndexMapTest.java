package com.miginfocom.lists;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 26/10/13
 *         Time: 14:22 PM
 */
public class HashIndexMapTest
{
	@Test
	public void testIt()
	{
		HashIndexMap2 map = new HashIndexMap2(512);
		map.insert(1, 12);
		map.insert(2, 21);
		map.insert(1, 11);
		map.insert(1, 10);
		map.insert(2, 20);
		map.insert(3, 31);
		map.insert(3, 32);
		map.insert(3, 30);
		map.insert(1, 15);
		map.insert(1, 14);
		map.insert(0, 5);

//		System.out.println(map);
//		System.out.println("");
//		System.out.println("0: " + Arrays.toString(map.getOffsets(0)));
//		System.out.println("1: " + Arrays.toString(map.getOffsets(1)));
//		System.out.println("2: " + Arrays.toString(map.getOffsets(2)));
//		System.out.println("3: " + Arrays.toString(map.getOffsets(3)));
//		System.out.println("4: " + Arrays.toString(map.getOffsets(4)));

		Assert.assertArrayEquals(new int[] {5}, map.getOffsets(0));
		Assert.assertArrayEquals(new int[] {10, 11, 12, 14, 15}, map.getOffsets(1));
		Assert.assertArrayEquals(new int[] {20, 21}, map.getOffsets(2));
		Assert.assertArrayEquals(new int[] {30, 31, 32}, map.getOffsets(3));
		Assert.assertArrayEquals(new int[] {}, map.getOffsets(4));

		map.remove(1, 12);
		map.remove(2, 21);
		map.remove(1, 11);
		map.remove(1, 10);
		map.remove(2, 20);
		map.remove(3, 31);
		map.remove(3, 32);
		map.remove(3, 30);
		map.remove(1, 15);
		map.remove(1, 14);
		map.remove(0, 5);

		Assert.assertArrayEquals(new int[] {}, map.getOffsets(0));
		Assert.assertArrayEquals(new int[] {}, map.getOffsets(1));
		Assert.assertArrayEquals(new int[] {}, map.getOffsets(2));
		Assert.assertArrayEquals(new int[] {}, map.getOffsets(3));
		Assert.assertArrayEquals(new int[] {}, map.getOffsets(4));
		Assert.assertEquals(0, map.size());
	}

	@Test
	public void testRemoveOffsets()
	{
		HashIndexMap2 map = new HashIndexMap2(512);
		map.insert(1, 12);
		map.insert(2, 21);
		map.insert(1, 11);
		map.insert(1, 10);
		map.insert(2, 20);
		map.insert(3, 31);
		map.insert(3, 32);
		map.insert(3, 30);
		map.insert(1, 15);
		map.insert(1, 14);
		map.insert(0, 5);

		map.removeOffsets(11, 31);

		Assert.assertArrayEquals(new int[] {5}, map.getOffsets(0));
		Assert.assertArrayEquals(new int[] {10}, map.getOffsets(1));
		Assert.assertArrayEquals(new int[] {}, map.getOffsets(2));
		Assert.assertArrayEquals(new int[] {31, 32}, map.getOffsets(3));
		Assert.assertArrayEquals(new int[] {}, map.getOffsets(4));

//		System.out.println(map);
//		System.out.println("");
//		System.out.println("0: " + Arrays.toString(map.getOffsets(0)));
//		System.out.println("1: " + Arrays.toString(map.getOffsets(1)));
//		System.out.println("2: " + Arrays.toString(map.getOffsets(2)));
//		System.out.println("3: " + Arrays.toString(map.getOffsets(3)));
//		System.out.println("4: " + Arrays.toString(map.getOffsets(4)));
	}
}
