package com.miginfocom.lists;

import java.util.Arrays;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 2013-10-22
 *         Time: 22:00
 */
public final class MigListFlat<E> extends MigListBase<E, int[]>
{
	private final Serializer<E> serializer;
	private final int objSize;

	public MigListFlat(Serializer<E> serializer)
	{
		this.serializer = serializer;
		this.objSize = serializer.intsRequired();
	}

	@SafeVarargs
	public MigListFlat(Serializer<E> serializer, E... elements)
	{
		this.serializer = serializer;
		this.objSize = serializer.intsRequired();
		addAll(Arrays.asList(elements));
	}

//	public int getInt(long ix, int serialIx)
//	{
//		FlatPage<T> page = (FlatPage<T>) pages[getPage(ix)];
//		return page.readInt(serialIx);
//	}

	Page<E, int[]> createPage()
	{
		return new FlatPage<>(serializer, pageSize);
	}

	Page<E, int[]> createPage(E[] arr, int size)
	{
		return new FlatPage<>(serializer, arr, size);
	}
}
