package com.miginfocom.lists;


import java.lang.reflect.Array;
import java.util.*;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 2013-09-24
 *         Time: 17:10
 */
public class MigListBase<E, ARR> implements MigList<E>
{
	private static final Page[] NO_PAGES = {};
	private static final long[] NO_STARTS = {};

//	private static final int PAGE_SIZE_ROR = 1; // 2
//	private static final int PAGE_SIZE_ROR = 3; // 8
//	private static final int PAGE_SIZE_ROR = 4; // 16
//	private static final int PAGE_SIZE_ROR = 5; // 32
//	private static final int PAGE_SIZE_ROR = 8; // 256
	private static final int PAGE_SIZE_ROR = 9; // 512
//	private static final int PAGE_SIZE_ROR = 10; // 1024
//	private static final int PAGE_SIZE_ROR = 13; // 8192
//	private static final int PAGE_SIZE_ROR = 15;// 32768

	static final int PAGE_SIZE = (1 << PAGE_SIZE_ROR);
	static {
		System.out.println("Page size: " + PAGE_SIZE);
	}

	final int pageSize = PAGE_SIZE;
	long size = 0;
	public int pageCount = 0;      // The number of pages in the array.

	@SuppressWarnings("unchecked")
	Page<E, ARR>[] pages;   // Elements NEVER null. Array can be empty though.
	long[] pageStarts;      // The start index of the corresponding pages.

	public MigListBase()
	{
		pages = NO_PAGES;
		pageStarts = NO_STARTS;
	}

	public MigListBase(Collection<? extends E> startCol)
	{
		this((E[]) startCol.toArray());
	}

	@SafeVarargs
	public MigListBase(E... startArray)
	{
		size = startArray.length;

		if (size == 0) {
			pages = NO_PAGES;
			pageStarts = NO_STARTS;
			return;
		}

		pageCount = size > 0 ? (int) ((size - 1) / pageSize) + 1 : 0;
		pages = new Page[pageCount];
		pageStarts = new long[pageCount];

		int pageStart = 0;
		int filledPageCount = (int) (size / pageSize);
		for (int pageIx = 0; pageIx < filledPageCount; pageIx++) {
			pageStarts[pageIx] = pageStart;
			pages[pageIx] = createPage(Arrays.copyOfRange(startArray, pageStart, (pageStart += pageSize)), pageSize); // todo Move the copyOfRange calls out so we can create flat view of a part of the original array instead of copy in two steps.
		}

		// Handle the last page separately so the condition doesn't have to be evaluated every loop iteration.
		int lastPageIx = pageCount - 1;
		int lastPageSize = (int) (size % pageSize);
		if (lastPageSize > 0) {
			pageStarts[lastPageIx] = pageStart;
			pages[lastPageIx] = createPage(Arrays.copyOfRange(startArray, pageStart, (pageStart += pageSize)), lastPageSize);
		}
	}

	private MigListBase(MigListBase<E, ARR> original)
	{
		size = original.size;
		pageCount = original.pageCount;

		pages = Arrays.copyOf(original.pages, pageCount);
		pageStarts = Arrays.copyOf(original.pageStarts, pageCount);
		for (Page page : pages)
			page.addSharer();
	}

	Page<E, ARR> createPage()
	{
//		return (Page<E, ARR>) new ObjectPageRWWrap<>(new ObjectPage<>(pageSize));
//		return (Page<E, ARR>) new ObjectPageSync<>(pageSize);
//		return (Page<E, ARR>) new ObjectPageRW<>(pageSize);
		return (Page<E, ARR>) new ObjectPage<>(pageSize);
//		return (Page<E, ARR>) new IndexedObjectPage(pageSize, null, false);
	}

	/** Creates a page with the specified array (overtaken) and size.
	 * @param arr The array. Is overtaken and should not be used by the caller again. This is the page's full data set.
	 * @param size The size.
	 * @return The new page.
	 */
	Page<E, ARR> createPage(E[] arr, int size)
	{
//		return (Page<E, ARR>) new ObjectPageRWWrap<>(new ObjectPage<>(size, arr));
//		return (Page<E, ARR>) new ObjectPageSync<>(size, arr);
//		return (Page<E, ARR>) new ObjectPageRW<>(size, arr);
		return (Page<E, ARR>) new ObjectPage<>(size, arr);
//		return (Page<E, ARR>) new IndexedObjectPage<>(size, arr, null, false);
	}

	// ***** Views **********************************************

	public MigList<E> getCOWView()
	{
		return new MigListBase<>(this);
	}

	// ***** Read **********************************************

	public E get(int ix)
	{
		if (pageCount == 1) {
			if (ix >= size)
				throw new IndexOutOfBoundsException(ix + " >= size: " + size);

			return pages[0].get(ix);
		} else {
			int pageIx = getPage(ix);
			return pages[pageIx].get((int) (ix - pageStarts[pageIx]));
		}
	}

	public E get(long ix)
	{
		if (pageCount == 1) {
			if (ix >= size)
				throw new IndexOutOfBoundsException(ix + " >= size: " + size);

			return pages[0].get((int) ix);
		} else {
			int pageIx = getPage(ix);
			return pages[pageIx].get((int) (ix - pageStarts[pageIx]));
		}
	}

	public int size()
	{
		return toInt(size);
	}

	public long sizeLong()
	{
		return size;
	}

	public boolean isEmpty()
	{
		return size == 0;
	}

	public boolean contains(Object o)
	{
		for (int pageIx = 0; pageIx < pageCount; pageIx++) {
			Page page = pages[pageIx];
			if (page.offsetOf(o, 0, page.size()) != -1)
				return true;
		}
		return false;
	}

	public boolean contains(Object o, long startIndex, long endIndex)
	{
		return indexOf(o, startIndex, endIndex) != -1;
	}

	public boolean containsAll(Collection<?> col)
	{
		for (Object o : col) {
			if (!contains(o))
				return false;
		}
		return true;
	}

	public boolean containsAll(Collection<?> col, long startIndex, long endIndex)
	{
		for (Object o : col) {
			if (!contains(o, startIndex, endIndex))
				return false;
		}
		return true;
	}

	public int indexOf(Object o)
	{
		return toInt(indexOfLong(o));
	}

	public long indexOfLong(Object o)
	{
		for (int pageIx = 0; pageIx < pageCount; pageIx++) {
			Page page = pages[pageIx];
			int offset = page.offsetOf(o, 0, page.size());
			if (offset != -1)
				return offset + pageStarts[pageIx];
		}
		return -1;
	}

	public long indexOf(Object o, long startIndex, long endIndex)
	{
		return indexOfImpl(o, startIndex, endIndex, false);
	}

	public int lastIndexOf(Object o)
	{
		return toInt(lastIndexOfLong(o));
	}

	public long lastIndexOfLong(Object o)
	{
		for (int pageIx = pageCount - 1; pageIx >= 0; pageIx--) {
			Page page = pages[pageIx];
			int offset = page.lastOffsetOf(o, 0, page.size());
			if (offset != -1)
				return offset + pageStarts[pageIx];
		}
		return -1;
	}

	public long lastIndexOf(Object o, long startIndex, long endIndex)
	{
		return indexOfImpl(o, startIndex, endIndex, true);
	}

	public long indexOfImpl(Object o, long startIndex, long endIndex, boolean reversed)
	{
		IndexOfVisitor<E, ARR> visitor = new IndexOfVisitor<>(o, reversed);
		int foundPageIx = reversed ? visitPagesReversed(visitor, startIndex, endIndex) : visitPages(visitor, startIndex, endIndex);
		int pageOffset = visitor.pageOffset;

		return pageOffset != -1 ? pageOffset + pageStarts[foundPageIx] : -1;
	}

	public Iterator<E> iterator()
	{
		return new MigIterator();
	}

	public MigIterator iterator(long startIndex)
	{
		return new MigIterator(startIndex, size);
	}

	public MigIterator iterator(long startIndex, long endIndex)
	{
		return new MigIterator(startIndex, endIndex);
	}

	public MigListIterator<E> listIterator()
	{
		return new MigListIterator<>(this, 0);
	}

	public MigListIterator<E> listIterator(int startIndex)
	{
		return new MigListIterator<>(this, startIndex);
	}

	public MigListIterator<E> listIterator(long startIndex)
	{
		return new MigListIterator<>(this, startIndex);
	}

	public MigListIterator<E> listIterator(long startIndex, long endIndex)
	{
		return new MigListIterator<>(this, startIndex, endIndex);
	}

	public MigList<E> subList(int fromIndex, int toIndex)
	{
		return subList((long) fromIndex, (long) toIndex);
	}

	public MigList<E> subList(long fromIndex, long toIndex)
	{
		return new MigListView<>(this, fromIndex, toIndex);
	}

	public Object[] toArray()
	{
		Object[] arr = new Object[toInt(size)];
		copyToArray(arr);
		return arr;
	}

	public <T> T[] toArray(T[] arr)
	{
		if (arr.length < size) {
			arr = (T[]) Array.newInstance(arr.getClass().getComponentType(), toInt(size));
		} else if (arr.length > size) {
			arr[toInt(size)] = null;
		}

		copyToArray(arr);

		return arr;
	}

	private void copyToArray(Object[] arr)
	{
		int ix = 0;
		for (int pageIx = 0; pageIx < pageCount; pageIx++) {
			Page page = pages[pageIx];
			page.toArray(arr, ix);
			ix += page.size();
		}
	}

	public <T> T[] toArray(T[] arr, long startIndex, long endIndex)
	{
		int len = toInt(endIndex - startIndex);

		if (arr.length < len) {
			arr = (T[]) Array.newInstance(arr.getClass().getComponentType(), len);
		} else if (arr.length > size) {
			arr[toInt(size)] = null;
		}

		visitPages(new ToArrayVisitor<E, ARR>((E[]) arr), startIndex, endIndex);

		return arr;
	}

	public <T> T[][] toArrays(T[] arr, int maxPageSize, long startIndex, long endIndex)
	{
		long len = endIndex - startIndex;

		T[][] retArr;
		if (len <= arr.length) { // It fits in the provided array

			retArr = (T[][]) Array.newInstance(arr.getClass().getComponentType(), 1, 0);
			retArr[0] = toArray(arr, startIndex, endIndex);

		} else if (len <= maxPageSize) {

			retArr = (T[][]) Array.newInstance(arr.getClass().getComponentType(), 1, (int) len);
			toArray(retArr[0], startIndex, endIndex);

		} else {

			int lastPageSize = (int) (len % maxPageSize); // last, as in last partial page.
			int pageCnt = (int) (len / maxPageSize) + (lastPageSize != 0 ? 1 : 0);

			retArr = (T[][]) Array.newInstance(arr.getClass().getComponentType(), pageCnt, maxPageSize); // Last page probably too large

			if (lastPageSize != 0) // Correct the last page
				retArr[retArr.length] = (T[]) Array.newInstance(arr.getClass().getComponentType(), lastPageSize);

			int aIx = 0;
			for (long index = startIndex; index < endIndex; index += maxPageSize) {
				T[] arrToFill = retArr[aIx++];
				visitPages(new ToArrayVisitor<E, ARR>((E[]) arrToFill), index, index + arrToFill.length);
			}
		}
		return retArr;
	}

	public Object[] toArray(long startIndex, long endIndex)
	{
		int len = toInt(endIndex - startIndex);
		Object[] arr = new Object[len];
		visitPages(new ToArrayVisitor<E, ARR>((E[]) arr), startIndex, startIndex + len);
		return arr;
	}

	// ***** Read & Write **********************************************

	public E set(int index, E element)
	{
		return set((long) index, element);
	}

	public E set(long index, E element)
	{
		int pageIx = getPage(index);
		int offset = (int) (index - pageStarts[pageIx]);
		Page<E, ARR> page = pages[pageIx];
		E old = page.get(offset);
		Page<E, ARR> modPage = page.set(offset, element);
		if (modPage != page)
			pages[pageIx] = modPage;

//		assertStructure();
		return old;
	}

	public boolean add(E object)
	{
		if (pageCount != 0) {
			int lastPage = pageCount - 1;

			Page<E, ARR> page = pages[lastPage];
			if (page.size() != pageSize) {
				Page<E, ARR> modPage = page.add(object);
				if (modPage != page)
					pages[lastPage] = modPage;
			} else {
				addPageLast().add(object);
			}
		} else {
			addPageLast().add(object);
		}
		size++;

//		assertStructure();
		return true;
	}

	public void add(int ix, E object)
	{
		add((long) ix, object);
	}

	public void add(long ix, E object)
	{
		if (ix == size) {
			add(object);
			return;
		}

		int pageIx = getPage(ix); // Will return the next page even though there are space before
		Page<E, ARR> page = pages[pageIx];

		int offset = (int) (ix - pageStarts[pageIx]);

		// If it's room in the page before it's better to add it there instead.
		if (offset == 0 && pageIx != 0 && pages[pageIx - 1].size() != pageSize) {
			page = pages[--pageIx];
			offset = (int) (ix - pageStarts[pageIx]);
		}

		if (!page.isFull()) {
			Page<E, ARR> modPage = page.add(offset, object);
			if (modPage != page)
				pages[pageIx] = modPage;
			addToPageStarts(pageIx + 1, 1);
		} else {
			page = splitPage(pageIx, offset);
			page.add(object);
			addToPageStarts(pageIx + 1, 1);
		}

		size++;
//		assertStructure();
	}

	public boolean addAll(Collection<? extends E> col)
	{
		if (col.isEmpty())
			return false;

		int firstPageIx = pageCount != 0 ? pageCount - 1 : 0;
		Page<E, ARR> page = pageCount != 0 ? pages[pageCount - 1] : addPageLast();
		Iterator<? extends E> it = col.iterator();
		Page<E, ARR> modPage = page.add(it);
		if (modPage != page)
			pages[pageCount - 1] = modPage;

		while(it.hasNext())
			addPageLast().add(it); // Will never have sharers so we don't need to handle replacing pages.

		size += col.size();
		recalcPageStarts(firstPageIx, pageCount - 1);

//		assertStructure();
		return true;
	}

	public boolean addAll(int index, Collection<? extends E> col)
	{
		return addAll((long) index, col);
	}

	public boolean addAll(long index, Collection<? extends E> col)
	{
		// todo Too slow.

		for (E object : col)
			add(index++, object);

//		assertStructure();
		return !col.isEmpty();
	}

	public boolean remove(Object o)
	{
		RemoveFirstVisitor<E, ARR> visitor = new RemoveFirstVisitor<>(o);
		visitPages(visitor, 0, size);
//		assertStructure();
		return visitor.removed;
	}

	public E remove(int ix)
	{
		return remove((long) ix);
	}

	public E remove(long ix)
	{
		int pageIx = getPage(ix);
		Page<E, ARR> page = pages[pageIx];

		int offset = (int) (ix - pageStarts[pageIx]);
		E old = page.get(offset);

		if (page.size() > 1) {
			Page<E, ARR> modPage = page.removeIx(offset);
			if (modPage != page)
				pages[pageIx] = modPage;

			addToPageStarts(pageIx + 1, -1);
		} else {

			// If 1 element we can just remove the page instead.
			int len = --pageCount - pageIx;
			if (len > 0) {
				System.arraycopy(pages, pageIx + 1, pages, pageIx, len);
				for (pageIx++; pageIx < pageCount; pageIx++)
					pageStarts[pageIx] = pageStarts[pageIx + 1] - 1;
			}
			pages[pageCount] = null;
		}
		size--;

//		assertStructure();
		return old;
	}

	public boolean remove(Object o, long startIndex, long endIndex)
	{
		RemoveFirstVisitor<E, ARR> visitor = new RemoveFirstVisitor<>(o);
		visitPages(visitor, startIndex, endIndex);
//		assertStructure();
		return visitor.removed;
	}

	public boolean removeAll(Collection<?> col)
	{
		return removeAll(col, 0, size) != 0;
	}

	public long removeAll(Collection<?> col, long startIndex, long endIndex)
	{
		long sizeBefore = size;
		for (Object o : col) // todo Visit pages once instead.
			visitPagesReversed(new RemoveAllVisitor<E, ARR>(o), startIndex, endIndex - (sizeBefore - size));

//		assertStructure();
		return sizeBefore - size;
	}

	public void removeRange(long startIndex, long endIndex)
	{
		visitPages(new RemoveRangeVisitor<E, ARR>(), startIndex, endIndex);
	}

	public boolean retainAll(Collection<?> col)
	{
		return retainAll(col, 0, size) != 0;
	}

	public long retainAll(Collection<?> col, long startIndex, long endIndex)
	{
		if (col == null)
			throw new NullPointerException("Collection is null");

		long sizeBefore = size;
		visitPages(new RetainAllVisitor<E, ARR>(col), startIndex, endIndex);
//		assertStructure();
		return sizeBefore - size;
	}

	public void clear()
	{
		pages = NO_PAGES;
		pageStarts = NO_STARTS;
		size = 0;
		pageCount = 0;
//		assertStructure();
	}

	// ***** Helper methods **********************************************

	/** Split a page into two pages.
	 * @param pageIx The page to split.
	 * @param offset Offset of the element to split the page before. Might be 0 but NOT after the page (i.e. not same as page size)
	 * @return The page that is guaranteed to not be full after the split and where the caller can insert an object that would
	 * en up in the same place as offset is before this call.
	 * If this page != the page that was at pageIx it is already set. It might be a new page because of defensive sharing and should
	 * be used instead of any cached page at pageIx.
	 * Not null.
	 */
	Page<E, ARR> splitPage(int pageIx, int offset)
	{
		ensurePageRefsCount(pageCount + 1);

		Page<E, ARR> page;
		if (offset != 0) { // Insert new page after split page and copy over elements after the split offset.
			int len = pageCount - pageIx - 1;
			System.arraycopy(pages, pageIx + 1, pages, pageIx + 2, len);
			page = pages[pageIx];
			Page<E, ARR>[] newPages = page.split(offset);
			Page<E, ARR> firstPage = newPages[0];
			if (page != firstPage) {
				pages[pageIx] = firstPage;
				page = firstPage;
			}
			pages[pageIx + 1] = newPages[1];
			recalcPageStarts(pageIx + 1, pageCount);

		} else { // Just create a new page before the split page
			System.arraycopy(pages, pageIx, pages, pageIx + 1, pageCount - pageIx);
			System.arraycopy(pageStarts, pageIx, pageStarts, pageIx + 1, pageCount - pageIx);
			page = createPage();
			pages[pageIx] = page;
		}
		pageCount++;
		return page;
	}

	Page<E, ARR> addPageLast()
	{
		ensurePageRefsCount(pageCount + 1);

		Page<E, ARR> page = createPage();
		if (pageCount > 0)
			pageStarts[pageCount] = pageStarts[pageCount - 1] + pages[pageCount - 1].size();
		pages[pageCount++] = page;

		return page;
	}

	private void ensurePageRefsCount(int minPageCount)
	{
		int arrLen = pages.length;
		if (arrLen < minPageCount) {
			int newSize = Math.max(minPageCount, arrLen + (arrLen >> 1) + 2);
			pages = Arrays.copyOf(pages, newSize);
			pageStarts = Arrays.copyOf(pageStarts, newSize);
		}
	}

	void removePage(int pageIx)
	{
		int len = --pageCount - pageIx;
		if (len > 0) {
			System.arraycopy(pages, pageIx + 1, pages, pageIx, len);
			System.arraycopy(pageStarts, pageIx + 1, pageStarts, pageIx, len);
		}

		pages[pageCount] = null;
	}

	/** Returns the page index that ix is in.
	 * @param ix Needs to be a valid index.
	 * @return Always a valid page index if.
	 * @throws IndexOutOfBoundsException if ix < 0 or >= size
	 */
	final int getPage(long ix)
	{
		rangeCheck(ix);

		int lastPage = pageCount - 1;
		int pageIx = (int) (ix >> PAGE_SIZE_ROR);
		if (pageIx > lastPage)
			pageIx = lastPage;

		while (ix < pageStarts[pageIx])
			pageIx--;

		while (pageIx++ < lastPage && ix >= pageStarts[pageIx])
			;

		return pageIx - 1;
	}

	void recalcPageStarts(int fromPageIx, int toPageIx)
	{
		long pageEnd = fromPageIx != 0 ? pageStarts[fromPageIx - 1] + pages[fromPageIx - 1].size() : 0;
		for (; fromPageIx <= toPageIx; fromPageIx++) {
			pageStarts[fromPageIx] = pageEnd;
			pageEnd += pages[fromPageIx].size();
		}
	}

	void addToPageStarts(int fromIx, long delta)
	{
		for (; fromIx < pageCount; fromIx++)
			pageStarts[fromIx] += delta;
	}

	private void rangeCheck(int index)
	{
		if (index < 0 || index >= size)
			throw new IndexOutOfBoundsException("Index out of bounds: " + index + ". Size: " + size);
	}

	private void rangeCheck(long index)
	{
		if (index < 0 || index >= size)
			throw new IndexOutOfBoundsException("Index out of bounds: " + index + ". Size: " + size);
	}

	public int hashCode()
	{
		int hashCode = 1;
		for (E e : this)
			hashCode = 31 * hashCode + (e != null ? e.hashCode() : 0);
		return hashCode;
	}

	public boolean equals(Object other)
	{
		if (other == this)
			return true;

		if (!(other instanceof List))
			return false;

		List oList = (List) other;
		if (oList.size() != size)
			return false;

		ListIterator it1 = listIterator();
		ListIterator it2 = ((List) other).listIterator();

		while (it1.hasNext()) {
			Object o1 = it1.next();
			Object o2 = it2.next();
			if (!(o1 == null ? o2 == null : o1.equals(o2)))
				return false;
		}
		return true;
	}

	static int toInt(long l)
	{
		return (int) Math.min(Integer.MAX_VALUE, l);
	}


	// Page Visitors ************************************************************************************************************

	/** Visits all pages between thos that contain <code>startIndex</code> and <code>endIndex</code> (excl)
	 * @param visitor
	 * @param startIndex
	 * @param endIndex
	 * @return The last index visited or -1 if none was. Only correct for non-modifying visitors.
	 */
	protected int visitPagesReadOnly(PageVisitor<Page<E, ARR>> visitor, long startIndex, long endIndex)
	{
		if (startIndex >= endIndex)
			return -1;

		int pageIx = getPage(startIndex);
		int startOffset = (int) (startIndex - pageStarts[pageIx]);

		while (visitor.loopAgain && pageIx < pageCount) {
			long endOffsetL = endIndex - pageStarts[pageIx];
			if (endOffsetL <= 0)
				break;

			Page<E, ARR> page = pages[pageIx++];

			int pageSize = page.size();
			int endOffset = endOffsetL > pageSize ? pageSize : (int) endOffsetL;

			visitor.visit(page, startOffset, endOffset);

			startOffset = 0;
		}

//		assertStructure();

		return pageIx - 1;
	}

	/** Visits all pages between thos that contain <code>startIndex</code> and <code>endIndex</code> (excl)
	 * @param visitor
	 * @param startIndex
	 * @param endIndex
	 * @return The last index visited or -1 if none was. Only correct for non-modifying visitors.
	 */
	protected int visitPages(PageVisitor<Page<E, ARR>> visitor, long startIndex, long endIndex)
	{
		if (startIndex >= endIndex)
			return -1;

		int pageIx = getPage(startIndex);

		int startOffset = (int) (startIndex - pageStarts[pageIx]);
		long accSizeDelta = 0;
		while (visitor.loopAgain && pageIx < pageCount) {
			long endOffsetL = endIndex - pageStarts[pageIx];
			if (endOffsetL <= 0)
				break;

			Page<E, ARR> page = pages[pageIx];
			int oldPageSize = page.size();

			int endOffset = endOffsetL > oldPageSize ? oldPageSize : (int) endOffsetL;

			Page<E, ARR> modPage = visitor.visit(page, startOffset, endOffset);
			if (modPage != null) {
				if (modPage != page) // Got a new copy? Use that if so.
					pages[pageIx] = modPage;

				int newPageSize = modPage.size();
				if (newPageSize != oldPageSize) {
					int delta = newPageSize - oldPageSize;
					accSizeDelta += delta;
					endIndex += delta;
					if (newPageSize == 0)
						removePage(pageIx--);
				}
			}

			if (accSizeDelta != 0 && pageIx < pageCount - 1)
				pageStarts[pageIx + 1] += accSizeDelta;

			startOffset = 0;
			pageIx++;
		}

		if (accSizeDelta != 0) {
			size += accSizeDelta;
			addToPageStarts(pageIx + 1, accSizeDelta);
		}

//		assertStructure();

		return pageIx - 1;
	}

	/**
	 * @param visitor
	 * @param startIndex
	 * @param endIndex
	 * @return The last index visited or -1 if none was.
	 */
	protected int visitPagesReversed(PageVisitor<Page<E, ARR>> visitor, long startIndex, long endIndex)
	{
		if (startIndex >= endIndex)
			return -1;

		int firstPageIx = getPage(startIndex);
		int lastPageIx = getPage(endIndex - 1);

		int pageIx = lastPageIx;
		int firstChangedPageIx = -1;
		for (; pageIx >= firstPageIx; pageIx--) {
			Page<E, ARR> page = pages[pageIx];

			int startOffset = pageIx != firstPageIx ? 0 : (int) (startIndex - pageStarts[pageIx]);
			int endOffset = pageIx != lastPageIx ? page.size() : (int) (endIndex - pageStarts[pageIx]);
			int sizeBefore = page.size();

			Page<E, ARR> modPage = visitor.visit(page, startOffset, endOffset);
			if (modPage != null) {
				if (modPage != page)
					pages[pageIx] = modPage;

				int newPageSize = modPage.size();
				int sizeDelta = newPageSize - sizeBefore;
				if (sizeDelta != 0) {
					size += sizeDelta;
					firstChangedPageIx = pageIx;
					if (newPageSize == 0)
						removePage(pageIx);
				}
			}

			if (!visitor.loopAgain)
				break;
		}

		if (firstChangedPageIx != -1 && pageCount > 0)
			recalcPageStarts(firstChangedPageIx, pageCount - 1);

//		assertStructure();

		return pageIx;
	}

	static final class RemoveRangeVisitor<T, A> extends PageVisitor<Page<T, A>>
	{
		public Page<T, A> visit(Page<T, A> page, int startOffset, int endOffset)
		{
			return page.removeRange(startOffset, endOffset);
		}
	}

	static final class ToArrayVisitor<T, A> extends PageVisitor<Page<T, A>>
	{
		private final T[] array;
		private int curIx = 0;

		ToArrayVisitor(T[] array)
		{
			this.array = array;
		}

		public Page<T, A> visit(Page<T, A> page, int startOffset, int endOffset)
		{
			page.toArray(array, curIx, startOffset, endOffset);
			curIx += (endOffset - startOffset);
			return null;
		}
	}

	static final class RetainAllVisitor<T, A> extends PageVisitor<Page<T, A>>
	{
		private final Collection<?> col;

		RetainAllVisitor(Collection<?> col)
		{
			this.col = col;
		}

		public Page<T, A> visit(Page<T, A> page, int startOffset, int endOffset)
		{
			return page.retainAll(col, startOffset, endOffset);
		}
	}

	static final class RemoveFirstVisitor<T, A> extends PageVisitor<Page<T, A>>
	{
		private final Object object;
		boolean removed = false;

		RemoveFirstVisitor(Object object)
		{
			this.object = object;
		}

		public Page<T, A> visit(Page<T, A> page, int startOffset, int endOffset)
		{
			Page<T, A> modPage = page.removeFirst(object, startOffset, endOffset);

			if (modPage != null) {
				removed = true;
				loopAgain = false;
			}
			return modPage;
		}
	}

	static final class RemoveAllVisitor<T, A> extends PageVisitor<Page<T, A>>
	{
		private final Object object;

		RemoveAllVisitor(Object object)
		{
			this.object = object;
		}

		public Page<T, A> visit(Page<T, A> page, int startOffset, int endOffset)
		{
			return page.removeAll(object, startOffset, endOffset);
		}
	}

	static final class IndexOfVisitor<T, A> extends PageVisitor<Page<T, A>>
	{
		private final boolean reversed;
		private final Object object;
		int pageOffset = -1;

		IndexOfVisitor(Object object, boolean reversed)
		{
			this.reversed = reversed;
			this.object = object;
		}

		public Page<T, A> visit(Page<T, A> page, int startOffset, int endOffset)
		{
			int offset = reversed ? page.lastOffsetOf(object, startOffset, endOffset) : page.offsetOf(object, startOffset, endOffset);
			if (offset != -1) {
				pageOffset = offset;
				loopAgain = false;
			}
			return null;
		}
	}

	public final class MigCOWIterator implements Iterator<E>
	{
		private final Page<E, ARR>[] localPages;
		private int curPageIx = 0, curOffset;
		private int toPageIx; // exclusive
		private Page<E, ARR> curPage;

		private long elementsLeft;

		MigCOWIterator()
		{
			this.localPages = Arrays.copyOfRange(pages, 0, pageCount);
			this.elementsLeft = size;

			curPage = localPages.length > 0 ? localPages[curPageIx] : null;
			curOffset = 0;
			toPageIx = pageCount;

			addSharesToPages();
		}

		MigCOWIterator(long fromIndex, long endIndex)
		{
			if (fromIndex < 0 || fromIndex > endIndex || endIndex > sizeLong())
				throw new IndexOutOfBoundsException("fromIndex: " + fromIndex + ", endIndex: " + endIndex + " for size: " + sizeLong());

			int startPageIx = getPage(fromIndex);
			toPageIx = getPage(endIndex - 1) + 1;
			this.localPages = (toPageIx - startPageIx) > 0 ? Arrays.copyOfRange(pages, startPageIx, toPageIx) : null;
			this.elementsLeft = endIndex - fromIndex;

			curPage = localPages != null ? localPages[curPageIx] : null;
			curOffset = (int) (fromIndex - pageStarts[startPageIx]);

			addSharesToPages();
		}

		private void addSharesToPages()
		{
			for (Page page : localPages)
				page.addSharer();
		}

		public boolean hasNext()
		{
			return elementsLeft != 0;
		}

		public E next()
		{
			if (elementsLeft <= 0)
				throw new NoSuchElementException();
			elementsLeft--;

			E retObject = curPage.get(curOffset++);

			if (curOffset == curPage.size()) {
				curOffset = 0;
				curPage.removeSharer();
				curPageIx++;
				curPage = curPageIx < localPages.length ? localPages[curPageIx] : null;
			}

			return retObject;
		}

		/** Disposes of the iterator. It can not be used after this call and {@link #hasNext()} will return false.
		 * If there exist any pages that would've been iterated over they are released from sharing so that not
		 * to impose copying of those pages needlessly.
		 */
		public void dispose()
		{
			elementsLeft = 0;
			for (int i = curPageIx; i < toPageIx; i++)
				localPages[i].removeSharer();
		}

		/** Use {@link MigList#listIterator()} for an iterator that can change the list.
		 * This iterator is mostly for for-each types of iterations and it can be made
		 * slightly faster without remove support.
		 * @throws UnsupportedOperationException
		 */
		public void remove()
		{
			throw new UnsupportedOperationException();
		}
	}

	/** A normal iterator that will return undefined results if the list it iterates is changed.
	 * The iterator is not thread safe.
	 */
	public final class MigIterator implements Iterator<E>
	{
		private int curPageIx = 0, curOffset = 0;
		private Page<E, ARR> curPage;
		private long elementsLeft;

		MigIterator()
		{
			elementsLeft = size;
			curPage = pages.length > 0 ? pages[curPageIx] : null;
		}

		MigIterator(long fromIndex, long endIndex)
		{
			if (fromIndex < 0 || fromIndex > endIndex || endIndex > sizeLong())
				throw new IndexOutOfBoundsException("fromIndex: " + fromIndex + ", endIndex: " + endIndex + " for size: " + sizeLong());

			this.elementsLeft = endIndex - fromIndex;

			curPage = pages.length > 0 ? pages[curPageIx] : null;
			curOffset = fromIndex != 0 ? (int) (fromIndex - pageStarts[getPage(fromIndex)]) : 0;
		}

		public boolean hasNext()
		{
			return elementsLeft != 0;
		}

		public E next()
		{
			if (elementsLeft == 0)
				throw new NoSuchElementException();
			elementsLeft--;

			E retObject = curPage.get(curOffset++);

			if (curOffset == curPage.size()) {
				curOffset = 0;
				curPage = ++curPageIx < pages.length ? pages[curPageIx] : null;
			}

			return retObject;
		}

		/** Use {@link MigList#listIterator()} for an iterator that can change the list.
		 * This iterator is mostly for for-each types of iterations and it can be made
		 * slightly faster without remove support.
		 * @throws UnsupportedOperationException
		 */
		public void remove()
		{
			throw new UnsupportedOperationException();
		}
	}

	// Debug ************************************************************************************************************


	final void assertStructure()
	{
		if (true)
			return;

		long ix = 0;
		for (int pageIx = 0; pageIx < pages.length; pageIx++) {
			Page page = pages[pageIx];

			if (page != null & pageIx >= pageCount)
				throw new AssertionError("page != null at " + pageIx + getPagesAroundString(pageIx));

			if (pageIx < pageCount && page == null)
				throw new AssertionError("Page is null. ix: " + pageIx + getPagesAroundString(pageIx));

			if (pageIx < pageCount && pageStarts[pageIx] != ix)
				throw new AssertionError("Page index error. ix: " + pageIx + ", " + page + getPagesAroundString(pageIx));

			if (pageIx < pageCount && pageIx > 0 && page.size() == 0)
				throw new AssertionError("Page size 0: " + pageIx + " in page: " + page + getPagesAroundString(pageIx));

			if (page != null) {
				if (page.size() > page.elementCountDebug())
					throw new AssertionError("Page length error. ix: " + pageIx + ", " + page + getPagesAroundString(pageIx));

				if (!(page instanceof FlatPage)) {
					for (int i = page.size(); i < page.elementCountDebug(); i++) {
						if (page.get(i) != null)
							throw new AssertionError("Non-null element at " + i + " in page: " + page + getPagesAroundString(pageIx));
					}
				}

				ix += page.size();
			}
		}
		if (ix != size)
			throw new AssertionError(ix + " elements for size " + size);
	}

	private String getPagesAroundString(int pageIx)
	{
		StringBuilder sb = new StringBuilder();
		sb.append('\n');

		for (int pIx = Math.max(0, pageIx - 10); pIx < Math.min(pages.length, pageIx + 10); pIx++)
			sb.append(pIx).append(": ").append(getPageString(pIx)).append('\n');

		sb.append("\npageCount: ").append(pageCount);

		return sb.toString();
	}

	private String getPageString(int pageIx)
	{
		if (pageIx < 0)
			return "none";

		if (pageIx >= pages.length)
			return "none";

		Page page = pages[pageIx];
		if (page == null)
			return "null";

		long pageStart = pageStarts[pageIx];
		return pageStart + "-" + (pageStart + page.size()) + " (" + page.size() + ")" + " arraySize: " + page.elementCountDebug();
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder(pages.length * 100);
		sb.append('[');
		for (E e : this)
			sb.append(e).append(", ");

		int len = sb.length();
		if (len > 2)
			sb.delete(len - 2, len);

		sb.append(']');
		return sb.toString();
	}

	public String toStringDebug()
	{
		StringBuilder sb = new StringBuilder(pages.length * 100);

		for (int i = 0; i < pages.length; i++) {
			Page page = pages[i];
			if (page == null) {
				sb.append(i).append(": null\n");
//			} else {
//				sb.append(i).append(": ").append(page.start).append('-').append(page.start + page.size).append(" (").append(page.size).append(")  ").append(Arrays.asList(page.array).subList(0, Math.min(20, page.arraySizeDebug()))).append('\n');
			}
		}
		return sb.toString();
	}

	public double getFillPercent()
	{
		return size / (double) (pageCount * PAGE_SIZE);
	}
}
