package com.miginfocom.lists;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 10/3/13
 *         Time: 23:29 PM
 */
abstract class PageVisitor<E extends MigList.Page>
{
	protected boolean loopAgain = true;

	/**
	 * @param page The page to visit. Never null
	 * @param startOffset The start offset into the page that should  be visited.
	 * @param endOffset The end offset into the page that should  be visited, exclusive.
	 * @return If a new page (!= <code>page</code>) then the page sent in should be exchanged
	 * with this one. null is returned if the page wasn't modified. <code>page</code> is returned
	 * if the page was modified but didn't need to be replaced.
	 */
	public abstract E visit(E page, int startOffset, int endOffset);
}
