package com.miginfocom.lists;

import java.util.Objects;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 05/11/13
 *         Time: 23:36 PM
 */
public abstract class Indexer<E>
{
	/**
	 * @param e The object to index. Might be null.
	 * @return May be null.
	 */
	public abstract Object index(E e);

	public boolean indexEquals(Object index, E e)
	{
		return Objects.equals(index, index(e));
	}
}
