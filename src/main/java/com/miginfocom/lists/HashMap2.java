package com.miginfocom.lists;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 13/11/13
 *         Time: 22:17 PM
 */
public final class HashMap2
{
	private final Object[] array;
	private final int bits;

	public HashMap2(int bits)
	{
		this.bits = bits + 1;
		this.array = new Object[1 << this.bits];
	}

	private HashMap2(int hash, Object startKey, Object startObj, int bits)
	{
		this.bits = ++bits;
		this.array = new Object[1 << bits];
		int ix = getIx(hash);
		this.array[ix] = startKey; // It's empty for sure.
		this.array[ix + 1] = startObj; // It's empty for sure.
	}

	public void put(int hash, Object key, Object o)
	{
		int ix = getIx(hash);

		Object arrKey = array[ix];

		if (arrKey == null) {
			array[ix] = key;
			array[ix + 1] = o;
		} else if (arrKey.getClass() == HashMap2.class) {
			((HashMap2) arrKey).put(hash << bits, key, o);
		} else {
			//			array[ix] = new HashMap2(hash << bits, o, Math.max(2, bits >> 1));
			array[ix] = new HashMap2(hash << bits, key, o, Math.max(2, bits - 8));
		}
	}

	public Object get(int hash, Object key)
	{
		int ix = getIx(hash);

		Object o = array[ix];

		if (o == null) {
			return null;
		} else if (o.getClass() == HashMap2.class) {
			return ((HashMap2) o).get(hash << bits, key);
		} else {
			return key.equals(array[ix]) ? o : null;
		}
	}

	private int getIx(int hash)
	{
		return hash >>> (33 - bits);
	}

	private void getSlots(AtomicLong count)
	{
		count.addAndGet(array.length);

		for (Object o : array) {
			if (o != null && o.getClass() == HashMap2.class)
				((HashMap2) o).getSlots(count);
		}
	}
}