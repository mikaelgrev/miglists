package com.miginfocom.lists;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 2013-10-21
 *         Time: 19:16
 */
public interface Serializer<T>
{
	/**
	 * @param object The object to write. The reason it's not <code>T</code> is that some methods
	 * accept Object as a parameter, e.g. {@link java.util.List#indexOf(Object)} and that needs to
	 * be converted if it's a <code>T</code>.
	 * @param pipe The pipe to write to.
	 * @return If the object could be written.
	 */
	public abstract boolean writeObject(Object object, WritePipe<T> pipe); // todo Can this dual write be avoided?

	public abstract void write(T object, WritePipe<T> pipe);

	public abstract T read(ReadPipe<T> pipe);

	public abstract int intsRequired();
}
