package com.miginfocom.lists;

import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 2013-10-17
 *         Time: 10:47
 */
public final class ObjectPageRWWrap<T> implements MigList.Page<T, T[]>
{
	private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
	private final MigList.Page<T, T[]> page;

	public ObjectPageRWWrap(MigList.Page<T, T[]> page)
	{
		this.page = page;
	}

	public MigList.Page<T, T[]> copyPage()
	{
		return new ObjectPageRWWrap<>(page.copyPage());
	}

	public int size()
	{
		lock.readLock().lock();
		try {
			return page.size();
		} finally {
			lock.readLock().unlock();
		}
	}

	public boolean isFull()
	{
		lock.readLock().lock();
		try {
			return page.isFull();
		} finally {
			lock.readLock().unlock();
		}
	}

	public void removeSharer()
	{
		lock.writeLock().lock();
		try {
			page.removeSharer();
		} finally {
			lock.writeLock().unlock();
		}
	}

	public int getSharers()
	{
		lock.readLock().lock();
		try {
			return page.getSharers();
		} finally {
			lock.readLock().unlock();
		}
	}

	public void addSharer()
	{
		lock.writeLock().lock();
		try {
			page.addSharer();
		} finally {
			lock.writeLock().unlock();
		}
	}

	public T get(int offset)
	{
		lock.readLock().lock();
		try {
			return page.get(offset);
		} finally {
			lock.readLock().unlock();
		}
	}

	public int offsetOf(Object o, int startOffset, int endOffset)
	{
		lock.readLock().lock();
		try {
			return page.offsetOf(o, startOffset, endOffset);
		} finally {
			lock.readLock().unlock();
		}
	}

	public int lastOffsetOf(Object o, int startOffset, int endOffset)
	{
		lock.readLock().lock();
		try {
			return page.lastOffsetOf(o, startOffset, endOffset);
		} finally {
			lock.readLock().unlock();
		}
	}

	public void toArray(T[] destArr, int destOffset)
	{
		lock.readLock().lock();
		try {
			page.toArray(destArr, destOffset);
		} finally {
			lock.readLock().unlock();
		}
	}

	public void toArray(T[] destArr, int destOffset, int startOffset, int endOffset)
	{
		lock.readLock().lock();
		try {
			page.toArray(destArr, destOffset, startOffset, endOffset);
		} finally {
			lock.readLock().unlock();
		}
	}

	public int elementCountDebug()
	{
		lock.readLock().lock();
		try {
			return page.elementCountDebug();
		} finally {
			lock.readLock().unlock();
		}
	}

	//************ Write *************************************


	public MigList.Page<T, T[]> set(int offset, T element)
	{
		lock.writeLock().lock();
		try {
			return page.set(offset, element);
		} finally {
			lock.writeLock().unlock();
		}
	}

	public MigList.Page<T, T[]> add(T element)
	{
		lock.writeLock().lock();
		try {
			return page.add(element);
		} finally {
			lock.writeLock().unlock();
		}
	}

	public MigList.Page<T, T[]> add(int offset, T element)
	{
		lock.writeLock().lock();
		try {
			return page.add(offset, element);
		} finally {
			lock.writeLock().unlock();
		}
	}

	public MigList.Page<T, T[]> add(Iterator<? extends T> source)
	{
		lock.writeLock().lock();
		try {
			return page.add(source);
		} finally {
			lock.writeLock().unlock();
		}
	}

	public MigList.Page<T, T[]> removeIx(int offset)
	{
		lock.writeLock().lock();
		try {
			return page.removeIx(offset);
		} finally {
			lock.writeLock().unlock();
		}
	}

	public MigList.Page<T, T[]> removeFirst(Object o, int startOffset, int endOffset)
	{
		lock.writeLock().lock();
		try {
			return page.removeFirst(o, startOffset, endOffset);
		} finally {
			lock.writeLock().unlock();
		}
	}

	public MigList.Page<T, T[]> removeAll(Object o, int startOffset, int endOffset)
	{
		lock.writeLock().lock();
		try {
			return page.removeAll(o, startOffset, endOffset);
		} finally {
			lock.writeLock().unlock();
		}
	}

	public MigList.Page<T, T[]> removeRange(int startOffset, int endOffset)
	{
		lock.writeLock().lock();
		try {
			return page.removeRange(startOffset, endOffset);
		} finally {
			lock.writeLock().unlock();
		}
	}

	public MigList.Page<T, T[]> retainAll(Collection<?> col, int startOffset, int endOffset)
	{
		lock.writeLock().lock();
		try {
			return page.retainAll(col, startOffset, endOffset);
		} finally {
			lock.writeLock().unlock();
		}
	}

	public MigList.Page<T, T[]> clear()
	{
		lock.writeLock().lock();
		try {
			return page.clear();
		} finally {
			lock.writeLock().unlock();
		}
	}

	public MigList.Page<T, T[]>[] split(int fromOffset)
	{
		lock.writeLock().lock();
		try {
			MigList.Page<T, T[]>[] pages = page.split(fromOffset);
			if (!(pages[0] instanceof ObjectPageRWWrap))
				pages[0] = new ObjectPageRWWrap<>(pages[0]);
			if (!(pages[1] instanceof ObjectPageRWWrap))
				pages[1] = new ObjectPageRWWrap<>(pages[1]);
			return pages;
		} finally {
			lock.writeLock().unlock();
		}
	}
}
