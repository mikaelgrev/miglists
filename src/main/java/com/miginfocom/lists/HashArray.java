package com.miginfocom.lists;

import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

public final class HashArray<V>
{
	private final Level<V> topLevel;

	public HashArray()
	{
		this.topLevel = new Level<>(0);
	}

	public  void put(int key, V o)
	{
		topLevel.put(key, o);
	}

	public void put2(int key, V o)
	{
		Level<V> level = topLevel;

		for(;;) {
			int ix = level.getIx(key);
			Object arrO = level.values[ix];

			if (arrO == null) {
				level.keys[ix] = key;
				level.values[ix] = o;
				return;
			} else if (arrO instanceof Level) {
				level = (Level<V>) arrO;
				key = (key << (level.bits & 0x0000000F));
			} else {
				Level<V> newLevel = new Level<>(key << (level.bits & 0x0000000F), o, level.level + 1);
				level.values[ix] = newLevel;
				newLevel.put(level.keys[ix], (V) arrO);
				return;
			}
		}
	}

	public  V get(int key)
	{
		return topLevel.get(key);
	}

	public long getSlots()
	{
		AtomicLong count = new AtomicLong();
		topLevel.getSlots(count);
		return count.get();
	}

	public long getObjects()
	{
		AtomicLong count = new AtomicLong();
		topLevel.getObjects(count);
		return count.get();
	}

	public long getPages()
	{
		AtomicLong count = new AtomicLong();
		topLevel.getPages(count);
		return count.get();
	}

	public HashMap<Integer, long[]> getFreeOccPerLevel()
	{
		HashMap<Integer, long[]> map = new HashMap<>();
		topLevel.getFreeOccPerLevel(map);
		return map;
	}

//	    private static final int[] LEVEL_BITS = {10, 6, 3, 2, 2, 2, 2, 2};
//		private static final int[] LEVEL_BITS = {6, 6, 6, 2, 2, 10};
//		private static final int[] LEVEL_BITS = {5, 5, 5, 5, 5, 5, 2};
//		private static final int[] LEVEL_BITS = {5, 5, 4, -5, -12};
//		private static final int[] LEVEL_BITS = {17, -5, -12};
		private static final int[] LEVEL_BITS = {8, 8, -5, -12};
//		private static final int[] LEVEL_BITS = {4, 4, 4, 4, 4, 4, 4, 4};
//		private static final int[] LEVEL_BITS = {6, 6, 6, 6, 6, 2};

	static final class Level<V>
	{
		private final int[] keys;
		private final Object[] values;
		private final int bits;
		private final int level;
//		private final int getShift;

		Level(int level)
		{
			this.bits = LEVEL_BITS[level];
			this.level = level;
			this.keys = new int[1 << bits];
			this.values = new Object[1 << bits];
//			this.getShift = 32 - bits;
		}

		Level(int key, Object startObj, int level)
		{
			this.bits = LEVEL_BITS[level];
			this.level = level;

			int ix = getIx(key);

			this.keys = new int[1 << bits];
			this.keys[ix] = key;

			this.values = new Object[1 << bits];
			this.values[ix] = startObj; // It's empty for sure.
//			this.getShift = 32 - bits;
		}

		void put(int key, V v)
		{
			int ix = getIx(key);

			Object arrV = values[ix];

			if (arrV == null) {

				keys[ix] = key;
				values[ix] = v;

			} else if (arrV.getClass() == Level.class) {

				((Level) arrV).put(key << bits, v);

			} else if (arrV.getClass() == DynLevel.class) {

				((DynLevel) arrV).put(key << bits, v);

			} else {
				if (LEVEL_BITS[level + 1] < 0) {
					values[ix] = new DynLevel<>(key << bits, v, keys[ix] << bits, (V) arrV);
				} else {
					Level<V> newLevel = new Level<>(key << bits, v, level + 1);
					newLevel.put(keys[ix] << bits, (V) arrV);
					values[ix] = newLevel;
				}
			}
		}

		V get(int key)
		{
			int ix = getIx(key);

			Object arrV = values[ix];

			if (arrV == null) {
				return null;
			} else if (arrV.getClass() == Level.class) {
				return ((Level<V>) arrV).get(key << bits);
			} else if (arrV.getClass() == DynLevel.class) {
				return ((DynLevel<V>) arrV).get(key << bits);
			} else {
				return (V) arrV;
			}
		}

		int getIx(int key)
		{
			return key >>> (32 - bits);
		}

		// *************************************************************************

		void getSlots(AtomicLong count)
		{
			count.addAndGet(values.length);

			for (Object o : values) {
				if (o != null && o.getClass() == Level.class)
					((Level) o).getSlots(count);
				if (o != null && o.getClass() == DynLevel.class)
					((DynLevel) o).getSlots(count);
			}
		}

		void getObjects(AtomicLong count)
		{
			for (Object o : values) {
				if (o != null && o.getClass() != Level.class && o.getClass() != DynLevel.class)
					count.incrementAndGet();

				if (o != null && o.getClass() == Level.class)
					((Level) o).getObjects(count);
				if (o != null && o.getClass() == DynLevel.class)
					((DynLevel) o).getObjects(count);
			}
		}

		void getPages(AtomicLong count)
		{
			for (Object o : values) {
				if (o != null && o.getClass() == Level.class) {
					count.incrementAndGet();
					((Level) o).getSlots(count);
				}
				if (o != null && o.getClass() == DynLevel.class) {
					count.incrementAndGet();
					((DynLevel) o).getSlots(count);
				}
			}
		}

		public void getFreeOccPerLevel(HashMap<Integer, long[]> map)
		{
			long[] freeOcc = map.get(level);
			if (freeOcc == null) {
				freeOcc = new long[2];
			    map.put(level, freeOcc);
			}
			for (Object o : values) {
				freeOcc[o == null ? 0 : 1]++;
				if (o instanceof Level)
					((Level) o).getFreeOccPerLevel(map);
				if (o instanceof DynLevel)
					((DynLevel) o).getFreeOccPerLevel(map, level + 1);
			}
		}
	}

	static final class DynLevel<V>
	{
		private int[] keys;
		private Object[] values;
		private int size;

		DynLevel(int key, V startObj, int key2, V startObj2)
		{
			this.keys = new int[] {key, key2, 0, 0};
			this.values = new Object[] {startObj, startObj2, null, null};
			this.size = 2;
		}

		void put(int key, V v)
		{
			if (size == values.length) {
				keys = Arrays.copyOf(keys, values.length << 1);
				values = Arrays.copyOf(values, values.length << 1);
//				keys = Arrays.copyOf(keys, (int) (values.length * 1.5) + 1);
//				values = Arrays.copyOf(values, (int) (values.length * 1.5) + 1);
			}

			int ix = findInsertPoint(key);
			int len = size - ix;
			if (len > 0) {
				System.arraycopy(keys, ix, keys, ix + 1, len);
				System.arraycopy(values, ix, values, ix + 1, len);
			}

			keys[ix] = key;
			values[ix] = v;
			size++;
		}

		V get(int key)
		{
//			for (int i = 0, iSz = size; i < iSz; i++) {
//				if (keys[i] == key)
//					return (V) values[i];
//			}
//			return null;

			int lo = 0;
			int hi = size - 1;

			while (lo <= hi) {
				int mid = (lo + hi) >>> 1;
				int arrKey = keys[mid];

				if (arrKey < key) {
					lo = mid + 1;
				} else if (arrKey > key) {
					hi = mid - 1;
				} else {
					return (V) values[mid];
				}
			}
			return null;
		}

		private int findInsertPoint(int key)
		{
			int lo = 0;
			int hi = size - 1;

			while (lo <= hi) {
				int mid = (lo + hi) >>> 1;
				int arrKey = keys[mid];

				if (arrKey < key) {
					lo = mid + 1;
				} else if (arrKey > key) {
					hi = mid - 1;
				} else {
					return mid;
				}
			}
			return lo;
		}

		// *************************************************************************

		void getSlots(AtomicLong count)
		{
			count.addAndGet(values.length);

			for (Object o : values) {
				if (o != null && o.getClass() == DynLevel.class)
					((Level) o).getSlots(count);
			}
		}

		void getObjects(AtomicLong count)
		{
			for (Object o : values) {
				if (o != null && o.getClass() != DynLevel.class)
					count.incrementAndGet();

				if (o != null && o.getClass() == DynLevel.class)
					((Level) o).getSlots(count);
			}
		}

		void getPages(AtomicLong count)
		{
			for (Object o : values) {
				if (o != null && o.getClass() == DynLevel.class) {
					count.incrementAndGet();
					((Level) o).getSlots(count);
				}
			}
		}

		public void getFreeOccPerLevel(HashMap<Integer, long[]> map, int level)
		{
			long[] freeOcc = map.get(level);
			if (freeOcc == null) {
				freeOcc = new long[2];
				map.put(level, freeOcc);
			}
			for (Object v : values) {
				freeOcc[v == null ? 0 : 1]++;
				if (v instanceof DynLevel)
					((Level) v).getFreeOccPerLevel(map);
			}
		}
	}

	private static int rehash(int x)
	{
		x ^= (x << 21);
		x ^= (x >>> 3);
		x ^= (x << 4);
		return x;
	}
}
