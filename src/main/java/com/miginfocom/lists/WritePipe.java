package com.miginfocom.lists;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 2013-10-22
 *         Time: 22:31
 */
public interface WritePipe<T>
{
	void writeBytes(byte b1, byte b2, byte b3, byte b4);

	void writeShorts(short s1, short s2);

	void writeInt(int i);

	void writeFloat(float f);

	void writeDouble(double d);

	void writeLong(long l);
}
