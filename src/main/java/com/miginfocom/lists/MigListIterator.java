package com.miginfocom.lists;

import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 10/3/13
 *         Time: 21:53 PM
 */
public class MigListIterator<E> implements ListIterator<E>
{
	private final MigList<E> list;
	private final long base;
	private long cursor, endIndex;
	private long lastRet = -1;

	MigListIterator(MigList<E> list, long fromIndex)
	{
		this(list, 0, fromIndex, list.sizeLong());
	}

	MigListIterator(MigList<E> list, long fromIndex, long endIndex)
	{
		this(list, 0, fromIndex, endIndex);
	}

	MigListIterator(MigList<E> list, long base, long fromIndex, long endIndex)
	{
		if (fromIndex < 0 || fromIndex > endIndex || endIndex > list.sizeLong())
			throw new IndexOutOfBoundsException("fromIndex: " + fromIndex + ", endIndex: " + endIndex + " for size: " + list.sizeLong());

		this.list = list;
		this.base = base;
		this.cursor = fromIndex;
		this.endIndex = endIndex;
	}

	public boolean hasNext()
	{
		return cursor != endIndex;
	}

	public int nextIndex()
	{
		return (int) Math.min(Integer.MAX_VALUE, nextIndexLong());
	}

	public long nextIndexLong()
	{
		return cursor - base;
	}

	public E next()
	{
		if (cursor >= endIndex)
			throw new NoSuchElementException();

		return list.get(lastRet = cursor++);
	}

	public boolean hasPrevious()
	{
		return cursor != base;
	}

	public int previousIndex()
	{
		return (int) Math.min(Integer.MAX_VALUE, previousIndexLong());
	}

	public long previousIndexLong()
	{
		return cursor - base - 1;
	}

	public E previous()
	{
		long i = cursor - 1;
		if (i < 0)
			throw new NoSuchElementException();

		cursor = i;
		return list.get(lastRet = i);
	}

	public void add(E e)
	{
		list.add(cursor++, e);
		lastRet = -1;
		endIndex++;
	}

	public void set(E e)
	{
		if (lastRet == -1)
			throw new IllegalStateException();

		list.set(lastRet, e);
	}

	public void remove()
	{
		if (lastRet == -1)
			throw new IllegalStateException();

		list.remove(lastRet);
		cursor = lastRet;
		lastRet = -1;
		endIndex--;
	}
}