package com.miginfocom.lists;

import java.util.Collection;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 10/3/13
 *         Time: 21:57 PM
 */
class MigListView<E> implements MigList<E>
{
	private final MigList<E> parent;
	private final long startOffset;

	private long endOffset;

	public MigListView(MigList<E> parent, long startIndex, long endIndex)
	{
		if (startIndex < 0 || endIndex < startIndex || endIndex > parent.sizeLong())
			throw new IndexOutOfBoundsException("startIndex: " + startIndex + ", endIndex: " + endIndex + " for size: " + parent.sizeLong());

		this.parent = parent;
		this.startOffset = startIndex;
		this.endOffset = endIndex;
	}

	// Read ********************************************

	public int size()
	{
		return toInt(endOffset - startOffset);
	}

	public long sizeLong()
	{
		return endOffset - startOffset;
	}

	public boolean isEmpty()
	{
		return endOffset == startOffset;
	}

	public boolean contains(Object o)
	{
		return parent.indexOf(o, startOffset, endOffset) != -1;
	}

	public boolean contains(Object o, long startIndex, long endIndex)
	{
		rangeCheck(startIndex, endIndex);

		return parent.contains(o, startOffset + startIndex, startOffset + endIndex);
	}

	public boolean containsAll(Collection<?> c)
	{
		return parent.containsAll(c, startOffset, endOffset);
	}

	public boolean containsAll(Collection<?> c, long startIndex, long endIndex)
	{
		rangeCheck(startIndex, endIndex);

		return parent.containsAll(c, startOffset + startIndex, startOffset + endIndex);
	}

	public Object[] toArray()
	{
		return parent.toArray(startOffset, endOffset);
	}

	public <T> T[] toArray(T[] a)
	{
		return parent.toArrays(a, Integer.MAX_VALUE, startOffset, endOffset)[0];
	}

	public <T> T[][] toArrays(T[] a, int maxPageSize, long startIndex, long endIndex)
	{
		rangeCheck(startIndex, endIndex);

		return parent.toArrays(a, maxPageSize, startOffset + startIndex, startOffset + endIndex);
	}

	public Object[] toArray(long startIndex, long endIndex)
	{
		rangeCheck(startIndex, endIndex);

		return parent.toArray(startOffset + startIndex, startOffset + endIndex);
	}

	public <T> T[] toArray(T[] arr, long startIndex, long endIndex)
	{
		rangeCheck(startIndex, endIndex);

		return parent.toArray(arr, startOffset + startIndex, startOffset + endIndex);
	}

	public E get(int index)
	{
		rangeCheck(index);

		return parent.get(startOffset + index);
	}

	public E get(long index)
	{
		return parent.get(startOffset + index);
	}

	public int indexOf(Object o)
	{
		return toInt(indexOfLong(o));
	}

	public long indexOfLong(Object o)
	{
		long ix = parent.indexOf(o, startOffset, endOffset);
		return ix != -1 ? toInt(ix - startOffset) : -1;
	}

	public long indexOf(Object o, long startIndex, long endIndex)
	{
		rangeCheck(startIndex, endIndex);

		long ix = parent.indexOf(o, startOffset + startIndex, startOffset + endIndex);
		return ix != -1 ? (ix - startOffset) : -1;
	}

	public int lastIndexOf(Object o)
	{
		return toInt(lastIndexOfLong(o));
	}

	public long lastIndexOfLong(Object o)
	{
		long ix = parent.lastIndexOf(o, startOffset, endOffset);
		return ix != -1 ? toInt(ix - startOffset) : -1;
	}

	public long lastIndexOf(Object o, long startIndex, long endIndex)
	{
		rangeCheck(startIndex, endIndex);

		long ix = parent.lastIndexOf(o, startOffset + startIndex, startOffset + endIndex);
		return ix != -1 ? (ix - startOffset) : -1;
	}

	public MigList<E> subList(int startIndex, int endIndex)
	{
		rangeCheck(startIndex, endIndex);

		return subList((long) startIndex, (long) endIndex);
	}

	public MigList<E> subList(long startIndex, long endIndex)
	{
		rangeCheck(startIndex, endIndex);

		return new MigListView<>(this, startIndex, endIndex);
	}

	// Write ********************************************

	public MigListIterator<E> iterator()
	{
		return new MigListIterator<>(this, 0, 0, sizeLong());
	}

	public MigListIterator<E> listIterator()
	{
		return new MigListIterator<>(this, 0, 0, sizeLong());
	}

	public MigListIterator<E> listIterator(int index)
	{
		rangeCheckExcl(index);

		return new MigListIterator<>(this, 0, index, sizeLong());
	}

	public MigListIterator<E> listIterator(long index)
	{
		rangeCheckExcl(index);

		return new MigListIterator<>(this, 0, index, sizeLong());
	}

	public MigListIterator<E> listIterator(long startIndex, long endIndex)
	{
		rangeCheck(startIndex, endIndex);

		return new MigListIterator<>(this, 0, startIndex, endIndex);
	}

	public E set(int index, E element)
	{
		return set((long) index, element);
	}

	public E set(long index, E element)
	{
		rangeCheck(index);

		return parent.set(startOffset + index, element);
	}

	public void add(int index, E element)
	{
		add((long) index, element);
	}

	public void add(long index, E element)
	{
		rangeCheckExcl(index);

		parent.add(startOffset + index, element);
		endOffset++;
	}

	public boolean add(E e)
	{
		parent.add(endOffset++, e);
		return true;
	}

	public boolean addAll(Collection<? extends E> col)
	{
		parent.addAll(endOffset, col);
		endOffset += col.size();
		return !col.isEmpty();
	}

	public boolean addAll(int index, Collection<? extends E> col)
	{
		return addAll((long) index, col);
	}

	public boolean addAll(long index, Collection<? extends E> col)
	{
		rangeCheckExcl(index);

		parent.addAll(startOffset + index, col);
		endOffset += col.size();
		return !col.isEmpty();
	}

	public E remove(int index)
	{
		endOffset--;
		return parent.remove(startOffset + index);
	}

	public boolean remove(Object o)
	{
		boolean succ = parent.remove(o, startOffset, endOffset);
		if (succ)
			endOffset--;
		return succ;
	}

	public boolean remove(Object o, long startIndex, long endIndex)
	{
		rangeCheck(startIndex, endIndex);

		boolean succ = parent.remove(o, startOffset + startIndex, startOffset + endIndex);
		if (succ)
			endOffset--;
		return succ;
	}

	public E remove(long index)
	{
		endOffset--;
		return parent.remove(startOffset + index);
	}

	public boolean removeAll(Collection<?> col)
	{
		return removeAll(col, 0, sizeLong()) != 0;
	}

	public long removeAll(Collection<?> col, long startIndex, long endIndex)
	{
		rangeCheck(startIndex, endIndex);

		long removed = parent.removeAll(col, startOffset + startIndex, startOffset + endIndex);
		endOffset -= removed;
		return removed;
	}

	public boolean retainAll(Collection<?> col)
	{
		return retainAll(col, 0, sizeLong()) != 0;
	}

	public long retainAll(Collection<?> col, long startIndex, long endIndex)
	{
		rangeCheck(startIndex, endIndex);

		long removed = parent.retainAll(col, startOffset + startIndex, startOffset + endIndex);
		endOffset -= removed;
		if(sizeLong() < 0)
			throw new IllegalArgumentException();
		return removed;
	}

	public void removeRange(long startIndex, long endIndex)
	{
		rangeCheck(startIndex, endIndex);

		parent.removeRange(startOffset + startIndex, startOffset + endIndex);
		endOffset -= (endIndex - startIndex);
	}

	public void clear()
	{
		parent.removeRange(startOffset, endOffset);
		endOffset = startOffset;
	}

	// Helper methods ********************************************

	private static int toInt(long l)
	{
		return (int) Math.min(Integer.MAX_VALUE, l);
	}

	private void rangeCheck(long index)
	{
		if (index < 0 || index >= (endOffset - startOffset))
			throw new IndexOutOfBoundsException("index: " + index + " for size: " + (endOffset - startOffset));
	}

	private void rangeCheckExcl(long index)
	{
		if (index < 0 || index > (endOffset - startOffset))
			throw new IndexOutOfBoundsException("index: " + index + " for size: " + (endOffset - startOffset));
	}

	private void rangeCheck(long startIndex, long endIndex)
	{
		if (startIndex < 0 || startIndex > endIndex || endIndex > (endOffset - startOffset))
			throw new IndexOutOfBoundsException("startIndex: " + startIndex + ", endIndex: " + endIndex + " for size: " + (endOffset - startOffset));
	}
}