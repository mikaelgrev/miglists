package com.miginfocom.lists;

import static com.miginfocom.lists.MigListBaseTest.OpType.*;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 9/29/13
 *         Time: 21:34 PM
 */
public class MigListBaseTest
{
	enum OpType {
		ITERATE, RETAIN_ALL, TO_ARRAYS, TO_ARRAY, TO_ARRAY_TARGET, CONTAINS, CONTAINS_ALL, GET_SIZE, GET, SET, CREATE, ADD, ADD_IX, REMOVE_IX, REMOVE_OBJ, CLEAR, ADD_ALL, REMOVE_RANGE,
		ADD_ALL_IX, INDEX_OF, LAST_INDEX_OF, IT_NEXT, IT_PREV, IT_REMOVE, IT_ADD, IT_SET, IT_HAS_NEXT, IT_HAS_PREV, SUBLIST_CLEAR, SUBLIST_COW
	}

	private static final int NORMAL_TEST_SIZE = Math.max(100, MigListBase.PAGE_SIZE * 3 + 10);
	private static final int NORMAL_TEST_PAGE_HEAVY = Math.max(100, MigListBase.PAGE_SIZE * 10 + 10);

	private static final Random random = new Random(1);

	@Test
	public void iteratorTest()
	{
		MigListBase<Integer, Integer[]> list = new MigListBase<>();
		list.add(1);

		Iterator<Integer> it = list.iterator();
		it.next();
		it.remove();


		list.assertStructure();
//
//		list.removeAll(Arrays.asList(1, 2, 3));
//		list.assertStructure();
//		assertEquals(0, list.size());
	}


	@Test
	public void removeAllTest()
	{
		MigListBase<Integer, Integer[]> list = new MigListBase<>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.assertStructure();

		list.removeAll(Arrays.asList(1, 2, 3));
		list.assertStructure();
		assertEquals(0, list.size());
	}

	@Test
	public void simpleTest2()
	{
		MigListBase<Integer, Integer> migList = new MigListBase<>();
		migList.add(0);
		migList.add(1);
		migList.add(2);
		migList.add(3);
		migList.add(4);
		migList.add(5);
		migList.add(6);
		migList.remove(1);
		migList.removeRange(0, 1);
		migList.assertStructure();
	}

	@Test
	public void removeIxTest()
	{
		ArrayList<Integer> elements = createOrderedList(NORMAL_TEST_SIZE);

		MigListBase<Integer, Integer> migList = new MigListBase<>(elements);
		ArrayList<Integer> refList = new ArrayList<>(elements);
		assertEqual(CREATE, migList, refList);

		Random r = new Random(10);

		while (!refList.isEmpty()) {
			int ix = r.nextInt(migList.size());
			migList.remove(ix);
			refList.remove(ix);
			assertEqual(REMOVE_IX, migList, refList);
		}
	}

	private static final AtomicLong MIGLIST_NANOS = new AtomicLong();
	private static final AtomicLong ARRAYLIST_NANOS = new AtomicLong();

	@Test
	public void testCOWConcurrent()
	{
		for (int i = 0; i < 100; i++)
			testCOWConcurrentOnce();

		System.out.println("MigList   Timed: " + (MIGLIST_NANOS.get() / 1000000f));
		System.out.println("ArrayList Timed: " + (ARRAYLIST_NANOS.get() / 1000000f));
	}

	public void testCOWConcurrentOnce()
	{
		ArrayList<Integer> elements = createOrderedList(getIndex(NORMAL_TEST_PAGE_HEAVY)[0]);

		MigListBase<Integer, Integer> migList = new MigListBase<>(elements);
		MigList<Integer> migListCopy = migList.getCOWView();
//		MigList<Integer> migListCopy = new MigListBase<>(elements);

		List<Integer> refList = new ArrayList<>(elements);
		List<Integer> refListCopy = new ArrayList<>(elements);

//		List<Integer> refList = Collections.synchronizedList(new ArrayList<>(elements));
//		List<Integer> refListCopy = Collections.synchronizedList(new ArrayList<>(elements));

//		List<Integer> refList = Collections.synchronizedList(new LinkedList<>(elements));
//		List<Integer> refListCopy = Collections.synchronizedList(new LinkedList<>(elements));

//		List<Integer> refList = new Vector<>(elements);
//		List<Integer> refListCopy = new Vector<>(elements);

		//		tortureLists(1000, 0, false, refList, migList);
//		tortureLists(1000, 0, false, refListCopy, migListCopy);

		Thread t1 = getTortureListsThread(0, 0, false, migList, refList);
		Thread t2 = getTortureListsThread(0, 0, false, migListCopy, refListCopy);
		t1.start();
		t2.start();

		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		migList.assertStructure();
		((MigListBase) migListCopy).assertStructure();
		assertEqual(SUBLIST_COW, refList, migList);
		assertEqual(SUBLIST_COW, refListCopy, migListCopy);
	}

	@Test
	public void testCOW2()
	{
		ArrayList<Integer> elements = createOrderedList(10);

		MigListBase<Integer, Integer> migList = new MigListBase<>(elements);
		List<Integer> refList = new ArrayList<>(elements);

		MigList<Integer> migListCopy = migList.getCOWView();
		List<Integer> refListCopy = new ArrayList<>(elements);

//		doCommandAndAssert(new TortureOp<>(ADD_IX, new int[] {1}, 123), refListCopy, migListCopy);
//		doCommandAndAssert(new TortureOp<>(REMOVE_IX, new int[] {8}, (Integer[]) null), refListCopy, migListCopy);
//		doCommandAndAssert(new TortureOp<>(REMOVE_RANGE, new int[] {1, 4}, (Integer[]) null), refListCopy, migListCopy);
//		doCommandAndAssert(new TortureOp<>(ITERATE, new int[] {1, 4}, (Integer[]) null), refListCopy, migListCopy);
//		doCommandAndAssert(new TortureOp<>(RETAIN_ALL, new int[] {1, 4}, getObjects(refList, 3, false)), refListCopy, migListCopy);

		assertEqual(SUBLIST_COW, refList, migList);
	}

	@Test
	public void testCOW()
	{
		ArrayList<Integer> elements = createOrderedList(getIndex(NORMAL_TEST_SIZE)[0]);

		MigListBase<Integer, Integer> migList = new MigListBase<>(elements);
		List<Integer> refList = new ArrayList<>(elements);

		MigList<Integer> migListCopy = migList.getCOWView();
		List<Integer> refListCopy = new ArrayList<>(elements);

		long testMillis = 1000;
		boolean assertLists = true;
		int maxSubListLevel = 2;
		tortureLists(testMillis, maxSubListLevel, assertLists, refList, migList);
		tortureLists(testMillis, maxSubListLevel, assertLists, refListCopy, migListCopy);
		tortureLists(testMillis, maxSubListLevel, assertLists, refList, migList);
		tortureLists(testMillis, maxSubListLevel, assertLists, refListCopy, migListCopy);
	}

	@Test
	public void testTorture()
	{
		ArrayList<Integer> elements = createOrderedList(getIndex(NORMAL_TEST_SIZE)[0]);

		MigList<Integer> migList = new MigListBase<>(elements);
		List<Integer> refList = new ArrayList<>(elements);

		tortureLists(4000, 3, true, refList, migList);
	}

	@SafeVarargs
	private final Thread getTortureListsThread(final long forMillis, final int maxSubListLevel, final boolean assertLists, final List<Integer>... lists)
	{
		return new Thread(new Runnable() {
			public void run()
			{
				tortureLists(forMillis, maxSubListLevel, assertLists, lists);
			}
		});
	}

	@SafeVarargs
	private final void tortureLists(long forMillis, int maxSubListLevel, boolean assertLists, List<Integer>... lists)
	{
		int loops = 0;
		long n = System.nanoTime();
		while (true) {

			List<Integer>[] tortureList = lists.clone();

			int subListDepth = maxSubListLevel > 0 ? RANDOM.nextInt(maxSubListLevel) : 0;

			for (int i = 0; i < subListDepth; i++) {
				int[] range = getRange(tortureList[0].size());

				for (int j = 0; j < tortureList.length; j++)
					tortureList[j] = tortureList[j].subList(range[0], range[1]);

				if (assertLists)
					assertEqual(SUBLIST_COW, tortureList);
			}

//			System.out.println("--- Source list size: " + tortureList[0].size());

			for (int i = 0; i < 10; i++)
				doCommandAndAssert(getTorture(tortureList[0]), assertLists, tortureList);

			loops++;

			long millisPassed = (long) ((System.nanoTime() - n) / 1000000f);
			if (millisPassed >= forMillis)
				break;
		}
//		System.out.println("Completed loops: " + loops);
	}

	private TortureOp<Integer> getTorture(List<Integer> exampleList)
	{
		int listSize = exampleList.size();
		boolean isHigh = listSize > MigListBase.PAGE_SIZE * 10; // No point going beyond this size by much
		boolean isLow = listSize < 10;
		boolean shouldDecrease = RANDOM.nextFloat() > (isHigh ? 0.2f : (isLow ? 0.9 : 0.6f));
		float rnd = RANDOM.nextFloat();

		if (shouldDecrease) {

			if (rnd < 0.002f)
				return new TortureOp<>(CLEAR, null, (Integer[]) null);

			if (rnd < 0.02f)
				return new TortureOp<>(RETAIN_ALL, getRange(listSize), getObjects(exampleList, listSize >> 5, RANDOM.nextBoolean()));

			if (rnd < 0.2f)
				return new TortureOp<>(REMOVE_RANGE, getRange(listSize), (Integer[]) null);

			if (rnd < 0.7 && listSize > 0)
				return new TortureOp<>(REMOVE_IX, getIndex(listSize - 1), (Integer[]) null);

			return new TortureOp<>(REMOVE_OBJ, null, getObject(exampleList));

		} else {

			if (rnd < 0.1f)
				return new TortureOp<>(ADD, null, RANDOM.nextInt());

			if (rnd < 0.2f)
				return new TortureOp<>(ADD_ALL, null, getIntegers(nextIntRand(MigListBase.PAGE_SIZE * 2)));

			if (rnd < 0.3f)
				return new TortureOp<>(ADD_IX, getIndex(listSize), RANDOM.nextInt());

			if (rnd < 0.4f)
				return new TortureOp<>(ADD_ALL_IX, getIndex(listSize), getIntegers(nextIntRand(MigListBase.PAGE_SIZE * 2)));

			if (rnd < 0.5f && listSize > 0)
				return new TortureOp<>(GET, getIndex(listSize - 1), (Integer[]) null);

			if (rnd < 0.55f)
				return new TortureOp<>(INDEX_OF, getIndex(listSize), getObject(exampleList));

			if (rnd < 0.6f)
				return new TortureOp<>(LAST_INDEX_OF, getIndex(listSize), getObject(exampleList));

			if (rnd < 0.65f && listSize > 0)
				return new TortureOp<>(SET, getIndex(listSize - 1), RANDOM.nextInt());

			if (rnd < 0.8f)
				return new TortureOp<>(ITERATE, null, nextIntRand(listSize));

			if (rnd < 0.82f)
				return new TortureOp<>(GET_SIZE, null, (Integer[]) null);

			if (rnd < 0.86f)
				return new TortureOp<>(CONTAINS, getRange(listSize), getObject(exampleList));

			if (rnd < 0.88f)
				return new TortureOp<>(CONTAINS_ALL, getRange(listSize), getObjects(exampleList, listSize >> 5, RANDOM.nextBoolean()));

			if (rnd < 0.92f)
				return new TortureOp<>(TO_ARRAY, getRange(listSize), (Integer[]) null);

			if (rnd < 0.95f)
				return new TortureOp<>(TO_ARRAY_TARGET, getRange(listSize), (Integer[]) null);

			return new TortureOp<>(TO_ARRAYS, getRange(listSize), (Integer[]) null);
		}
	}
	@SafeVarargs
	private final <E> void doCommandAndAssert(TortureOp<E> op, boolean assertList, List<E>... lists)
	{
		ArrayList<Object> answers = assertList ? new ArrayList<>(lists.length) : null;
		Object noAns = new Object();

//		System.out.println("cmd: " + op.toString());
//		System.out.println(lists[0].size());
		int commonSeed = RANDOM.nextInt();
		for (List<E> list : lists) {
			Object ans = noAns;
			long n = System.nanoTime();
			switch (op.op) {
				case SET:
					ans = list.set(op.startIx, op.object);
					break;

				case ADD:
					ans = list.add(op.object);
					break;

				case ADD_ALL:
					ans = list.addAll(Arrays.asList(op.objects));
					break;

				case ADD_IX:
					list.add(op.startIx, op.object);
					break;

				case ADD_ALL_IX:
					ans = list.addAll(op.startIx, Arrays.asList(op.objects));
					break;

				case REMOVE_IX:
					ans = list.remove(op.startIx);
					break;

				case CLEAR:
					list.clear();
					break;

				case RETAIN_ALL:
					ans = list.retainAll(Arrays.asList(op.objects));
					break;

				case REMOVE_RANGE:
					if (list instanceof MigList) {
						((MigList) list).removeRange(op.startIx, op.endIx);
					} else {
						list.subList(op.startIx, op.endIx).clear();
					}
					break;

				case GET:
					ans = list.get(op.startIx);
					break;

				case INDEX_OF:
					ans = list.indexOf(op.object);
					break;

				case LAST_INDEX_OF:
					ans = list.lastIndexOf(op.object);
					break;

				case GET_SIZE:
					ans = list.size();
					break;

				case ITERATE:
					ans = doIterate(list, commonSeed);
					break;

				case CONTAINS:
					ans = list.contains(op.object);
					break;

				case CONTAINS_ALL:
					ans = list.containsAll(Arrays.asList(op.objects));
					break;

				case TO_ARRAY:
					list.toArray();
					break;

				case TO_ARRAY_TARGET:
					E[] arr = (E[]) new Integer[RANDOM.nextBoolean() ? 0 : nextIntRand(list.size() * 2)];
					list.toArray(arr);
					break;

				case TO_ARRAYS:
					arr = (E[]) new Integer[RANDOM.nextBoolean() ? 0 : nextIntRand(list.size() * 2)];
					if (list instanceof MigList)
						((MigList) list).toArrays(arr, Integer.MAX_VALUE, op.startIx, op.endIx);
					break;
			}

			long nanos = System.nanoTime() - n;
			if (list instanceof MigList) {
				MIGLIST_NANOS.addAndGet(nanos);
			} else {
				ARRAYLIST_NANOS.addAndGet(nanos);
			}

			if (assertList && ans != noAns)
				answers.add(ans);
		}

		if (assertList) {
			if (!answers.isEmpty()) {
				for (int i = 1; i < lists.length; i++)
					assertEquals(op.op.toString(), answers.get(i), answers.get(i - 1));
			}
			assertEqual(op.op, lists);
		}
	}

	private static <E> ArrayList<Object> doIterate(List<E> list, int seed)
	{
		Random rnd = new Random(seed);
		int ix = nextIntRand(rnd, list.size());
		ListIterator<E> iterator = list.listIterator(ix);

		ArrayList<Object> results = new ArrayList<>(30);

		results.add(iterator.nextIndex());
		results.add(iterator.previousIndex());

		for (int i = 0; i < 1; i++) {
			if (rnd.nextBoolean()) {
				if (iterator.hasNext()) {
					results.add(iterator.next());
					doIterOp(iterator, rnd);
				}
			} else {
				if (iterator.hasPrevious()) {
					results.add(iterator.previous());
					doIterOp(iterator, rnd);
				}
			}
			results.add(iterator.nextIndex());
			results.add(iterator.previousIndex());
		}
		return results;
	}

	public static <E> void doIterOp(ListIterator<E> iterator, Random rnd)
	{
		float cmd = rnd.nextFloat();
		if (cmd < 0.1f) {
//			System.out.println("set");
			iterator.set((E) (Integer) rnd.nextInt());
		} else if (cmd < 0.55f) {
//			System.out.println("remove");
			iterator.remove();
		} else {
//			System.out.println("add");
			iterator.add((E) (Integer) rnd.nextInt());
		}
	}

	private static class TortureOp<E>
	{
		private final OpType op;
		private final int startIx, endIx;
		private final E[] objects;
		private final E object;

		private TortureOp(OpType op, int[] range, E... objects)
		{
			this.op = op;
			this.startIx = range != null ? range[0] : -1;
			this.endIx = range != null && range.length > 1 ? range[1] : -1;
			this.objects = objects;
			this.object = objects != null && objects.length > 0 ? objects[0] : null;
		}

		public String toString()
		{
			return "TortureOp {" +
			       op +
			       (startIx != -1 ? (", startIx=" + startIx) : "") +
			       (endIx != -1 ? (", endIx=" + endIx) : "") +
                   (objects != null && objects.length > 0 ? (objects.length > 1 ? (", objects=" + Arrays.toString(objects)) : (" '" + objects[0]) + "'") : "") +
			       '}';
		}
	}

	private Integer[] getIntegers(int count)
	{
		Integer[] ints = new Integer[count];
		for (int i = 0; i < count; i++)
			ints[i] = RANDOM.nextInt();
		return ints;
	}

	private Integer[] getObjects(List<Integer> list, int count, boolean unique)
	{
		Collection<Integer> ints = unique ? new HashSet<Integer>(count) : new ArrayList<Integer>(count);
		for (int i = 0; i < count; i++)
			ints.add(list.get(nextIntRand(list.size())));
		return ints.toArray(new Integer[ints.size()]);
	}

	private Integer getObject(List<Integer> list)
	{
		if (RANDOM.nextFloat() < 0.1)
			return RANDOM.nextInt(); // Probably won't exist.

		return list.size() > 0 ? list.get(nextIntRand(list.size())) : null;
	}

	private int[] getRange(int listSize)
	{
		int startIx = getIndex(listSize)[0];
		int sizeLeft = listSize - startIx;
		int size = Math.min(sizeLeft, Math.round(nextIntRand(sizeLeft) * 1.2f));
		return new int[] {startIx, startIx + size};
	}

	private int[] getIndex(int listSize)
	{
		float rnd = RANDOM.nextFloat();
		int ix = rnd < 0.03 ? 0 : (rnd > 0.97 ? listSize : Math.min(listSize, Math.round(rnd * listSize)));
		return new int[] {ix};
	}

	private int nextIntRand(int max)
	{
		return max > 0 ? RANDOM.nextInt(max) : 0;
	}

	private static int nextIntRand(Random rnd, int max)
	{
		return max > 0 ? rnd.nextInt(max) : 0;
	}

	@Test
	public void testSubListClear()
	{
		for (int size = 0; size < NORMAL_TEST_SIZE; size = (int) Math.round(size * 1.05 + 1)) {
			ArrayList<Integer> elements = createOrderedList(size);
			MigListBase<Integer, Integer> migList = new MigListBase<>(elements);
			ArrayList<Integer> refList = new ArrayList<>(elements);

			while (refList.size() > 0) {
				removeSubListHelper(migList, refList, 0, 1);
				if (refList.size() > 0)
					removeSubListHelper(migList, refList, refList.size() - 1, refList.size());
			}
		}
	}

	private void removeSubListHelper(MigListBase<Integer, Integer> migList, ArrayList<Integer> refList, int startIx, int endIx)
	{
		migList.subList(startIx, endIx).clear();
		refList.subList(startIx, endIx).clear();
		assertEqual(SUBLIST_CLEAR, migList, refList);
	}

	@Test
	public void testPageOkUntil()
	{
		ArrayList<Integer> elements = createOrderedList(NORMAL_TEST_SIZE);
		MigListBase<Integer, Integer> migList = new MigListBase<>(elements);
		ArrayList<Integer> refList = new ArrayList<>(elements);

		removeIxHelper(migList, refList, 0);
		assertEquals(refList.get(refList.size() - 1), migList.get(refList.size() - 1));
	}

	@Test
	public void testCreate()
	{
		for (int size = 0; size < NORMAL_TEST_SIZE; size++) {

			ArrayList<Integer> elements = createOrderedList(size);
			MigListBase<Integer, Integer> migList = new MigListBase<>(elements);
			ArrayList<Integer> refList = new ArrayList<>(elements);

			assertEqual(ADD_ALL, migList, refList);
		}
	}

	@Test
	public void testIter()
	{
		for (int size = 0; size < NORMAL_TEST_SIZE; size = (int) Math.round(size * 1.05 + 1)) {

			ArrayList<Integer> elements = createOrderedList(size);
			MigList<Integer> migList = new MigListBase<>(elements);
			ListIterator<Integer> migListIt = migList.listIterator();

			ArrayList<Integer> refList = new ArrayList<>(elements);
			ListIterator<Integer> refListIt = refList.listIterator();

			if (iterHelper(IT_HAS_NEXT, migList, migListIt, refList, refListIt, null))
				iterHelper(IT_NEXT, migList, migListIt, refList, refListIt, random.nextInt());

			iterHelper(IT_ADD, migList, migListIt, refList, refListIt, random.nextInt());

			if (iterHelper(IT_HAS_NEXT, migList, migListIt, refList, refListIt, null)) {
				iterHelper(IT_NEXT, migList, migListIt, refList, refListIt, random.nextInt());
				iterHelper(IT_REMOVE, migList, migListIt, refList, refListIt, random.nextInt());
			}

			if (iterHelper(IT_HAS_PREV, migList, migListIt, refList, refListIt, null)) {
				iterHelper(IT_PREV, migList, migListIt, refList, refListIt, random.nextInt());
				iterHelper(IT_SET, migList, migListIt, refList, refListIt, random.nextInt());
				iterHelper(IT_REMOVE, migList, migListIt, refList, refListIt, random.nextInt());
			}

			if (iterHelper(IT_HAS_NEXT, migList, migListIt, refList, refListIt, null))
				iterHelper(IT_NEXT, migList, migListIt, refList, refListIt, random.nextInt());
			if (iterHelper(IT_HAS_NEXT, migList, migListIt, refList, refListIt, null))
				iterHelper(IT_NEXT, migList, migListIt, refList, refListIt, random.nextInt());
			if (iterHelper(IT_HAS_NEXT, migList, migListIt, refList, refListIt, null))
				iterHelper(IT_NEXT, migList, migListIt, refList, refListIt, random.nextInt());
			if (iterHelper(IT_HAS_NEXT, migList, migListIt, refList, refListIt, null))
				iterHelper(IT_NEXT, migList, migListIt, refList, refListIt, random.nextInt());

			while (iterHelper(IT_HAS_PREV, migList, migListIt, refList, refListIt, null)) {
				iterHelper(IT_PREV, migList, migListIt, refList, refListIt, random.nextInt());
				iterHelper(IT_REMOVE, migList, migListIt, refList, refListIt, random.nextInt());
			}

			while (iterHelper(IT_HAS_NEXT, migList, migListIt, refList, refListIt, null)) {
				iterHelper(IT_NEXT, migList, migListIt, refList, refListIt, random.nextInt());
				iterHelper(IT_REMOVE, migList, migListIt, refList, refListIt, random.nextInt());
			}

			assertEquals(0, migList.size());

			iterHelper(IT_ADD, migList, migListIt, refList, refListIt, random.nextInt());
		}

		ArrayList<Integer> elements = createOrderedList(1);
		MigListBase<Integer, Integer> list = new MigListBase<>(elements);
		MigListIterator migListIt = list.listIterator();

		migListIt.next();
		list.assertStructure();
		migListIt.remove();
		list.assertStructure();
		migListIt.add(2);
	}

	private boolean iterHelper(OpType op, MigList<Integer> migList, ListIterator<Integer> migListIt, ArrayList<Integer> refList, ListIterator<Integer> refListIt, Integer obj)
	{
		Object retObj1 = null, retObj2 = null;
		switch (op) {
			case IT_ADD:
				refListIt.add(obj);
				migListIt.add(obj);
				break;
			case IT_REMOVE:
				refListIt.remove();
				migListIt.remove();
				break;
			case IT_SET:
				refListIt.set(obj);
				migListIt.set(obj);
				break;
			case IT_PREV:
				retObj2 = refListIt.previous();
				retObj1 = migListIt.previous();
				break;
			case IT_NEXT:
				retObj2 = refListIt.next();
				retObj1 = migListIt.next();
				break;
			case IT_HAS_NEXT:
				retObj2 = refListIt.hasNext();
				retObj1 = migListIt.hasNext();
				break;
			case IT_HAS_PREV:
				retObj2 = refListIt.hasPrevious();
				retObj1 = migListIt.hasPrevious();
				break;
		}

		assertEquals(migListIt.hasNext(), refListIt.hasNext());
		assertEquals(migListIt.hasPrevious(), refListIt.hasPrevious());
		assertEquals(retObj2, retObj1);
		assertEqual(op, migList, refList);

		return retObj1 instanceof Boolean && (Boolean) retObj1;
	}


	@Test
	public void testAdd()
	{
		MigListBase<Integer, Integer> migList = new MigListBase<>();
		ArrayList<Integer> refList = new ArrayList<>();

		for (int i = 0; i < 2000; i++)
			addHelper(migList, refList);
	}

	private void addHelper(MigListBase<Integer, Integer> migList, ArrayList<Integer> refList)
	{
		int val = random.nextInt();
		migList.add(val);
		refList.add(val);
		assertEqual(OpType.ADD, migList, refList);
	}

	@Test
	public void testInsert()
	{
		MigListBase<Integer, Integer> migList = new MigListBase<>();
		ArrayList<Integer> refList = new ArrayList<>();

		insertHelper(0, migList, refList);
		insertHelper(0, migList, refList);
		insertHelper(1, migList, refList);
		insertHelper(3, migList, refList);
		insertHelper(0, migList, refList);
	}

	private void insertHelper(int ix, MigListBase<Integer, Integer> migList, ArrayList<Integer> refList)
	{
		int val = random.nextInt();
		migList.add(ix, val);
		refList.add(ix, val);
		assertEqual(OpType.ADD_IX, migList, refList);
	}

	@Test
	public void testAddAll()
	{
		MigListBase<Integer, Integer> migList = new MigListBase<>();
		ArrayList<Integer> refList = new ArrayList<>();

		List<Integer> elements = Arrays.asList();
		migList.addAll(elements);
		refList.addAll(elements);

		assertEqual(OpType.ADD_ALL, migList, refList);

		elements = Arrays.asList(1, 2, 3);
		migList.addAll(elements);
		refList.addAll(elements);

		assertEqual(OpType.ADD_ALL, migList, refList);

		elements = Arrays.asList();
		migList.addAll(elements);
		refList.addAll(elements);

		assertEqual(OpType.ADD_ALL, migList, refList);

		elements = Arrays.asList(4);
		migList.addAll(elements);
		refList.addAll(elements);

		assertEqual(OpType.ADD_ALL, migList, refList);

		elements = new ArrayList<>();
		for (int i = 0; i < 1000; i++)
			elements.add(random.nextInt());

		migList.addAll(elements);
		refList.addAll(elements);

		assertEqual(OpType.ADD_ALL, migList, refList);
	}

	@Test
	public void testRemoveAll()
	{
		List<Integer> elements = Arrays.asList(1, 2, 3, 5, 3, 3, 0);
		MigListBase<Integer, Integer> migList = new MigListBase<>(elements);
		ArrayList<Integer> refList = new ArrayList<>(elements);
		assertEqual(OpType.CREATE, migList, refList);

		removeAllHelper(migList, refList, Collections.<Integer>emptyList());
		removeAllHelper(migList, refList, Arrays.asList("not", "in", "list"));
		removeAllHelper(migList, refList, Arrays.asList(2));
		removeAllHelper(migList, refList, Arrays.asList(1));
		removeAllHelper(migList, refList, Arrays.asList(0));
		removeAllHelper(migList, refList, Arrays.asList(3));
		removeAllHelper(migList, refList, Arrays.asList(5));

		for (int size = 0; size < MigListBase.PAGE_SIZE * 3 + 10; size++)
			removeAllElementsHelper(createRandomList(size));
	}

	private void removeAllElementsHelper(List<Integer> elements)
	{
		MigListBase<Integer, Integer> migList;ArrayList<Integer> refList;
		migList = new MigListBase<>(elements);
		refList = new ArrayList<>(elements);
		assertEqual(OpType.CREATE, migList, refList);
	}

	private void removeIxHelper(MigListBase<Integer, Integer> migList, ArrayList<Integer> refList, int ix)
	{
		migList.remove(ix);
		refList.remove(ix);
		assertEqual(OpType.REMOVE_IX, migList, refList);
	}

	@SafeVarargs
	private final <E> void setIxHelper(int ix, E obj, List<E>... lists)
	{
		for (List<E> list : lists)
			list.set(ix, obj);
		assertEqual(OpType.REMOVE_IX, lists);
	}

	private void removeAllHelper(MigListBase<Integer, Integer> migList, ArrayList<Integer> refList, List<?> elements)
	{
		migList.removeAll(elements);
		refList.removeAll(elements);
		assertEqual(OpType.CLEAR, migList, refList);
	}

	@Test
	public void testInsertAll()
	{
		MigListBase<Integer, Integer> migList = new MigListBase<>();
		ArrayList<Integer> refList = new ArrayList<>();

		ArrayList<Integer> elements = createRandomList(2);
		migList.addAll(0, elements);
		refList.addAll(0, elements);
		assertEqual(OpType.ADD_ALL_IX, migList, refList);

		elements = createRandomList(2);
		migList.addAll(2, elements);
		refList.addAll(2, elements);
		assertEqual(OpType.ADD_ALL_IX, migList, refList);

		insertAllHelper(migList, refList, Arrays.<Integer>asList());
		insertAllHelper(migList, refList, Arrays.asList(1, 2, 3));
		insertAllHelper(migList, refList, Arrays.<Integer>asList());
		insertAllHelper(migList, refList, Arrays.asList(4));
		insertAllHelper(migList, refList, createRandomList(MigListBase.PAGE_SIZE * 2 + 1));
	}

	private ArrayList<Integer> createRandomList(int size)
	{
		ArrayList<Integer> elements = new ArrayList<>();
		for (int i = 0; i < size; i++)
			elements.add(random.nextInt());
		return elements;
	}

	private ArrayList<Integer> createOrderedList(int size)
	{
		ArrayList<Integer> elements = new ArrayList<>();
		for (int i = 0; i < size; i++)
			elements.add(i);
		return elements;
	}

	private void insertAllHelper(MigListBase<Integer, Integer> migList, ArrayList<Integer> refList, List<Integer> elements)
	{
		migList.addAll(0, elements);
		refList.addAll(0, elements);
		assertEqual(OpType.ADD_ALL_IX, migList, refList);

		if (migList.size() > 0) {
			migList.addAll(1, elements);
			refList.addAll(1, elements);
			assertEqual(OpType.ADD_ALL_IX, migList, refList);

			migList.addAll(migList.size() - 1, elements);
			refList.addAll(refList.size() - 1, elements);
			assertEqual(OpType.ADD_ALL_IX, migList, refList);
		}

		migList.addAll(migList.size(), elements);
		refList.addAll(refList.size(), elements);
		assertEqual(OpType.ADD_ALL_IX, migList, refList);
	}

	private final Random RANDOM = new Random(1);

	public static void assertEqual(OpType opType, List ... testedList)
	{
		for (List list : testedList)
			assertStructure(list);

		for (int i = 0; i < testedList.length - 1; i++) {

			List list1 = testedList[i];
			List list2 = testedList[i + 1];

			assertEquals("Size for " + opType, list1.size(), list2.size());

			int size = list1.size();

			if (size > 0) {
				assertEquals("Last element for " + opType + ". Size: " + size, list1.get(size - 1), list2.get(size - 1));
				assertEquals("First element for " +  opType + ". Size: " + size, list1.get(0), list2.get(0));
			}

			assertArrayEquals("Arrays not equal for " + opType, list1.toArray(), list2.toArray());
		}
	}

	private static void assertStructure(List testedList)
	{
		if (testedList instanceof MigListBase)
			((MigListBase) testedList).assertStructure();
	}
}
