package com.miginfocom.lists;

import java.util.Collection;
import java.util.Iterator;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 2013-09-26
 *         Time: 21:22
 */
public class MigListInt extends MigListBase<Integer, int[]>
{
	protected IntPage createPage()
	{
		return new IntPage(pageSize);
	}

	IntPage createPage(Integer[] arr, int size)
	{
		int[] intArr = new int[arr.length];
		for (int i = 0; i < intArr.length; i++)
			intArr[i] = arr[i];
		return new IntPage(size, intArr);
	}

	private static final class IntPage implements Page<Integer, int[]>
	{
		private int size;
		private int sharers = 0;
		final int[] array;

		IntPage(int pageSize)
		{
			this.array = new int[pageSize];
		}

		IntPage(int size, int[] array)
		{
			this.size = size;
			this.array = array;
		}

		public void addSharer()
		{
			sharers++;
		}

		public void removeSharer()
		{
			sharers--;
		}

		public int getSharers()
		{
			return sharers;
		}

		public int size()
		{
			return size;
		}

		public boolean isFull()
		{
			return size == array.length;
		}

		public Page<Integer, int[]> copyPage()
		{
			return null;
		}

		private IntPage clonePageNoSharers()
		{
			return new IntPage(size, array.clone());
		}

		public Integer get(int offset)
		{
			return array[offset];
		}

		public int getInt(int offset)
		{
			return array[offset];
		}

		public int offsetOf(Object o)
		{
			if (!(o instanceof Integer))
				return -1;

			int oInt = (Integer) o;

			for (int i = 0; i < size; i++) {
				if (array[i] == oInt)
					return i;
			}
			return -1;
		}

		public int offsetOf(Object o, int startOffset, int endOffset)
		{
			if (!(o instanceof Integer))
				return -1;

			int oInt = (Integer) o;

			for (int i = startOffset; i < endOffset; i++) {
				if (array[i] == oInt)
					return i;
			}
			return -1;
		}

		int lastOffsetOf(Object o)
		{
			return lastOffsetOf(o, 0, size);
		}

		public int lastOffsetOf(Object o, int startOffset, int endOffset)
		{
			if (!(o instanceof Integer))
				return -1;

			int oInt = (Integer) o;

			for (int i = size - 1; i >= 0; i--) {
				if (array[i] == oInt)
					return i;
			}
			return -1;
		}

		public Page<Integer, int[]> set(int offset, Integer element)
		{
			if (sharers == 0) {
				array[offset] = element;
				return null;
			} else {
				IntPage pageClone = clonePageNoSharers();
				pageClone.array[offset] = element;
				return pageClone;
			}
		}

		public void toArray(Integer[] dest, int destOffset)
		{
			for (int i = 0; i < size; i++)
				dest[destOffset++] = array[i];
		}

		public void toArray(Integer[] dest, int destOffset, int startOffset, int endOffset)
		{
			for (int i = startOffset; i < endOffset; i++)
				dest[destOffset++] = array[i];
		}

		public void toArray(int[] dest, int destOffset)
		{
			for (int i = 0; i < size; i++)
				dest[destOffset++] = array[i];
		}

		public void toArray(int[] dest, int destOffset, int startOffset, int endOffset)
		{
			for (int i = startOffset; i < endOffset; i++)
				dest[destOffset++] = array[i];
		}

		public IntPage removeFirst(Object o, int startOffset, int endOffset)
		{
			if (!(o instanceof Integer))
				return null;

			int oInt = (int) o;
			for (int i = startOffset; i < endOffset; i++) {
				if (array[i] == oInt) {
					removeIx(i);
					return this;
				}
			}
			return null;
		}

		public Page<Integer, int[]> removeAll(Object o, int startOffset, int endOffset)
		{
			if (!(o instanceof Integer))
				return null;

			int integer = (Integer) o;
			int src = 0, dst = 0;
			for (; src < size; src++) {
				int sInt = array[src];
				if (integer != sInt && dst++ != src)
					array[dst] = sInt;
			}

			if (src == dst)
				return null;

			size = dst;
			return this;
		}

		public Page<Integer, int[]> retainAll(Collection<?> col, int startOffset, int endOffset)
		{
			int src = 0, dst = 0;
			for (; src < size; src++) {
				int sInt = array[src];
				if (col.contains(sInt) && dst++ != src)
					array[dst] = sInt;
			}
			size = dst;
			return this;
		}

		public IntPage removeRange(int startOffset, int endOffset)
		{
			int len = endOffset - startOffset;
			System.arraycopy(array, endOffset, array, startOffset, len);
			size -= len;
			return this;
		}

		public IntPage clear()
		{
			size = 0;
			return this;
		}

		boolean contains(Object o)
		{
			return o instanceof Integer && containsInt((int) o);
		}

		boolean containsInt(int i)
		{
			for (int a : array) {
				if (a == i)
					return true;
			}
			return false;
		}

		public IntPage add(Integer object)
		{
			offerInt(object);
			return null; //todo fix
		}

		public boolean offerInt(int i)
		{
			if (size < array.length) {
				array[size++] = i;
				return true;
			} else {
				return false;
			}
		}

		public IntPage add(int offset, Integer object)
		{
			return null; //todo fix
//			if (size < PAGE_SIZE) {
//				System.arraycopy(array, offset, array, offset + 1, size++ - offset);
//				array[offset] = object;
//				return true;
//			}
//			return false;
		}

		public Page<Integer, int[]> add(Iterator<? extends Integer> source)
		{
			return null;
		}

		public IntPage[] split(int fromOffset)
		{
			return null; // todo Well...
		}

		public IntPage removeIx(int offset)
		{
			int len = --size - offset;
			if (len > 0)
				System.arraycopy(array, offset + 1, array, offset, len);

			return null; // todo Well...
		}

		public int elementCountDebug() { return array.length; }
	}

}
