package com.miginfocom.lists;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 10/14/13
 *         Time: 23:07 PM
 */
//public final class ObjectPageSync<T> extends ObjectPage<T>
//{
//	public ObjectPageSync(int pageSize)
//	{
//		super(pageSize);
//	}
//
//	public ObjectPageSync(int size, T[] array)
//	{
//		super(size, array);
//	}
//
//	public synchronized MigList.Page<T, T[]> add(T element)
//	{
//		return super.add(element);
//	}
//
//	public synchronized MigList.Page<T, T[]> add(int offset, T element)
//	{
//		return super.add(offset, element);
//	}
//
//	public synchronized MigList.Page<T, T[]> add(Iterator<? extends T> source)
//	{
//		return super.add(source);
//	}
//
//	public synchronized void removeSharer()
//	{
//		super.removeSharer();
//	}
//
//	public synchronized void addSharer()
//	{
//		super.addSharer();
//	}
//
//	public synchronized int arraySizeDebug()
//	{
//		return super.arraySizeDebug();
//	}
//
//	public synchronized MigList.Page<T, T[]> clear()
//	{
//		return super.clear();
//	}
//
//	public synchronized T get(int offset)
//	{
//		return super.get(offset);
//	}
//
//	public synchronized int lastOffsetOf(Object o, int startOffset, int endOffset)
//	{
//		return super.lastOffsetOf(o, startOffset, endOffset);
//	}
//
//	public synchronized int offsetOf(Object o, int startOffset, int endOffset)
//	{
//		return super.offsetOf(o, startOffset, endOffset);
//	}
//
//	public synchronized MigList.Page<T, T[]> removeAll(Object o, int startOffset, int endOffset)
//	{
//		return super.removeAll(o, startOffset, endOffset);
//	}
//
//	public synchronized MigList.Page<T, T[]> removeFirst(Object o, int startOffset, int endOffset)
//	{
//		return super.removeFirst(o, startOffset, endOffset);
//	}
//
//	public synchronized MigList.Page<T, T[]> removeIx(int offset)
//	{
//		return super.removeIx(offset);
//	}
//
//	public synchronized MigList.Page<T, T[]> removeRange(int startOffset, int endOffset)
//	{
//		return super.removeRange(startOffset, endOffset);
//	}
//
//	public synchronized MigList.Page<T, T[]> retainAll(Collection<?> col, int startOffset, int endOffset)
//	{
//		return super.retainAll(col, startOffset, endOffset);
//	}
//
//	public synchronized MigList.Page<T, T[]> set(int offset, T element)
//	{
//		return super.set(offset, element);
//	}
//
//	public synchronized MigList.Page<T, T[]>[] split(int fromOffset)
//	{
//		return super.split(fromOffset);
//	}
//
//	public synchronized void toArray(T[] destArr, int destOffset)
//	{
//		super.toArray(destArr, destOffset);
//	}
//
//	public synchronized void toArray(T[] destArr, int destOffset, int startOffset, int endOffset)
//	{
//		super.toArray(destArr, destOffset, startOffset, endOffset);
//	}
//}
