package com.miginfocom.lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 26/10/13
 *         Time: 10:35 AM
 */
public class IndexedObjectPage<T> implements MigList.Page<T, T[]>
{
	private int size;
	private int sharers = 0;
	private final T[] array;

	private final Indexer<T>[] indexers;
	private final HashIndexMap2 valueHashMap;
	private final HashIndexMap2[] hashMaps; // Never null but can be length 0;

	/**
	 * @param pageSize The maximum number of elements in the page.
	 * @param indexers Overtaken and will not be changed.
	 */
	IndexedObjectPage(int pageSize, Indexer<T>[] indexers, boolean indexValues)
	{
		this(0, (T[]) new Object[pageSize], indexers, indexValues);
	}

	/**
	 * @param size The number of active elements in the a array. The rest should be null.
	 * @param array The array with possibly some data. The length will be the page size.
	 * @param indexers Overtaken and will not be changed. Can be null.
	 */
	IndexedObjectPage(int size, T[] array, Indexer<T>[] indexers, boolean indexValues)
	{
		this.size = size;
		this.array = array;
		this.indexers = indexers;
		this.hashMaps = new HashIndexMap2[(indexers != null ? indexers.length : 0)];
		this.valueHashMap = indexValues ? new HashIndexMap2(array.length) : null;

		for (int i = 0; i < hashMaps.length; i++)
			hashMaps[i] = new HashIndexMap2(array.length);

		if (size > 0)
			rebuildHashMaps();
	}

	// read ********************************************************

	/** Return the objects that is indexed by <code>index</code>.
	 * @param index The index to look up. Not null.
	 * @param indexerIx The indexer to use. Index in the indexers array sent into the constructor.
	 * @param maxResults The maximum number of entries that <code>outCol</code> will contain. At least one will be added (if found).
	 * @return <code>outCol</code> if it was != null or a new collection with the found entries. null if outCol was null and no elements was found.
	/** Return the objects that is indexed by <code>index</code>.
	 * @return <code>outCol</code> if it was != null or a new collection with the found entries. null if outCol was null and no elements was found.
	 * @throws IllegalArgumentException if <code>indexer</code> was not one of the indexers the page was created with.
	 */
	public List<T> getIndexed(Object index, int indexerIx, int maxResults, List<T> outCol)
	{
		int[] indexes = hashMaps[indexerIx].getOffsets(index);
		Indexer<T> indexer = indexers[indexerIx];

		for (int ix : indexes) {
			T obj = array[ix];
			if (indexer.indexEquals(index, obj)) {
				if (outCol == null)
					outCol = new ArrayList<>(Math.min(indexes.length, 10)); // Some memory consumption protection against very bad hashing.

				outCol.add(obj);

				if (outCol.size() >= maxResults)
					break;
			}
		}
		return outCol;
	}

	public int size()
	{
		return size;
	}

	public boolean isFull()
	{
		return size == array.length;
	}

	public IndexedObjectPage<T> copyPage()
	{
		removeSharer();
		return new IndexedObjectPage<>(size, array.clone(), indexers, valueHashMap != null);
	}

	public T get(int offset)
	{
		return array[offset];
	}

	public int offsetOf(Object o, int startOffset, int endOffset)
	{
		if (valueHashMap != null) {
			for (int ix : valueHashMap.getOffsets(o, startOffset, endOffset)) {
				if (Objects.equals(array[ix], o))
					return ix;
			}
		} else {
			for (int i = startOffset; i < endOffset; i++) {
				if (Objects.equals(array[i], o))
					return i;
			}
		}
		return -1;
	}

	public int lastOffsetOf(Object o, int startOffset, int endOffset)
	{
		for (int i = endOffset - 1; i >= startOffset; i--) {
			if (Objects.equals(array[i], o))
				return i;
		}
		return -1;
	}

	public void toArray(T[] destArr, int destOffset)
	{
		System.arraycopy(array, 0, destArr, destOffset, size);
	}

	public void toArray(T[] destArr, int destOffset, int startOffset, int endOffset)
	{
		System.arraycopy(array, startOffset, destArr, destOffset, endOffset - startOffset);
	}

	public int elementCountDebug()
	{
		return array.length;
	}

	// Write ********************************************************

	public void addSharer()
	{
		sharers++;
	}

	public void removeSharer()
	{
		sharers--;
	}

	public int getSharers()
	{
		return sharers;
	}

	public MigList.Page<T, T[]> set(int offset, T element)
	{
		if (sharers == 0) {
			return setImpl(offset, element);
		} else {
			return copyPage().setImpl(offset, element);
		}
	}

	private MigList.Page<T, T[]> setImpl(int offset, T element)
	{
		removeFromHashMaps(offset);
		addToHashMaps(element, offset);

		array[offset] = element;
		return this;
	}

	public MigList.Page<T, T[]> add(T element)
	{
		if (sharers == 0) {
			return addImpl(element);
		} else {
			return copyPage().addImpl(element);
		}
	}

	private MigList.Page<T, T[]> addImpl(T element)
	{
		addLastToHashMaps(element);

		array[size++] = element;
		return this;
	}

	public MigList.Page<T, T[]> add(int offset, T element)
	{
		if (sharers == 0) {
			return addImpl(offset, element);
		} else {
			return copyPage().addImpl(offset, element);
		}
	}

	private MigList.Page<T, T[]> addImpl(int offset, T element)
	{
		addToHashMaps(element, offset);

		System.arraycopy(array, offset, array, offset + 1, size - offset);
		array[offset] = element;
		size++;
		return this;
	}

	public MigList.Page<T, T[]> add(Iterator<? extends T> source)
	{
		if (sharers == 0) {
			return addImpl(source);
		} else {
			return copyPage().addImpl(source);
		}
	}

	private MigList.Page<T, T[]> addImpl(Iterator<? extends T> source)
	{
		while(size < array.length && source.hasNext())
			addImpl(source.next());

		return this;
	}

	public MigList.Page<T, T[]> removeIx(int offset)
	{
		if (sharers == 0) {
			return removeIxImpl(offset);
		} else {
			return copyPage().removeIxImpl(offset);
		}
	}

	private MigList.Page<T, T[]> removeIxImpl(int offset)
	{
		removeFromHashMaps(offset);

		int len = --size - offset;
		if (len > 0)
			System.arraycopy(array, offset + 1, array, offset, len);

		array[size] = null;

		return this;
	}

	public MigList.Page<T, T[]> removeFirst(Object o, int startOffset, int endOffset)
	{
		if (sharers == 0) {
			return removeFirstImpl(o, startOffset, endOffset);
		} else {
			return removeFirstOnCopyIfFound(o, startOffset, endOffset);
		}
	}

	private MigList.Page<T, T[]> removeFirstImpl(Object o, int startOffset, int endOffset)
	{
		if (valueHashMap != null) {
			for (int ix : valueHashMap.getOffsets(o, startOffset, endOffset)) {
				if (Objects.equals(array[ix], o)) {
					removeIxImpl(ix);
					return this;
				}
			}
		} else {
			for (int i = startOffset; i < endOffset; i++) {
				if (Objects.equals(array[i], o)) {
					removeIxImpl(i);
					return this;
				}
			}
			return null;
		}
		return null;
	}

	private MigList.Page<T, T[]> removeFirstOnCopyIfFound(Object o, int startOffset, int endOffset)
	{
		if (valueHashMap != null) {
			for (int ix : valueHashMap.getOffsets(o, startOffset, endOffset)) {
				if (Objects.equals(array[ix], o))
					return copyPage().removeIxImpl(ix);
			}
		} else {
			for (int i = startOffset; i < endOffset; i++) {
				if (Objects.equals(array[i], o))
					return copyPage().removeIxImpl(i);
			}
		}
		return null;
	}

	public MigList.Page<T, T[]> removeAll(Object o, int startOffset, int endOffset)
	{
		if (valueHashMap != null) {
			int[] offsets = valueHashMap.getOffsets(o, startOffset, endOffset);
			if (sharers == 0) {
				return removeAllImpl(o, offsets, 0);
			} else {
				return removeAllOnCopyIfFound(o, offsets);
			}
		} else {
			if (sharers == 1) {
				return removeAllImpl(o, startOffset, endOffset);
			} else {
				return removeAllOnCopyIfFound(o, startOffset, endOffset);
			}
		}
	}

	private MigList.Page<T, T[]> removeAllImpl(Object o, int[] offsets, int startIx)
	{
		boolean changed = false;
		for (int i = offsets.length - 1; i >= startIx; i--) {
			int offset = offsets[i];
			if (Objects.equals(o, array[offset])) {
				removeIxImpl(offset);
				changed = true;
			}
		}
		return changed ? this : null;
	}

	// Fallback if no value hash map
	private MigList.Page<T, T[]> removeAllImpl(Object o, int startOffset, int endOffset)
	{
		int src = startOffset, dst = startOffset;
		for (; src < endOffset; src++) {
			T t = array[src];
			if (!Objects.equals(o, t))
				array[dst++] = t;
		}

		if (dst == src)
			return null; // No change

		// Clear end of array to not have a memory leak
		Arrays.fill(array, dst, size, null);
		size = dst;
		return this;
	}

	private MigList.Page<T, T[]> removeAllOnCopyIfFound(Object o, int[] offsets)
	{
		for (int i = 0; i < offsets.length; i++) {
			if (Objects.equals(array[offsets[i]], o))
				return copyPage().removeAllImpl(o, offsets, i);
		}
		return null;
	}

	// Fallback if no value hash map
	private MigList.Page<T, T[]> removeAllOnCopyIfFound(Object o, int startOffset, int endOffset)
	{
		for (int i = startOffset; i < endOffset; i++) {
			if (Objects.equals(array[i], o))
				return copyPage().removeAllImpl(o, i, endOffset);
		}
		return null;
	}

	public MigList.Page<T, T[]> removeRange(int startOffset, int endOffset)
	{
		if (startOffset == endOffset)
			return null;

		if (sharers == 0) {
			return removeRangeImpl(startOffset, endOffset);
		} else {
			return copyPage().removeRangeImpl(startOffset, endOffset);
		}
	}

	private MigList.Page<T, T[]> removeRangeImpl(int startOffset, int endOffset)
	{
		removeOffsetsFromHashMaps(startOffset, endOffset);

		int moveLen = size - endOffset;
		if (moveLen != 0)
			System.arraycopy(array, endOffset, array, startOffset, moveLen);

		int newSize = size - (endOffset - startOffset);
		Arrays.fill(array, newSize, size, null);
		size = newSize;
		return this;
	}

	public MigList.Page<T, T[]> retainAll(Collection <?> col, int startOffset, int endOffset)
	{
		if (sharers == 0) {
			return retainAllImpl(col, startOffset, endOffset);
		} else {
			return retainAllOnCopyIfFound(col, startOffset, endOffset);
		}
	}

	private MigList.Page<T, T[]> retainAllImpl(Collection<?> col, int startOffset, int endOffset)
	{
		int src = startOffset, dst = startOffset;
		for (; src < endOffset; src++) {
			T t = array[src];
			if (col.contains(t))
				array[dst++] = t;
		}

		int removedLen = src - dst;
		if (removedLen == 0) // Nothing changed
			return null;

		int moveLen = size - endOffset;
		if (moveLen > 0)
			System.arraycopy(array, src, array, dst, moveLen);

		// Clear end of array to not have a memory leak
		Arrays.fill(array, size - removedLen, size, null);
		size -= removedLen;

		rebuildHashMaps();

		return this;
	}

	private MigList.Page<T, T[]> retainAllOnCopyIfFound(Collection<?> col, int startOffset, int endOffset)
	{
		for (int ix = startOffset; ix < endOffset; ix++) {
			if (!col.contains(array[ix]))
				return copyPage().retainAllImpl(col, ix, endOffset);
		}
		return null;
	}

	public MigList.Page<T, T[]> clear()
	{
		if (size == 0)
			return null;

		if (sharers == 0) {
			return clearImpl();
		} else {
			return copyPage().clearImpl();
		}
	}

	private MigList.Page<T, T[]> clearImpl()
	{
		clearHashMaps();
		Arrays.fill(array, 0, size, null);
		size = 0;
		return this;
	}

	private void rebuildHashMaps()
	{
		if (valueHashMap != null)
			valueHashMap.setAll(array, size);

		for (int i = 0; i < hashMaps.length; i++) {
			HashIndexMap2 map = hashMaps[i];
			map.clear();
			Indexer<T> indexer = indexers[i];

			for (int offset = 0; offset < array.length; offset++)
				map.addLast(indexer.index(array[offset]), offset);
		}
	}

	private void removeFromHashMaps(int offset)
	{
		T t = array[offset];
		if (valueHashMap != null)
			valueHashMap.remove(t, offset);

		for (HashIndexMap2 map : hashMaps)
			map.remove(t, offset);
	}

	private void removeOffsetsFromHashMaps(int startOffset, int endOffset)
	{
		if (valueHashMap != null)
			valueHashMap.removeOffsets(startOffset, endOffset);

		for (HashIndexMap2 map : hashMaps)
			map.removeOffsets(startOffset, endOffset);
	}

	private void removeOffsetsFromHashMaps(int startOffset)
	{
		if (valueHashMap != null)
			valueHashMap.removeOffsets(startOffset);

		for (HashIndexMap2 map : hashMaps)
			map.removeOffsets(startOffset);
	}

	private void addToHashMaps(T element, int offset)
	{
		if (valueHashMap != null)
			valueHashMap.insert(element, offset);

		for (int i = 0; i < hashMaps.length; i++)
			hashMaps[i].insert(indexers[i].index(element), offset);
	}

	private void addLastToHashMaps(T element)
	{
		if (valueHashMap != null)
			valueHashMap.addLast(element, size);

		for (int i = 0; i < hashMaps.length; i++)
			hashMaps[i].addLast(indexers[i].index(element), size);
	}

	private void clearHashMaps()
	{
		if (valueHashMap != null)
			valueHashMap.clear();

		for (HashIndexMap2 map : hashMaps)
			map.clear();
	}

	public MigList.Page<T, T[]>[] split(int fromOffset)
	{
		IndexedObjectPage<T> newPage = new IndexedObjectPage<>(array.length, indexers, valueHashMap != null);
		int newPageSize = size - fromOffset;
		System.arraycopy(array, fromOffset, newPage.array, 0, newPageSize);
		newPage.size = newPageSize;
		newPage.rebuildHashMaps();

		if (sharers == 0) {
			Arrays.fill(array, fromOffset, size, null);
			removeOffsetsFromHashMaps(fromOffset);
			size = fromOffset;
			return new MigList.Page[] {this, newPage};
		} else {
			sharers--;
			IndexedObjectPage<T> pageClone = new IndexedObjectPage<>(array.length, indexers, valueHashMap != null);
			System.arraycopy(array, 0, pageClone.array, 0, fromOffset);
			pageClone.size = fromOffset;
			pageClone.rebuildHashMaps();
			return new MigList.Page[] {pageClone, newPage};
		}
	}
}