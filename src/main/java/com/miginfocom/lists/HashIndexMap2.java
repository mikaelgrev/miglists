package com.miginfocom.lists;

import java.util.Arrays;
import java.util.HashMap;

/** Maps one or more hash (int) to a set of sorted offsets (ints) used somewhere else, like a list. All offsets for a hash can be returned and they are then sorted.
 * All ints are stored in one array of set length which makes this efficient for small sizes but not for large.<p>
 * Basically this is a SortedIntToSortedIntsMultiMap
 * It is custom made for use in a {@link com.miginfocom.lists.MigList.Page}
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 26/10/13
 *         Time: 11:37 AM
 */
final class HashIndexMap2
{
	private static final int[] NO_HIT = new int[0];

	// [hash, offset, hash, offset, ...]  the element index is called "index"
	private final int[] array;
	private int size2;

	HashIndexMap2(int hashCount)
	{
		array = new int[(int) ((hashCount << 1) * 1.1) & 0xFFFFFFFE];
	}

	int size()
	{
		return size2 >> 1;
	}

	void insert(Object o, int offset)
	{
		if (offset == size()) {
			addLast(o, offset);
			return;
		}
		System.out.println("no1");
		int hash = getHash(o);
		int ix = 0;

		// First find the hash insert point and increment offsets on the way
		for (; ix < size2; ix += 2) {
			int arrHash = array[ix];
			if (arrHash < hash) {
				// Increment all offsets after 'offset'.
				int arrOffset = array[ix + 1];
				if (arrOffset >= offset)
					array[ix + 1] = arrOffset + 1;

			} else if (arrHash > hash || offset <= array[ix + 1]) {
				// If at insertion point, defer to next loop
				break;
			}
		}

		int len = size2 - ix;
		if (len > 0)
			System.arraycopy(array, ix, array, ix + 2, len);

		array[ix++] = hash;
		array[ix++] = offset;
		size2 += 2;

		for (ix++; ix < size2; ix += 2) {
			int arrOffset = array[ix];
			if (arrOffset >= offset)
				array[ix] = arrOffset + 1;
		}
	}

//	private static long countAll = 0, countCopy = 0;
	void addLast(Object o, int offset)
	{
		int hash = getHash(o);

		int insertIx = findIndexAfterHash(hash);

//		countAll += size2;

		if (insertIx == array.length || array[insertIx] != 0) {
			int freeIx = findClosestFreeIndexAround(insertIx);
			if (freeIx < insertIx) {
				int len = insertIx - freeIx - 2;
				if (len > 0) {
					System.arraycopy(array, freeIx + 2, array, freeIx, len);
//					countCopy += len;
				}
				insertIx -= 2;
			} else if (freeIx < insertIx) {
				int len = freeIx - insertIx;
				if (len > 0) {
					System.arraycopy(array, insertIx, array, insertIx + 2, len);
//					countCopy += len;
				}
			}
		}

//		if (countAll % 1000000 == 23234) {
//			System.out.println("Copy perc: " + countCopy / (float) countAll * 100);
//		}

		array[insertIx] = hash;
		array[insertIx + 1] = offset;
		size2 += 2;
	}

	/** Finds the first free slot that is closest to hashIx.
	 * There must be a free index to call this method or an exception will be thrown.
	 * @param hashIx The starting point, but the index itself it not checked!
	 * @return The index.
	 * @throws IllegalStateException If there was no free slot in the array.
	 */
	private int findClosestFreeIndexAround(int hashIx)
	{
		int downIx = hashIx - 2;
		int upIx = hashIx + 2;

		// First try to go up and down in the same loop so we can find the closest index
		while (downIx >= 0 && upIx < array.length) {
			if (array[downIx] == 0)
				return downIx;

			if (array[upIx] == 0)
				return upIx;

			downIx -= 2;
			upIx += 2;
		}

		// If one loop hits < 0 or >= size continue with the loop that is left
		if (downIx >= 0) {
			do {
				if (array[downIx] == 0)
					return downIx;
				downIx -= 2;
			} while (downIx>= 0);
		} else {
			do {
				if (array[upIx] == 0)
					return upIx;
				upIx += 2;
			} while (upIx < array.length);
		}

		throw new IllegalStateException("Could not find index around index: " + hashIx);
	}

	static int getHash(Object t)
	{
		return t != null ? rehash(t.hashCode()) : Integer.MAX_VALUE;
	}

	private static int rehash(int x)
	{
		x ^= (x << 21);
		x ^= (x >>> 3);
		x ^= (x << 4);
		return x != 0 ? x : 1; // 0 is reserved for "free".
	}

	void removeOffsets(int fromOffset)
	{
		System.out.println("no2");
		int src = 0, dst = 0;
		for (; src < size2; src += 2) {
			int offset = array[src + 1];
			if (offset < fromOffset) {
				array[dst++] = array[src];
				array[dst++] = offset;
			}
		}

		size2 = dst;
	}

	void removeOffsets(int startOffset, int endOffset)
	{
		System.out.println("no3");
		int diff = endOffset - startOffset;
		int src = 0, dst = 0;
		for (; src < size2; src += 2) {
			int offset = array[src + 1];
			if (offset < startOffset) {
				array[dst++] = array[src];
				array[dst++] = offset;
			} else if (offset >= endOffset) {
				array[dst++] = array[src];
				array[dst++] = offset - diff;
			}
		}

		size2 = dst;
	}

	void remove(Object t, int offset)
	{
		System.out.println("no4");
		int hash = getHash(t);
		int ix = 0;

		// First find the hash insert point and increment offsets on the way
		for (; ix < size2; ix += 2) {
			int arrHash = array[ix];
			int arrOffset = array[ix + 1];

			// If at insertion point, defer to next loop
			if (arrHash == hash && offset == arrOffset)
				break;

			// Decrement all offsets after 'offset'.
			if (arrOffset >= offset)
				array[ix + 1] = arrOffset - 1;
		}

		// Then move array one left decrementing offsets along the way
		for (size2 -= 2; ix < size2; ix += 2) {
			array[ix] = array[ix + 2];
			int arrOffset = array[ix + 3];
			array[ix + 1] = arrOffset >= offset ? arrOffset - 1 : arrOffset;
		}

		//		assertStructure();
	}

	void removeOffset(int offset)
	{
		int ix = 1;

		// First find the hash insert point and increment offsets on the way
		for (; ix < size2; ix += 2) {
			int arrOffset = array[ix];

			// Decrement all offsets after 'offset'.
			if (arrOffset > offset) {
				array[ix] = arrOffset - 1;
			} else if (offset == arrOffset) { // If at insertion point, defer to next loop
				break;
			}
		}
		ix--;

		// Then move array one left decrementing offsets along the way
		for (size2 -= 2; ix < size2; ix += 2) {
			array[ix] = array[ix + 2];
			int arrOffset = array[ix + 3];
			array[ix + 1] = arrOffset >= offset ? arrOffset - 1 : arrOffset;
		}

		//		assertStructure();
	}

	int[] getOffsets(Object o)
	{
		return getOffsets(getHash(o), 0, Integer.MAX_VALUE);
	}

	int[] getOffsets(Object o, int minOffset, int maxOffset)
	{
		return getOffsets(getHash(o), minOffset, maxOffset);
	}

	/**
	 * @param hash Must be generated with {@link #getHash(Object)}!
	 * @param minOffset
	 * @param maxOffset Excluding
	 * @return
	 */
	private int[] getOffsets(int hash, int minOffset, int maxOffset)
	{
		long loHiIndex = findLoHiIndex(hash, minOffset, maxOffset);
		int lo = (int) loHiIndex;
		int hi = (int) (loHiIndex >> 32);
		if (lo == hi)
			return NO_HIT;

		int size = (hi++ - lo++) >> 1;
		int[] retArr = new int[size];
		for (int ix = 0; lo < hi; lo += 2, ix++)
			retArr[ix] = array[lo];
		return retArr;
	}

	/** Returns the index that has a arrHash that is same o less than hash.
	 * @param hash
	 * @return
	 */
	private int findIndexAfterHash(int hash)
	{
		int offset = estimateOffset(hash);
		int arrHash = array[offset];
		if (arrHash == 0)
			return offset; // Fast path for addLast, we have not former element with that hash.

		if (arrHash > hash) { // We overshot, move down until next is or hash, or empty.
			while (offset > 0 && (arrHash = array[offset - 2]) > hash && arrHash != 0)
				offset -= 2;
		} else {  // We undershot, move up until we find higher than hash or empty slot
			do {
				offset+= 2;
			} while(offset < array.length && (arrHash = array[offset]) <= hash && arrHash != 0);
		}
		return offset;
	}

//	private static long countPlus = 0;
//	private static long countMinus = 0;
//	private static double diffPlusPerc = 0;
//	private static double diffMinusPerc = 0;

	/** Finds the lowest and highest indexes that has the hash and offset between startOffset and endOffset.
	 * As soon as a free slot in found it is returned since it is guaranteed that an estimated hash index will have connection
	 * to the sought for hash without any free gaps.
	 * @param hash
	 * @param startOffset
	 * @param endOffset
	 * @return
	 */
	private long findLoHiIndex(int hash, int startOffset, int endOffset)
	{
		int loOffset = estimateOffset(hash);

//		int estIff = loOffset;

		int arrHash = array[loOffset];
		if (arrHash == 0)
			return ((long) loOffset << 32) | loOffset; // fast path for free slot

		if (arrHash > hash) { // We overshot, first move down to find hash group
			while (loOffset > 0 && (arrHash = array[loOffset - 2]) > hash && arrHash != 0)
				loOffset -= 2;
		} else if (arrHash < hash) {  // We undershot, first move up to find hash group
			do {
				loOffset += 2;
			} while(loOffset < array.length && (arrHash = array[loOffset]) < hash && arrHash != 0);
		}

//		double diffPerc = diff / (double) array.length;
//		if (diff > 0) {
//			countPlus++;
//			diffPlusPerc += diffPerc;
//		} else {
//			countMinus++;
//			diffMinusPerc += diffPerc;
//		}
//		if ((countMinus + countPlus) % 1000000 == 239420) {
//			System.out.println("diffPlus: " + diffPlusPerc / countPlus + ", count: " + countPlus);
//			System.out.println("diffMinus: " + diffMinusPerc / countMinus + ", count: " + countMinus);
//			System.out.println("");
//		}

		// Not found, bail out.
		if (arrHash != hash || loOffset == array.length)
			return ((long) loOffset << 32) | loOffset;

		// Go back with the lower index
		while (loOffset > 0 && array[loOffset - 2] == hash && array[loOffset - 1] >= startOffset)
			loOffset -= 2;

		// Go forward with the lower index since we might have started too early in last loop
		// We can actually go higher than search bounds since we might not actually have a hit due to startOffset and endOffset
		while (loOffset < array.length && array[loOffset] == hash && array[loOffset + 1] < startOffset)
			loOffset += 2;

		int hiOffset = loOffset;
		while(hiOffset < array.length && array[hiOffset] == hash && array[hiOffset + 1] < endOffset)
			hiOffset += 2;

		//		long loHiIndex = findLoHiIndexBS(hash, startOffset, endOffset);
		//		int lo = (int) loHiIndex;
		//		int hi = (int) (loHiIndex >> 32);
		//		if (lo != loOffset || hi != hiOffset)
		//			throw new IllegalStateException(lo + " != " + loOffset + "  or  " + hi + " != " + hiOffset);

		return ((long) hiOffset << 32) | loOffset;
	}

	private static final float MAXx2 = (long) Integer.MAX_VALUE << 1;
	private int estimateOffset(long hash)
	{
		hash += (long) Integer.MAX_VALUE + 1;
		float factor = hash / MAXx2;
		return (int) ((array.length - 1) * factor) & 0xFFFFFFFE;
	}

	public void setAll(Object[] objects, int count)
	{
		clear();
		for (int offset = 0; offset < count; offset++)
			addLast(objects[offset], offset);
	}

	public void clear()
	{
		size2 = 0;
		Arrays.fill(array, 0);
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		int prevHash = Integer.MAX_VALUE - 1;
		for (int i = 0; i < size2;) {
			int hash = array[i++];
			int offset = array[i++];
			if (hash != prevHash) {
				sb.append("\n").append(hash).append(" -> ").append(offset);
				prevHash = hash;
			} else {
				sb.append(", ").append(offset);
			}
		}
		return sb.toString() + "\n";// + DEB;
	}

	//	private static final StringBuilder DEB = new StringBuilder(128);

	private void assertStructure()
	{
		if (size() == 0)
			return;

		if (size2 > array.length)
			throw new IllegalStateException("Size to big " + size2 + ", len: " + array.length + "\n" + toString());

		HashMap<Integer, Integer> offsets = new HashMap<>();
		for (int i = 0; i < size2; i += 2)
			offsets.put(i / 2, array[i]);

		int prevHash = array[0];
		int prevOffset = array[1];

		offsets.remove(prevOffset);

		for (int i = 2; i < size2;) {
			int hash = array[i++];
			int offset = array[i++];

			offsets.remove(offset);

			if (hash != prevHash) {
				if (hash < prevHash)
					throw new IllegalStateException("Hash error for offset " + offset + "   " + hash + " < " + prevHash + "\n" + toString());
			} else {
				if (prevOffset == offset)
					throw new IllegalStateException("Same offset " + offset + " for  " + hash + "\n" + toString());

				if (prevOffset > offset)
					throw new IllegalStateException("Offset " + offset + " < " + prevOffset + " for  " + hash + "\n" + toString());
			}

			prevHash = hash;
			prevOffset = offset;
		}

		if (!offsets.isEmpty()) {
			throw new IllegalStateException("Not all offsets where found. " + offsets + "\n" + toString());
		}
	}
}
