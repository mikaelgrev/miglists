package com.miginfocom.lists;

import com.google.common.primitives.Ints;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 2013-10-22
 *         Time: 22:35
 */
public class Pipe<T> implements ReadPipe<T>, WritePipe<T> // todo make package private
{
	final int[] array;
	private final int objSize;

	// These are modified even for read operations
	int index, endIx;

	public Pipe(int[] array, int objSize)// todo make package private
	{
		this.array = array;
		this.objSize = objSize;
	}

	public void setFlatOffset(int ix)// todo make package private
	{
		index = ix;
		endIx = ix + objSize;
	}

	public void setToWhole()// todo make package private
	{
		index = 0;
		endIx = objSize;
	}

	// ****************** Read *******************************

	public byte[] readBytes()
	{
		checkBefore4();
		return Ints.toByteArray(array[index++]);
	}

	public void readBytes(byte[] target)
	{
		checkBefore4();
		int i = array[index++];
		target[0] =	(byte) (i >> 24);
		target[1] =	(byte) (i >> 16);
		target[2] =	(byte) (i >> 8);
		target[3] =	(byte) i;
	}

	public short[] readShorts()
	{
		checkBefore4();
		int i = array[index++];
		return new short[] {
			(short) (i >> 16),
			(short) i};
	}

	public void readShorts(short[] target)
	{
		checkBefore4();
		int i = array[index++];
		target[0] =	(byte) (i >> 16);
		target[1] =	(short) i;
	}

	public int readInt()
	{
		checkBefore4();
		return array[index++];
	}

	public int readInt(int ix)
	{
		ix += index;
		checkIx(ix);
		return array[ix];
	}

	public long readLong()
	{
		checkBefore8();
		return ((long) array[index++] & 0xFFFFFFFFL) | ((long) array[index++] << 32);
	}

	public long readLong(int ix)
	{
		ix += index;
		checkIx(ix + 1);
		return ((long) array[ix++] & 0xFFFFFFFFL) | (((long) array[ix]) << 32);
	}

	public float readFloat()
	{
		return Float.intBitsToFloat(readInt());
	}

	public float readFloat(int ix)
	{
		return Float.intBitsToFloat(readInt(ix));
	}

	public double readDouble()
	{
		return Double.longBitsToDouble(readLong());
	}

	public double readDouble(int ix)
	{
		return Double.longBitsToDouble(readLong(ix));
	}

	// ****************** Write *******************************

	public void writeBytes(byte b1, byte b2, byte b3, byte b4)
	{
		checkBefore4();
		array[index++] = Ints.fromBytes(b1, b2, b3, b4);
	}

	public void writeShorts(short s1, short s2)
	{
		checkBefore4();
		array[index++] = s1 << 16 | (s2 & 0xFFFF);
	}

	public void writeInt(int i)
	{
		checkBefore4();
		array[index++] = i;
	}

	public void writeLong(long l)
	{
		checkBefore8();
		array[index++] = (int) l;
		array[index++] = (int) (l >> 32);
	}

	public void writeFloat(float f)
	{
		writeInt(Float.floatToRawIntBits(f));
	}

	public void writeDouble(double d)
	{
		writeLong(Double.doubleToRawLongBits(d));
	}

	// ****************** Checks *******************************

	private void checkIx(int ix)
	{
		if (ix >= endIx)
			throw new IllegalArgumentException(ix + " bytes will not fit. Index: " + this.index + " and endIx: " + endIx);
	}

	private void checkBefore4()
	{
		if (index == endIx)
			throw new ArrayIndexOutOfBoundsException("Accessed int index " + index + " for end: " + endIx);
	}

	private void checkBefore8()
	{
		if (index == endIx - 1)
			throw new ArrayIndexOutOfBoundsException("Accessed long index " + index + " for end: " + endIx);
	}
}
