package test;

import com.miginfocom.lists.IndexMigList;
import com.miginfocom.lists.Indexer;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 09/11/13
 *         Time: 9:46 AM
 */
public class IndexedListTest
{
	/**
	 * @param
	 * @return
	 */
	@Test
	public void test1()
	{
		Indexer<String> indexer = new Indexer<String>() {
			public Integer index(String s) {
				return s.length();
			}
		};

		IndexMigList<String> list = new IndexMigList<>(false, indexer);
		list.add("hello");
		list.add("I");
		list.add("am");
		list.add("Mikael");
		list.add("Greve");

		Assert.assertArrayEquals(list.getIndexed(indexer, 0, 1).toArray(), new Object[] {});
		Assert.assertArrayEquals(list.getIndexed(indexer, 2, 1).toArray(), new Object[] {"am"});
		Assert.assertArrayEquals(list.getIndexed(indexer, 5, 2).toArray(), new Object[] {"hello", "Greve"});
		Assert.assertArrayEquals(list.getIndexed(indexer, 5, 1).toArray(), new Object[] {"hello"});
	}
}
