package test;

import com.miginfocom.lists.IndexMigList;
import com.miginfocom.lists.Indexer;
import com.miginfocom.lists.MigList;
import com.miginfocom.lists.MigListBase;
import com.miginfocom.lists.MigListFlat;
import com.miginfocom.lists.MigListInt;
import static test.FlatTester.Vec;
import static test.FlatTester.VecSer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 2013-09-26
 *         Time: 21:38
 */
public class MigListBench
{
	private static final Random RANDOM = new Random(1);
	private static final int[] RND = new int[10000];
	static {
		for (int i = 0; i < RND.length; i++)
			RND[i] = Math.abs(RANDOM.nextInt());
	}

	public static void main2(String[] args)
	{
	}

	public static int rehash2(int x)
	{
		x ^= (x << 1);
		x ^= (x >>> 3);
		x ^= (x << 10);
		return x;
	}

	public static int rehash4(int x) {
		x ^= (x << 21);
		x ^= (x >>> 3);
		x ^= (x << 4);
		return x;
	}

	public static int spread(int hash)
	{
		return hash ^ (hash >>> 16);
	}

	public static int rehash(int hash)
	{
		return rehash4(hash);
	}

	public static final boolean BENCHMARKING = false;
	public static Object retSink = null;

	private static Vec[] vecs;
	private static Vec[] vecs2;

	public static void main(String[] args)
	{
//		int ror = 8;
//		int sz = 1 << ror;
//		ArrayList <AtomicInteger> counts = new ArrayList<>(sz);
//		for (int i = 0; i < sz; i++)
//			counts.add(new AtomicInteger());
//
//		long n1 = System.nanoTime();
//		int hashMask = 0xFFFFFFFF >>> ror;
////		System.out.println(Integer.toHexString(hashMask));
//
//		int offsetMask = (1 << ror) - 1;
////		System.out.println(Integer.toHexString(offsetMask));
//
//
//		for (int i = 0; i < Math.round(sz * 10000); i++) {
//			int hash = new Vec(RANDOM.nextInt(sz / 2) + 93423423, 0, 0, 0).hashCode();
//			counts.get(Math.abs(rehash(hash) & offsetMask)).incrementAndGet();
//		}
//		System.out.println("Timed: " + ((System.nanoTime() - n1) / 1000000f));
//
//		for (AtomicInteger i : counts) {
//			System.out.print(i + ",");
//		}

//		System.out.println("Integer.MIN_VALUE, 100: " + estimateOffset(Integer.MIN_VALUE, 100));
//		System.out.println("Integer.MAX_VALUE, 100: " + estimateOffset(Integer.MAX_VALUE, 100));
//
//		System.out.println("Integer.MIN_VALUE, Integer.MAX_VALUE: " + estimateOffset(Integer.MIN_VALUE, Integer.MAX_VALUE));
//		System.out.println("Integer.MAX_VALUE, Integer.MAX_VALUE: " + estimateOffset(Integer.MAX_VALUE, Integer.MAX_VALUE));
//
//		System.out.println("Integer.MIN_VALUE, 1: " + estimateOffset(Integer.MIN_VALUE, 1));
//		System.out.println("Integer.MAX_VALUE, 1: " + estimateOffset(Integer.MAX_VALUE, 1));
//		System.out.println("Integer.MAX_VALUE, 2: " + estimateOffset(Integer.MAX_VALUE, 2));
//		System.out.println("Integer.MAX_VALUE, 3: " + estimateOffset(Integer.MAX_VALUE, 3));
//		System.out.println("Integer.MAX_VALUE, 4: " + estimateOffset(Integer.MAX_VALUE, 4));
//
//		System.out.println("0, 100: " + estimateOffset(0, 100));
//		System.out.println("");
//
//		long nn = System.nanoTime();
//
//		long l = 0;
//		l = mes(l);
//		System.out.println("Timed: " + ((System.nanoTime() - nn) / 1000000f));
//
//		System.out.println(l);
//		System.exit(0);



		long nanos = 0, nanosCreate = 0;

		int runs = 10;
		int size = 100_000;

		//		final int[] arr1 = new int[1000000];
		//		final int[] arr2 = new int[1000000];
		//		for (int i = 0; i < 4; i++) {
		//			Thread t = new Thread(new Runnable() {
		//				public void run()
		//				{
		//					while (true) {
		//						System.arraycopy(arr2, 0, arr1, 0, arr1.length);
		//					}
		//				}
		//			});
		//			t.setDaemon(true);
		//			t.setPriority(9);
		//			t.start();
		//		}

		//		int[] rndArr = createRandomArray(size, Integer.MAX_VALUE);
		//		int[] rndDecArr = createRandomDecArray(size);

//		Indexer<Vec>[] indexers = new Indexer[0];
//		Indexer<Vec>[] indexers = new Indexer[] {Vec.VEC_1000_INDEXER};
		Indexer<Vec>[] indexers = new Indexer[] {Vec.VEC_INDEXER};

		vecs = new Vec[size];
		for (int i = 0; i < vecs.length; i++)
			vecs[i] = new Vec();

		vecs2 = new Vec[size];
		for (int i = 0; i < vecs2.length; i++)
			vecs2[i] = new Vec();

		//		System.out.println("MigListBase Mem size: " + MemoryMeasurer.measureBytes(createMigList(size)));
//		System.out.println("MigListFlat Mem size: " + MemoryMeasurer.measureBytes(createMigListFlat(size)));
//		System.out.println("ArrayList   Mem size: " + MemoryMeasurer.measureBytes(createArrayList(size)));
//		System.out.println("createIndexedMigList   Mem size: " + MemoryMeasurer.measureBytes(createIndexedMigList(indexers, size)));
//		System.out.println("createValueIndexedMigList   Mem size: " + MemoryMeasurer.measureBytes(createValueIndexedMigList(indexers, size)));
//		System.out.println("createIndexedArrayList   Mem size: " + MemoryMeasurer.measureBytes(createIndexedArrayList(indexers, size)));

		long sum = 0;

		try {
			Thread.sleep(300);
		} catch (InterruptedException ignored) {}
		System.gc();

//	    List<Vec> list = createArrayList(size);
//		MigList<Vec> list = createMigList(size);
		IndexMigList<Vec> list = createIndexedMigList(indexers, size);
//		IndexMigList<Vec> list = createValueIndexedMigList(indexers, size);
//		IndexedArrayList<Vec> list = createIndexedArrayList(indexers, size);
		for (int j = 0; j < runs + 10; j++) {

//			System.gc();

			long nCr = System.nanoTime();

//		    List<Vec> list = createArrayList(size);
//			MigList<Vec> list = createMigList(size);
//			IndexMigList<Vec> list = createIndexedMigList(indexers, size);
//			IndexMigList<Vec> list = createValueIndexedMigList(indexers, size);
//			IndexedArrayList<Vec> list = createIndexedArrayList(indexers, size);
//			MigList<Vec> list = createMigListFlat(size);
//			MigListInt list = createNewListInt(size);
//			List<Vec> list = createCOWArrayList(size);
//			List<Vec> list = createLinkedList(size);
//			Vector<Vec> list = createVector(size);
//			List<Vec> list = Collections.synchronizedList(createArrayList(size));

//			System.gc();

			if (j >= 10)
				nanosCreate += System.nanoTime() - nCr;

			long n = System.nanoTime();

//			doTestAdd(list, 1000000);
//			doTestGetLinear(list);
//			doTestGetMiddle(list);
//			doTestGetRandom(list);
//			sum = doTestGetForEach(list);
//			sum = doTestGetIteratorNoOpt(list);
//			sum = doTestGetIterator(list);

			sum += doGetIndexed(list, indexers[0], 0);
//			sum += doIndexOf(list);
//			sum += doTestLots(list);
//			doTestAddRandom(list, size);
//			doTestRemoveIxRandom(list);
//			doTestAddGetIxRemoveObjRandom(list);
//			doTestAddThenRemoveRandom(list, size);

//			System.gc();

//			doTestRemoveLast(list);
//			doTestRemoveMiddle(list);
			if (j >= 10)
				nanos += System.nanoTime() - n;

//			System.gc();
//			try {
//				Thread.currentThread().sleep(10);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//			if (list instanceof MigList)
//				((MigObjectList) list).assertStructure();
		}

		//		System.out.println(newList.toString());

		System.out.println("Create: " + (nanosCreate / 1000000f));
		System.out.println("Tested: " + (nanos / 1000000f));
		System.out.println("Chksum: " + sum);
		//		ArrayTest<Integer> list = new ArrayTest<>();
		//		for (int i = 0; i < 1000; i++)
		//			list.add(i);
		//		System.out.println("");
		System.exit(0);
	}

	private static void doTestAdd(List<Integer> list, int max)
	{
		for (int i = 0; i < max; i++)
			list.add(i);
	}

	private static void doTestAdd(MigListInt list, int max)
	{
//		for (int i = 0; i < max; i++)
//			list.addInt(i);
	}

	public static int f = 1293;
	private static Object doTestGetRandom(List<Vec> list)
	{
		Object o = null;
		for (int i = 0, iSz = list.size(); i < iSz; i++) {
			o = list.get(RND[i % RND.length] % iSz);
			if (f == 123.1)
				o = null;
		}
		return o;
	}

	private static long doTestGetIteratorNoOpt(List<Vec> list)
	{
		long sum = 0;
		for (Iterator<Vec> it = list.iterator(); it.hasNext();) {
			Vec v = it.next();
			sum += v.hashCode();
			if (f == 123)
				it.remove(); // Without this "threat" the loop is optimized exactly like a for-each
		}
		return sum;
	}

	private static long doTestGetIterator(List<Vec> list)
	{
		long sum = 0;
		for (Iterator<Vec> it = list.iterator(); it.hasNext();) {
			Vec v = it.next();
			sum += v.hashCode();
			if (f == 123)
				System.out.println("efr");
		}
		return sum;
	}

	private static long doTestGetForEach(List<Vec> list)
	{
		// Note. for-each is very optimized for ArrayList And CopyOnWriteArrayList and don't use the standard Itr.
		long sum = 0;
		for (Vec v : list) {
			sum += v.hashCode();
			if (f == 123) // Just to make it same as doTestGetIterator()
				System.out.println("wed");
		}

		return sum;
	}

	private static Object doTestGetLinear(List<Vec> list)
	{
		Object l = null;
		for (int i = 0, iSz = list.size(); i < iSz; i++)
			l = list.get(i);
		return l;
	}


	private static Object doTestGetMiddle(List<Vec> list)
	{
		Object l = null;
		for (int i = 0, iSz = list.size(), iMid = iSz / 2; i < iSz; i++) {
			l = list.get(iMid);
		}
		return l;
	}

	private static void doTestAddRandom(List<Vec> list, int maxSize)
	{
		list.clear();
		list.add(new Vec());

		for (int i = 0; i < maxSize; i++)
			list.add(RANDOM.nextInt(list.size()), vecs[(int) i]);
	}

	private static void doTestAddThenRemoveRandom(List<Vec> list, int maxSize)
	{
		Random r = new Random();

		list.clear();
		list.add(null);

		for (int i = 0; i < maxSize; i++)
			list.add(r.nextInt(list.size()), null);

		while (list.size() > 1)
			list.remove(r.nextInt(list.size()));
	}

	private static long doTestAddGetIxRemoveObjRandom(List<Vec> list)
	{
		long l = 0;
		for (int i = 0, iSz = list.size() / 2; i < iSz; i++) {
			list.add(vecs[(int) i]);
			list.remove(new Vec(list.get(RANDOM.nextInt(list.size()))));
		}
		return l;
	}

	private static long doTestLots(List<Vec> list)
	{
		long sum = 0;
		for (int i = 0, iSz = list.size() / 2; i < iSz; i++) {
			list.add(vecs[(int) i]);
			list.remove(RANDOM.nextInt(list.size()));
			list.remove(new Vec(list.get(RANDOM.nextInt(list.size()))));
			sum += list.indexOf(new Vec(list.get(RANDOM.nextInt(list.size()))));
		}
		return sum;
	}

	private static long doGetIndexed(IndexMigList<Vec> list, Indexer<Vec> indexer, double hitRate)
	{
		long ix = 0;
		int size = list.size();
		for (int i = 0, iSz = list.size() / 2; i < iSz; i++) {
			Object index = indexer.index(RANDOM.nextDouble() > hitRate ? vecs2[RANDOM.nextInt(size)] : vecs[RANDOM.nextInt(size)]);
			ix += list.getIndexed(indexer, index, 1).size();
//			ix += list.getIndexed(indexer, index, 1, 0, size).hashCode();
		}
		return ix;
	}

	private static long doGetIndexed(IndexedArrayList<Vec> list, Indexer<Vec> indexer, double hitRate)
	{
		long ix = 0;
		int size = list.size();
		for (int i = 0, iSz = list.size() / 2; i < iSz; i++) {
			Object index = indexer.index(RANDOM.nextDouble() > hitRate ? vecs2[RANDOM.nextInt(size)] : vecs[RANDOM.nextInt(size)]);
			ix += list.getIndexed(indexer, index).size();
		}
		return ix;
	}

	private static long doIndexOf(List<Vec> list)
	{
		long ix = 0;
		for (int i = 0, iSz = list.size() / 2; i < iSz; i++) {
			Vec vec = list.get(RANDOM.nextInt(list.size()));
			ix += list.indexOf(new Vec(vec));
		}
		return ix;
	}

	private static long doTestRemoveIxRandom(List<Vec> list)
	{
		long l = 0;
		while (list.size() > 0) {
			list.remove(RANDOM.nextInt(list.size()));
		}
		return l;
	}

	private static void doTestRemoveFirst(List<Integer> list)
	{
		while (list.size() > 0) {
			list.remove(0);
		}
	}

	private static void doTestRemoveLast(List<Integer> list)
	{
		while (list.size() > 0) {
			list.remove(list.size() - 1);
		}
	}

	private static void doTestRemoveMiddle(List<Integer> list)
	{
		while (list.size() > 0) {
			list.remove(list.size() / 2);
		}
	}

	private static MigListInt createNewListInt(long size)
	{
		MigListInt list = new MigListInt();
		for (long i = 0; i < size; i++)
			list.add((int) i);
		return list;
	}

	private static MigList<Vec> createMigList(long size)
	{
		MigListBase<Vec, Vec> list = new MigListBase<>();
		for (long i = 0; i < size; i++)
			list.add(vecs[(int) i]);

//		list.ensureIndexUpdatedForPage(Integer.MAX_VALUE);
		return list;
	}

	private static IndexMigList<Vec> createValueIndexedMigList(Indexer<Vec>[] indexers, long size)
	{
		IndexMigList<Vec> list = new IndexMigList<>(true, indexers);
		for (long i = 0; i < size; i++)
			list.add(vecs[(int) i]);

		//		list.ensureIndexUpdatedForPage(Integer.MAX_VALUE);
		return list;
	}

	private static IndexMigList<Vec> createIndexedMigList(Indexer<Vec>[] indexers, long size)
	{
		IndexMigList<Vec> list = new IndexMigList<>(false, indexers);
		for (long i = 0; i < size; i++)
			list.add(vecs[(int) i]);

		//		list.ensureIndexUpdatedForPage(Integer.MAX_VALUE);
		return list;
	}

	private static IndexedArrayList<Vec> createIndexedArrayList(Indexer<Vec>[] indexers, long size)
	{
		IndexedArrayList<Vec> list = new IndexedArrayList<>(indexers);
		for (long i = 0; i < size; i++)
			list.add(vecs[(int) i]);

		//		list.ensureIndexUpdatedForPage(Integer.MAX_VALUE);
		return list;
	}

	private static MigList<Vec> createMigListFlat(long size)
	{
		MigListFlat<Vec> list = new MigListFlat<>(new VecSer());
		for (long i = 0; i < size; i++)
			list.add(vecs[(int) i]);

		//		list.ensureIndexUpdatedForPage(Integer.MAX_VALUE);
		return list;
	}

	private static ArrayList<Vec> createArrayList(long size)
	{
		ArrayList<Vec> list = new ArrayList<>();
		for (long i = 0; i < size; i++)
			list.add(vecs[(int) i]);
		return list;
	}

	private static CopyOnWriteArrayList<Vec> createCOWArrayList(long size)
	{
		Vec[] arr = new Vec[(int) size];
		System.arraycopy(vecs, 0, arr, 0, arr.length);

		return new CopyOnWriteArrayList<>(arr);
	}

	private static LinkedList<Vec> createLinkedList(long size)
	{
		LinkedList<Vec> list = new LinkedList<>();
		for (int i = 0; i < size; i++)
			list.add(vecs[i]);
		return list;
	}

	private static Vector<Vec> createVector(long size)
	{
		Vector<Vec> list = new Vector<>();
		for (long i = 0; i < size; i++)
			list.add(vecs[(int) i]);
		return list;
	}

	public static int[] createRandomArray(int size, int maxIx)
	{
		int[] arr = new int[size];
		Random r = new Random(1);
		for (int i = 0; i < arr.length; i++)
			arr[i] = r.nextInt(maxIx);
		return arr;
	}

	public static int[] createRandomDecArray(int size)
	{
		int[] arr = new int[size];
		Random r = new Random(1);
		for (int i = arr.length - 1; i >= 0; i--)
			arr[i] = r.nextInt(Math.max(i, 1));
		return arr;
	}
}
