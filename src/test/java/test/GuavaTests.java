package test;

import com.google.common.collect.testing.ListTestSuiteBuilder;
import com.google.common.collect.testing.TestStringListGenerator;
import com.google.common.collect.testing.features.CollectionFeature;
import com.google.common.collect.testing.features.CollectionSize;
import com.google.common.collect.testing.features.ListFeature;
import com.miginfocom.lists.MigListBase;
import junit.framework.Test;
import junit.framework.TestCase;

import java.lang.reflect.Method;
import java.util.List;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 9/29/13
 *         Time: 22:30 PM
 */
public class GuavaTests extends TestCase
{
	public static Test suite() {
		return ListTestSuiteBuilder.using(
			new TestStringListGenerator() {
				protected List<String> create(String[] elements)
				{
					return new MigListBase<>(elements);
				}
			})
			// The name of the test suite
			.named("MigList Tests")
				// Here we give a hit what features our collection supports
			.withFeatures(ListFeature.SUPPORTS_SET,
			              ListFeature.SUPPORTS_ADD_WITH_INDEX,
			              ListFeature.SUPPORTS_REMOVE_WITH_INDEX,
			              CollectionFeature.ALLOWS_NULL_VALUES,
//			              CollectionFeature.SERIALIZABLE,
//			              CollectionFeature.FAILS_FAST_ON_CONCURRENT_MODIFICATION,
			              CollectionSize.ANY)

//			.suppressing(Arrays.asList(suppressMethod(ListListIteratorTester.class, "testSubList_entireList")))
//			.suppressing(Arrays.asList(suppressClass(ListSubListTester.class)))
			.createTestSuite();
	}

//	public static Test suite() {
//		return ListTestSuiteBuilder.using(
//			new TestStringListGenerator() {
//				protected List<String> create(String[] elements)
//				{
//					return new MigListFlat<>(new FlatTester.StringSer(), elements);
//				}
//			})
//			// The name of the test suite
//			.named("MigList Flat Tests")
//				// Here we give a hit what features our collection supports
//			.withFeatures(ListFeature.GENERAL_PURPOSE,
//			              CollectionFeature.ALLOWS_NULL_VALUES,
//				//			              CollectionFeature.SERIALIZABLE,
//				//			              CollectionFeature.FAILS_FAST_ON_CONCURRENT_MODIFICATION,
//			              CollectionSize.ANY)
//
//				//			.suppressing(Arrays.asList(suppressMethod(ListListIteratorTester.class, "testSubList_entireList")))
//				//			.suppressing(Arrays.asList(suppressClass(ListSubListTester.class)))
//			.createTestSuite();
//	}

	private static Method suppressMethod(Class testClass, String methodName)
	{
		try {
			return testClass.getMethod(methodName, new Class[0]);
		} catch (NoSuchMethodException e) {
			throw new AssertionError("Could not find method to suppress ", e);
		}
	}

	private static Method[] suppressClass(Class testClass)
	{
		return testClass.getMethods();
	}
}
