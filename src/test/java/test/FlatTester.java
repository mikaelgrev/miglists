package test;

import com.miginfocom.lists.Indexer;
import com.miginfocom.lists.MigListFlat;
import com.miginfocom.lists.ReadPipe;
import com.miginfocom.lists.Serializer;
import com.miginfocom.lists.WritePipe;

import java.util.Random;


/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 24/10/13
 *         Time: 21:57 PM
 */
public class FlatTester
{
	public static void main(String[] args)
	{
		VecSer vecSer = new VecSer();
		MigListFlat<Vec> list = new MigListFlat<>(new VecSer());
		list.add(new Vec());
//		list.add(new Vec());

		Vec v = list.get(0);
		int ix = list.indexOf(v);
		System.out.println(ix);

//		Vec vec = new Vec();
//		Pipe<Vec> p = new Pipe<>(new int[vecSer.intsRequired()], vecSer.intsRequired());
//		p.setFlatOffset(0);
//		vecSer.write(vec, p);
//		p.setFlatOffset(0);
//		Vec vec2 = vecSer.read(p);
//		System.out.println(vec);
//		System.out.println(vec2);


//		list.remove(v);
//		System.out.println(Arrays.toString(list.toArray()));
//		System.out.println(v);

//		MigListFlat<String> list = new MigListFlat<>(new StringSer(), "1", "2", "3");
//		list.retainAll(Arrays.asList());
//		list.addAll(Arrays.asList("1", "2", "3"));
//		list.add(null);
//		list.add("aa");
//		list.add("aaa");
//		list.removeAll(Arrays.asList((Object) null));

//		System.out.println(Arrays.toString(list.toArray()));

		//		MigListBase<Integer, Integer[]> testList = new MigListBase<>();
//		testList.add(1);
//		testList.add(2);
//		testList.add(3);
//
//		testList.removeAll(Arrays.asList(1, 2, 3));
//		System.out.println(Arrays.toString(testList.toArray()));

	}

	public static class StringSer implements Serializer<String>
	{
		public int intsRequired()
		{
			return 1;
		}

		public boolean writeObject(Object object, WritePipe<String> pipe)
		{
			if (object != null && !(object instanceof String))
				return false;

			write((String) object, pipe);
			return true;
		}

		public void write(String s, WritePipe<String> pipe)
		{
			if (s != null) {
				byte[] bytes = s.getBytes();
				pipe.writeBytes(
					(byte) (bytes.length & 0xFF),
					bytes.length > 0 ? bytes[0] : (byte) -1,
					bytes.length > 1 ? bytes[1] : (byte) -1,
					bytes.length > 2 ? bytes[2] : (byte) -1
					);
			} else {
				pipe.writeBytes((byte) -1, (byte) 0, (byte) 0, (byte) 0);
			}
		}

		public String read(ReadPipe<String> pipe)
		{
			byte[] bytes = pipe.readBytes();
			return bytes[0] != (byte) -1 ? new String(bytes, 1, bytes[0]) : null;
		}
	}

	public static class VecSer implements Serializer<Vec>
	{
		public int intsRequired()
		{
			return 6;
//			return 8;
		}

		public boolean writeObject(Object object, WritePipe<Vec> pipe)
		{
			if (object != null && !(object instanceof Vec))
				return false;

			write((Vec) object, pipe);
			return true;
		}

		public void write(Vec v, WritePipe<Vec> pipe)
		{
			if (v != null) {
	//			pipe.writeBytes(v.v1, (byte) 0, (byte) 0, (byte) 0);
	//			pipe.writeShorts(v.v2, (short) 0);
				pipe.writeInt(v.v3);
				pipe.writeLong(v.v4);
				pipe.writeFloat(v.v5);
				pipe.writeDouble(v.v6);
			} else {
				pipe.writeInt(Integer.MIN_VALUE + 1);
			}
		}

		public Vec read(ReadPipe<Vec> pipe)
		{
			int i = pipe.readInt();
//			return new Vec(pipe.readBytes()[0], pipe.readShorts()[0], pipe.readInt(), pipe.readLong(), pipe.readFloat(), pipe.readDouble());
			return i != Integer.MIN_VALUE + 1 ? new Vec(pipe.readInt(), pipe.readLong(), pipe.readFloat(), pipe.readDouble()) : null;
		}
	}

	private static final Random RANDOM = new Random(1);
	public final static class Vec
	{
//		private final byte v1;
//		private final short v2;
		private final int v3;
		private final long v4;
		private final float v5;
		private final double v6;
		private final int hash;

		public static Indexer<Vec> VEC_INDEXER = new Indexer<Vec>()
		{
			public Integer index(Vec vec)
			{
				return vec.v3;
			}
		};

//		public static Indexer<Vec> VEC_1000_INDEXER = new Indexer<Vec>()
//		{
//			public Integer index(Vec vec)
//			{
//				return vec.v3 % 1000;
//			}
//		};

		public Vec()
		{
//			this(Byte.MAX_VALUE, Short.MAX_VALUE, Integer.MAX_VALUE, Long.MAX_VALUE, Float.MAX_VALUE, Double.MAX_VALUE);

//			this(RANDOM.nextBoolean() ? RANDOM.nextInt() : RANDOM.nextInt(500),
//			     RANDOM.nextBoolean() ? RANDOM.nextLong() : RANDOM.nextInt(500),
//			     RANDOM.nextBoolean() ? RANDOM.nextFloat() : Float.intBitsToFloat(RANDOM.nextInt(500)),
//			     RANDOM.nextBoolean() ? RANDOM.nextDouble() : Double.longBitsToDouble(RANDOM.nextInt(500)));
			this(RANDOM.nextInt(),
			     RANDOM.nextLong(),
			     RANDOM.nextFloat(),
			     RANDOM.nextDouble());
		}

		public Vec(Vec v)
		{
			//			this(Byte.MAX_VALUE, Short.MAX_VALUE, Integer.MAX_VALUE, Long.MAX_VALUE, Float.MAX_VALUE, Double.MAX_VALUE);
			this(v.v3, v.v4, v.v5, v.v6);
		}

		//		public Vec(byte v1, short v2, int v3, long v4, float v5, double v6)
		public Vec(int v3, long v4, float v5, double v6)
		{
//			this.v1 = v1;
//			this.v2 = v2;
			this.v3 = v3;
			this.v4 = v4;
			this.v5 = v5;
			this.v6 = v6;

			this.hash = hashCodeImpl();
		}

		public boolean equals(Object o)
		{
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}

			Vec vec = (Vec) o;

			if (v3 != vec.v3) {
				return false;
			}
			if (v4 != vec.v4) {
				return false;
			}
			if (Float.compare(vec.v5, v5) != 0) {
				return false;
			}
			if (Double.compare(vec.v6, v6) != 0) {
				return false;
			}

			return true;
		}

		public int hashCode()
		{
			return hash;
		}

		private int hashCodeImpl2()
		{
			return v3;
		}

		private int hashCodeImpl()
		{
			int result;
			long temp;
			result = v3;
			result = 31 * result + (int) (v4 ^ (v4 >>> 32));
			result = 31 * result + (v5 != +0.0f ? Float.floatToIntBits(v5) : 0);
			temp = Double.doubleToLongBits(v6);
			result = 31 * result + (int) (temp ^ (temp >>> 32));
			return result;
		}

		public String toString()
		{
			return "Vec {" + v3  + ", " + v4 + ", " + v5 + ", " + v6 + "}";
//			return "Vec {" + v1 + ", " + v2 + ", " + v3  + ", " + v4 + ", " + v5 + ", " + v6 + "}";
		}
	}
}
