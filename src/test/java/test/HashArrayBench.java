package test;

import com.miginfocom.lists.HashArray;
import com.miginfocom.lists.HashMap2;
import objectexplorer.MemoryMeasurer;

import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 13/11/13
 *         Time: 23:44 PM
 */
public class HashArrayBench
{
	static final Random rnd = new Random(1);
	static final int size = 1000000;
	static final int iter = 30;
	static final int[] RND_ARR = new int[size];
	static final Integer[] VALS = new Integer[size];
	static {
		for (int i = 0; i < RND_ARR.length; i++)
			RND_ARR[i] = rnd.nextInt();

		for (int i = 0; i < VALS.length; i++)
			VALS[i] = i;
	}

	public static void main(String[] args)
	{
		HashArray testMap = doHashArrayPutTest(size);
//		HashMap2 testMap = doHashMap2PutTest(size);
//		HashMap testMap = doHashMapPutTest(size);
//		ConcurrentHashMap testMap = doConcurrentHashMapPutTest(size);

		for (int i = 0; i < 11000; i++) // warmup
			doTestPut(1000);

		for (int i = 0; i < 10; i++)
			doTestGet(testMap, size);

		System.out.println("Start");
		Object theMap = null;
		System.gc();
		try {
			Thread.sleep(300);
		} catch (InterruptedException ignored) {}


		long n = System.nanoTime();

		for (int i = 0; i < iter; i++) {
//			theMap = doTestPut(size);
			theMap = doTestGet(testMap, size);
			if (theMap == null)
				throw new IllegalStateException();
		}

		System.out.println("Timed: " + ((System.nanoTime() - n) / 1000000f));
		System.gc();
		System.gc();
		System.out.println("Timed GC: " + ((System.nanoTime() - n) / 1000000f));
		System.out.println(theMap.getClass().getSimpleName() + " Mem size: " + MemoryMeasurer.measureBytes(theMap));

		if (theMap instanceof HashArray) {
			long slots = ((HashArray) theMap).getSlots();
			System.out.println("Fill ratio: " + (slots / (float) size));
			System.out.println("Object count: " + ((HashArray) theMap).getObjects());

			long pages = ((HashArray) theMap).getPages();
			System.out.println("Pages: " + pages);

			HashMap<Integer, long[]> freeOccMap = ((HashArray) theMap).getFreeOccPerLevel();
			for (int i = 0; i < 20; i++) {
				long[] occFree = freeOccMap.get(i);
				if (occFree != null)
					System.out.println("occFree: " + occFree[1] / (float) (occFree[0] + occFree[1]) + "   Free: " + occFree[0] + ", Occ: " + occFree[1]);
			}

		}

		if (Math.random() == 0.234)
			System.out.println(theMap);
	}

	private static Object doTestPut(int size)
	{
		//		return doHashMap2PutTest(size);
		return doHashArrayPutTest(size);
//				return doHashMapPutTest(size);
		//		return doConcurrentHashMapPutTest(size);
	}

	private static Object doTestGet(Object map, int size)
	{
		if (map instanceof HashArray) {
			return doGetTest((HashArray) map, size);
		} else if (map instanceof HashMap2) {
			return doGetTest((HashMap2) map, size);
		} else if (map instanceof HashMap) {
			return doGetTest((HashMap) map, size);
		} else if (map instanceof ConcurrentHashMap) {
			return doGetTest((ConcurrentHashMap) map, size);
		} else {
			throw new IllegalArgumentException(map.toString());
		}
	}

	private static HashArray doGetTest(HashArray hashArray, int size)
	{
		for (int i = 0; i < size; i++) {
			int rnd = RND_ARR[i];
			Object o = hashArray.get(rnd);
			if (rnd == 12422039)
				return (HashArray) o;
		}
		return hashArray;
	}

	private static HashMap2 doGetTest(HashMap2 hashMap2, int size)
	{
		for (int i = 0; i < size; i++) {
			int rnd = RND_ARR[i];
			Object o = hashMap2.get(rnd, rnd);
			if (rnd == 12422039)
				return (HashMap2) o;
		}
		return hashMap2;
	}

	private static HashMap doGetTest(HashMap hashArray, int size)
	{
		for (int i = 0; i < size; i++) {
			int rnd = RND_ARR[i];
			Object o = hashArray.get(rnd);
			if (rnd == 12422039)
				return (HashMap) o;
		}
		return hashArray;
	}

	private static ConcurrentHashMap doGetTest(ConcurrentHashMap hashArray, int size)
	{
		for (int i = 0; i < size; i++) {
			int rnd = RND_ARR[i];
			Object o = hashArray.get(rnd);
			if (rnd == 12422039)
				return (ConcurrentHashMap) o;
		}
		return hashArray;
	}

	private static HashArray doHashArrayPutTest(int size)
	{
		HashArray hashArray = new HashArray();
		for (int i = 0; i < size; i++) {
			int rnd = RND_ARR[i];
			hashArray.put(rnd, VALS[i]);
			if (rnd == 12422039)
				return null;
		}
		return hashArray;
	}

	private static HashMap2 doHashMap2PutTest(int size)
	{
		HashMap2 hashMap2 = new HashMap2(0);
		for (int i = 0; i < size; i++) {
			int rnd = RND_ARR[i];
			hashMap2.put(rnd, rnd, VALS[i]);
			if (rnd == 12422039)
				return null;
		}
		return hashMap2;
	}

	private static HashMap doHashMapPutTest(int size)
	{
		HashMap hashMap = new HashMap();
		for (int i = 0; i < size; i++) {
			int rnd = RND_ARR[i];
			hashMap.put(rnd, VALS[i]);
			if (rnd == 1242039)
				return null;
		}
		return hashMap;
	}

	private static ConcurrentHashMap doConcurrentHashMapPutTest(int size)
	{
		ConcurrentHashMap hashMap = new ConcurrentHashMap<>();
		for (int i = 0; i < size; i++) {
			int rnd = RND_ARR[i];
			hashMap.put(rnd, VALS[i]);
			if (rnd == 1242039)
				return null;
		}
		return hashMap;
	}

}
