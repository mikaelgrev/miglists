package test;

import com.google.common.collect.ArrayListMultimap;
import com.miginfocom.lists.Indexer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/** Test class to have something to compare to that have many indexes to a list.
 * @author Mikael Grev, MiG InfoCom AB
 *         Date: 09/11/13
 *         Time: 22:28 PM
 */
public class IndexedArrayList<E> extends ArrayList<E>
{
	private final ArrayListMultimap<Object, E>[] indexMaps;
	private final Indexer<E>[] indexers;

	@SafeVarargs
	public IndexedArrayList(Indexer<E>... indexers)
	{
		this(null, indexers);
	}

	@SafeVarargs
	public IndexedArrayList(Collection<? extends E> c, Indexer<E>... indexers)
	{
		this.indexers = indexers.clone();
		if (c != null)
			addAll(c);

		indexMaps = (ArrayListMultimap<Object, E>[]) new ArrayListMultimap[indexers.length];
		for (int i = 0; i < indexers.length; i++)
			indexMaps[i] = ArrayListMultimap.create();
	}

	public void add(int index, E element)
	{
		addToIndexed(element);
		super.add(index, element);
	}

	public boolean add(E e)
	{
		addToIndexed(e);
		return super.add(e);
	}

	public boolean addAll(Collection<? extends E> c)
	{
		for (E e : c)
			addToIndexed(e);
		return super.addAll(c);
	}

	public boolean addAll(int index, Collection<? extends E> c)
	{
		for (E e : c)
			addToIndexed(e);
		return super.addAll(index, c);
	}

	public List<E> getIndexed(Indexer<E> indexer, Object key)
	{
		int ix = getIndexOfIndexer(indexer);
		return indexMaps[ix].get(key);
	}

	public E remove(int index)
	{
		removeFromIndexes(get(index));
		return super.remove(index);
	}

	public boolean remove(Object o)
	{
		removeFromIndexes((E) o);
		return super.remove(o);
	}

	public void clear()
	{
		super.clear();
		clearIndexes();
	}

	protected void removeRange(int fromIndex, int toIndex)
	{
		for (int i = fromIndex; i < toIndex; i++)
			removeFromIndexes(get(i));

		super.removeRange(fromIndex, toIndex);
	}

	public boolean removeAll(Collection<?> c)
	{
		for (Object e : c) {
			removeFromIndexes((E) e);
		}

		return super.removeAll(c);
	}

	private void addToIndexed(E element)
	{
		for (int i = 0; i < indexers.length; i++)
			indexMaps[i].put(indexers[i].index(element), element);
	}

	private void removeFromIndexes(E element)
	{
		for (int i = 0; i < indexers.length; i++)
			indexMaps[i].remove(indexers[i].index(element), element);
	}

	private void clearIndexes()
	{
		for (int i = 0; i < indexers.length; i++)
			indexMaps[i].clear();
	}

	private int getIndexOfIndexer(Indexer<E> indexer)
	{
		for (int i = 0; i < indexers.length; i++) {
			if (indexers[i] == indexer)
				return i;
		}
		throw new IllegalArgumentException("Unknown indexer: " + indexer);
	}
}
